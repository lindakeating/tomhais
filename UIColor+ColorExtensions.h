//
//  UIColor+ColorExtensions.h
//  Facts
//
//  Created by Linda Keating on 15/04/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorExtensions)

+(UIColor *) backgroundColorGrey;
+(UIColor *) foregroundColorGrey;
+(UIColor *) greenSpacesColor;
+(UIColor *) warningRedColor;
+(UIColor *) textGreyColor;
+(UIColor *) profileYellowColor;
+(UIColor *) inactiveGreyColor;
+(UIColor *) horizontalGreyColor;
+(UIColor *) whiteGreyColor;

@end

//
//  SavedRiddleCell.h
//  Facts
//
//  Created by Linda Keating on 14/04/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SavedRiddleCell : UITableViewCell
@property (strong, nonatomic) UILabel *star;
@property (strong, nonatomic) UILabel *savedTomhais;

@end

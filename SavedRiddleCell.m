//
//  SavedRiddleCell.m
//  Facts
//
//  Created by Linda Keating on 14/04/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "SavedRiddleCell.h"
#import "UIColor+ColorExtensions.h"

@implementation SavedRiddleCell{
    NSString *device;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            device = @"IPAD";
            self.frame = CGRectMake(0, 0, 500, 40);
        }else{
            device = @"IPHONE";
            self.frame = CGRectMake(0, 0, 320, 40);
        }
        
        self.backgroundColor = [UIColor backgroundColorGrey];
        
        self.star = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 35, 35)];
        self.star.textColor = [UIColor profileYellowColor];
        self.star.font = [UIFont fontWithName:@"tomhais-icons" size:24];
        self.star.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.star];
        NSLog(@"FrameWidth : %f" ,self.frame.size.width);
        
        if ([device isEqualToString:@"IPHONE"]) {
            self.savedTomhais = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, self.frame.size.width -85, 45)];
            self.savedTomhais.font = [UIFont fontWithName:@"AvenirNext-Medium" size:12.0f];
        }else{
            self.savedTomhais = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, self.frame.size.width -85, 80)];
           self.savedTomhais.font = [UIFont fontWithName:@"AvenirNext-Medium" size:16.0f];
        }
        self.savedTomhais.textColor = [UIColor whiteColor];
        self.savedTomhais.lineBreakMode = NSLineBreakByWordWrapping;
        self.savedTomhais.numberOfLines = 0;
        [self addSubview:self.savedTomhais];
        
        
    }
    return self;
}

@end

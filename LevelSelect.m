//
//  LevelSelect.m
//  Facts
//
//  Created by Linda Keating on 09/12/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "LevelSelect.h"
#import "GameScene.h"
#import "FactsScene.h"


@implementation LevelSelect {
    long actualPlayerLevel;
}

-(id) initWithSize:(CGSize)size{
    if (self = [super initWithSize:size]) {
        self.backgroundColor = [SKColor colorWithRed:0.25 green:0.35 blue:0.15 alpha:1.0];
    }
    return self;
}

-(void) didMoveToView:(SKView *)view {
    _backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _backButton.frame = CGRectMake(CGRectGetMidX(self.frame)-240, CGRectGetMaxY(self.frame) +100, 200, 70.0);
    _backButton.backgroundColor = [UIColor clearColor];
    [_backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *buttonExitImageNormal = [UIImage imageNamed:@"back.png"];
    UIImage *stretchableButtonExitImageNormal = [buttonExitImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [_backButton setBackgroundImage:stretchableButtonExitImageNormal forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(moveToHome) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_backButton];
    
    SKLabelNode *titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    titleLabel.text = @"Level Select!!";
    titleLabel.fontSize = 60;
    titleLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)+300);
    [self addChild:titleLabel];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.frame) -280, CGRectGetMidY(self.frame)-200, 300, 400)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    _levelsArray = [[NSArray alloc] initWithObjects:
                     @"Level 1.",
                     @"Level 2.",
                     @"Level 3.",
                     @"Level 4.",
                     @"Level 5.",
                     @"Level 6.",
                     @"Level 7.",
                     @"Level 8.",
                     @"Level 9.",
                     @"Level 10.",
                    nil];
    
    _levelsDescriptionArray = [[NSArray alloc] initWithObjects:
                               @"The adventure begins",
                               @"A new Step",
                               @"Achievements?!",
                               @"Level 4 description",
                               @"Level 5 description",
                               @"Level 6 description",
                               @"Level 7 description",
                               @"Level 8 description",
                               @"Level 9 description",
                               @"Level 10 description",
                               nil];
    
    actualPlayerLevel = 1;
    
    [self.view addSubview:_tableView];
}

-(void)moveToHome{
    GameScene* gScene = [[GameScene alloc] initWithSize:CGSizeMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame))];
    [self removeUIViews];
    [self.scene.view presentScene:gScene];
}

-(void)removeUIViews{
    [_backButton removeFromSuperview];
    [_tableView removeFromSuperview];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_levelsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *levels = [_levelsArray objectAtIndex:indexPath.row];
    NSString *descriptions = [_levelsDescriptionArray objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Identifier"];
    
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Identifier"];
    }
    
    if (indexPath.row >= actualPlayerLevel)
        [cell setUserInteractionEnabled:FALSE];
    
    [cell.textLabel setText:levels];
    cell.imageView.image = [UIImage imageNamed:@"appleLogo.png"];
    [cell.detailTextLabel setText:descriptions];
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SKTransition* transition = [SKTransition fadeWithDuration:1];
    FactsScene* fScene = [[FactsScene alloc] initWithSize:CGSizeMake((CGRectGetMaxX(self.frame)), CGRectGetMaxY(self.frame)) inLevel:1 withPlayerLives:3];
    [self removeUIViews];
    [self.scene.view presentScene:fScene transition:transition];

}

@end

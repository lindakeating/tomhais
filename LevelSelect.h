//
//  LevelSelect.h
//  Facts
//
//  Created by Linda Keating on 09/12/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface LevelSelect : SKScene <UITableViewDataSource , UITableViewDelegate>
@property (nonatomic, retain) UIButton* backButton;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *levelsArray;
@property (strong, nonatomic) NSArray *levelsDescriptionArray;

@end

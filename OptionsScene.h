//
//  OptionsScene.h
//  Facts
//
//  Created by Linda Keating on 09/12/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface OptionsScene : SKScene

@property (nonatomic, retain) UIButton* backButton;
@property (nonatomic, retain) IBOutlet UISwitch *musicSwitch;
@property (nonatomic, retain) IBOutlet UISwitch *soundSwitch;
@property (nonatomic, retain) SKLabelNode* soundTitle;
@property (nonatomic, retain) SKLabelNode* musicTitle;

@end

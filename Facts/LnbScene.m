//
//  LnbScene.m
//  Facts
//
//  Created by Linda Keating on 22/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "LnbScene.h"
#import "LDProgressView.h"
#import "GameViewController.h"
#import "UIColor+ColorExtensions.h"
#import "DBManager.h"

@implementation
LnbScene {

    float nextSpaceTile;
    SKLabelNode* _level;
    SKLabelNode* _riddlesCompleted;
				LDProgressView* progressView;
				UIColor* wrongColor;
				UIColor* neutralColor;
				CGRect screen;
				button* favourite;
				DBManager *dbManager;
	
}

-(id) initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
								[self setUpScoreLabels];
								
								// initialise the DBManager
								dbManager = [[DBManager alloc] initWithDatabaseFilename:@"tomhaisdb.sql"];
								
        nextSpaceTile = 0;
        self.backgroundColor = [SKColor colorWithRed:38.0f/255.0f
                                               green:42.0f/255.0f
                                                blue:46.0f/255.0f
                                               alpha:1];
        
        _tomhaiseanna = [[NSMutableArray alloc] init];
								_tilesCollection = [[NSMutableArray alloc] init];
        NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"tomhaiseanna"
																																																														ofType:@"plist"];
								
        NSMutableDictionary* dictionary  = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
        
        if ([dictionary objectForKey:@"Tomhaiseanna"] != nil) {
            NSMutableArray *array = [dictionary objectForKey:@"Tomhaiseanna"];
            
            for (int i= 0; i < [array count]; i++) {
                NSMutableDictionary *questions = [array objectAtIndex:i];
																if ([[questions objectForKey:@"answer"]length] < 16) {
																				tomhaisObject *tObject  = [tomhaisObject new];
																				tObject.tomhaisId = [[questions objectForKey:@"id"] intValue];
																				tObject.gameType = [questions objectForKey:@"gameType"];
																				tObject.question = [questions objectForKey:@"question"];
																				tObject.answer = [questions objectForKey:@"answer"];
																				tObject.hint = [questions objectForKey:@"hint"];
																				tObject.favourite = [[questions objectForKey:@"favourite"] intValue];
																				tObject.isCorrect = [[questions objectForKey:@"isCorrect"]intValue];
																				[_tomhaiseanna addObject:tObject];
																}
            }
        }
    }
    return self;
}

-(void)didMoveToView:(SKView *)view {
				screen = [[UIScreen mainScreen]bounds];
				
				wrongColor = [UIColor warningRedColor];
				
				neutralColor = [UIColor whiteGreyColor];
				
				//initialise the game scores
				_score.text = [NSString stringWithFormat:@" %li", [gameData sharedGameData].score];
				// find out which riddle is current
				_currentRiddle = [gameData sharedGameData].riddlesCompleted;
				
    //teidealbackground
				SKSpriteNode *teidealBackground;

				teidealBackground = [SKSpriteNode spriteNodeWithImageNamed:@"teidealBackground.png"];
				
    teidealBackground.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame));
				
				if ([self.device isEqualToString:@"IPHONE"]) {
								teidealBackground.size = CGSizeMake(self.frame.size.width, 110);
				}
    [self addChild:teidealBackground];
    
    // teidealText
				CGRect labelFrame = CGRectMake(0, 0, self.frame.size.width,118);
				if ([self.device isEqualToString:@"IPHONE"]) {
								labelFrame.size = CGSizeMake(self.frame.size.width, 70);
				}
    _teidealTitle= [[UILabel alloc] initWithFrame:labelFrame];
				NSMutableAttributedString* labelText = [[NSMutableAttributedString alloc] initWithString:@"LÍON NA BEARNAÍ"];
				NSRange range = NSMakeRange(0, labelText.length);
				[labelText addAttribute:NSKernAttributeName value:@(4.0) range:range];
				[labelText addAttribute:NSForegroundColorAttributeName value:[UIColor textGreyColor] range:range];
				[labelText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AvenirNext-Medium" size:18] range:range];
				if ([self.device isEqualToString:@"IPHONE"]) {
								[labelText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AvenirNext-Medium" size:12] range:range];
				}
				 NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
				[paragraphStyle setAlignment:NSTextAlignmentCenter];
				[labelText addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
				NSShadow *shadow = [[NSShadow alloc]init];
				[shadow setShadowColor:[UIColor colorWithWhite:0 alpha:0.5]];
				[shadow setShadowOffset:CGSizeMake(0, 1)];
				[labelText addAttribute:NSShadowAttributeName value:shadow range:range];
				[_teidealTitle setAttributedText:labelText];

				[self.view addSubview:_teidealTitle];
				
				// add Navigation Icon
				//TODO: Make Font from Sinéad's Menu button and swap in here.
				button *navigation = [[button alloc] initWithString:@"Q" fontNamed:@"Icon-Works" size:40];
				navigation.position = CGPointMake(720, CGRectGetMaxY(self.frame)-70);
				
				
				if ([self.device isEqualToString:@"IPHONE"]) {
								navigation = [[button alloc] initWithString:@"Q" fontNamed:@"Icon-Works" size:30];
								navigation.position = CGPointMake(296, CGRectGetMaxY(self.frame)-42);
				}
				[self addChild:navigation];
				
				favourite;
				if ([self.device isEqualToString:@"IPHONE"]) {
								favourite = [[button alloc] initWithString:@"e" fontNamed:@"tomhais-icons" size:37];
								favourite.position = CGPointMake(35, CGRectGetMaxY(self.frame) - (screen.size.height / 4.57));
								NSLog(@"Favourite View frame: %@", NSStringFromCGRect(favourite.frame));
				}else{
					   favourite = [[button alloc] initWithString:@"e" fontNamed:@"tomhais-icons" size:45];
								favourite.position = CGPointMake(115, 820);
				}
				[self addChild:favourite];
				
				
    // scoreIcon
				CGRect iconFram;
				if ([self.device isEqualToString:@"IPHONE"]) {
								iconFram = CGRectMake(130,  CGRectGetMaxY(self.frame) - (screen.size.height / 1.14), 49, 50);
									NSLog(@"Icon frame: %@", NSStringFromCGRect(iconFram));
				}else{
								iconFram = CGRectMake(715.0f/2.0f -30, 130, 88, 101);
				}
    _scoreIcon = [[UILabel alloc] initWithFrame:iconFram];
    NSString *iconText = @"v";
    [_scoreIcon setText:iconText];
    [_scoreIcon setTextColor:[UIColor textGreyColor]];
    [_scoreIcon setTextAlignment:NSTextAlignmentCenter];
    [_scoreIcon setFont:[UIFont fontWithName:@"Icon-Works" size:50]];
				if ([self.device isEqualToString:@"IPHONE"]) {
								[_scoreIcon setFont:[UIFont fontWithName:@"Icon-Works" size:45]];
				}
    _scoreIcon.shadowColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:.5];
    _scoreIcon.shadowOffset = CGSizeMake(0, 2);
    _scoreIcon.layer.masksToBounds = NO;
    [self.view addSubview:_scoreIcon];
    
    // add bulb Icon
				button *bulbLife;
				if ([self.device isEqualToString:@"IPHONE"]) {
								bulbLife = [[button alloc] initWithString:@"b" fontNamed:@"hint-icon" size:30];
				}else{
							 bulbLife = [[button alloc] initWithString:@"b" fontNamed:@"hint-icon" size:40];
				}
    bulbLife.position = CGPointMake(CGRectGetMinX(self.frame) + 556, CGRectGetMaxY(self.frame)-190);
				if ([self.device isEqualToString:@"IPHONE"]) {
								bulbLife.position = CGPointMake(CGRectGetMinX(self.frame) + 215, CGRectGetMaxY(self.frame) - (screen.size.height / 5));
									NSLog(@"Bulb Life frame: %@", NSStringFromCGRect(bulbLife.frame));
				}
    [self addChild:bulbLife];
    
    // add bulb Lives (hint Lives)
    _hintLives = [[lives alloc] initWithString:@"bulb" lives:[gameData sharedGameData].hints];
    _hintLives.position = CGPointMake(CGRectGetMinX(self.frame) + 544, CGRectGetMaxY(self.frame) - 202);
				if ([self.device isEqualToString:@"IPHONE"]) {
								_hintLives.position = CGPointMake(CGRectGetMinX(self.frame) + 203, CGRectGetMaxY(self.frame) - (screen.size.height / 4.57));
				}
    [self addChild:_hintLives];
    
    // add firstLetter Icon
				// TODO: Change this to read lives from Database
				
				button *firstLetter;
				if ([self.device isEqualToString:@"IPHONE"]) {
								firstLetter = [[button alloc] initWithString:@"f" fontNamed:@"hint-icon" size:30];
				}else{
							firstLetter = [[button alloc] initWithString:@"f" fontNamed:@"hint-icon" size:40];
				}
    firstLetter.position = CGPointMake(CGRectGetMinX(self.frame) + 602, CGRectGetMaxY(self.frame)-190);
				if ([self.device isEqualToString:@"IPHONE"]) {
								firstLetter.position = CGPointMake(CGRectGetMinX(self.frame) + 251, CGRectGetMaxY(self.frame) - (screen.size.height / 5));
				}
    [self addChild:firstLetter];
    
    // add firstLetter Lives
				// TODO: Change this to read lives from the database
    _firstLetterLives = [[lives alloc] initWithString:@"firstLetter" lives:[gameData sharedGameData].firstLetters];
				if ([self.device isEqualToString:@"IPHONE"]) {
								_firstLetterLives.position = CGPointMake(CGRectGetMinX(self.frame) + 239, CGRectGetMaxY(self.frame) - (screen.size.height / 4.57));
				}else{
								_firstLetterLives.position = CGPointMake(CGRectGetMinX(self.frame) + 590, CGRectGetMaxY(self.frame) -202);
				}
    [self addChild:_firstLetterLives];
    
    // add answerHint Icon
				// TODO: Change this to read lives from the database
				button *answerHint;
				if ([self.device isEqualToString:@"IPHONE"]) {
								answerHint = [[button alloc] initWithString:@"t" fontNamed:@"hint-icon" size:30];
				}else{
								answerHint = [[button alloc] initWithString:@"t" fontNamed:@"hint-icon" size:40];
				}
    answerHint.position = CGPointMake(CGRectGetMinX(self.frame) + 647, CGRectGetMaxY(self.frame)-190);
				if ([self.device isEqualToString:@"IPHONE"]) {
								answerHint.position = CGPointMake(CGRectGetMinX(self.frame) + 287, CGRectGetMaxY(self.frame) - (screen.size.height / 5));
				}
    [self addChild:answerHint];
    
    // add answer Lives
				// TODO: Change this to read lives rom the database
    _answerLives = [[lives alloc] initWithString:@"answerLives" lives:[gameData sharedGameData].answers];
				if ([self.device isEqualToString:@"IPHONE"]) {
								_answerLives.position = CGPointMake(CGRectGetMinX(self.frame) + 275, CGRectGetMaxY(self.frame) - (screen.size.height / 4.57));
				}else{
								_answerLives.position = CGPointMake(CGRectGetMinX(self.frame) + 635, CGRectGetMaxY(self.frame) - 202);
				}
    [self addChild:_answerLives];
    
    float fr = self.frame.size.height;
    tomahaisQuestion *tomhaisQuestion = [[tomahaisQuestion alloc]initWithString:@"jdksjlfsdj" :fr :self.device];
    [self addChild:tomhaisQuestion];
    
    // riddle Text
				CGRect riddleFrame;
				if ([self.device isEqualToString:@"IPHONE"]) {
								riddleFrame = CGRectMake(40, self.frame.size.height / 3.22, 237, self.frame.size.height/3.75);
				}else{
							riddleFrame = CGRectMake(159, 290, 450, 187);
				}
    _questionLabel = [[UILabel alloc] initWithFrame:riddleFrame];
    [_questionLabel setTextAlignment:NSTextAlignmentCenter];
    [_questionLabel setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:32]];
				if ([self.device isEqualToString:@"IPHONE"]) {
							[_questionLabel setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:22]];
				}
    [_questionLabel setTextColor:[UIColor textGreyColor]];
    NSString *riddleText = [[_tomhaiseanna objectAtIndex:_currentRiddle]question];
    [_questionLabel setText:riddleText];
    [_questionLabel setNumberOfLines:0];
    [_questionLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [self.view addSubview:_questionLabel];
    
    //set Current Answer
    _currentTomhais = [_tomhaiseanna objectAtIndex:_currentRiddle];
    
    //create Bearnaí
    [self createLetterSpaces];
    
    //create letter options
    [self createLetterOptions];
    
    //Add Progress Bar
				if ([self.device isEqualToString:@"IPHONE"]) {
								progressView = [[LDProgressView alloc] initWithFrame:CGRectMake(20, screen.size.height / 4, self.frame.size.width - 40, screen.size.height / 34.285)];
							//	progressView = [[LDProgressView alloc] initWithFrame:CGRectMake(20, 120, self.frame.size.width - 40, 14)];
				}else{
								progressView = [[LDProgressView alloc] initWithFrame:CGRectMake(95, 220, self.frame.size.width - 200, 25)];
				}
    progressView.progress = _currentRiddle/[_tomhaiseanna count];
    progressView.type = LDProgressSolid;
    progressView.color = [UIColor colorWithRed:107.0f/255.0f green:182.0f/255.0f blue:196.0f/255.0f alpha:1];
    progressView.background = [UIColor colorWithRed:26.0f/255.0f green:29.0f/255.0f blue:33.0f/255.0f alpha:1];
    progressView.borderRadius = @2;
    progressView.showText = @NO;
    [progressView setFlat:@YES];
    [self.view addSubview:progressView];
    
}

//TODO: create a view class for the spaces
-(void) createLetterSpaces{
				float xSize;
				
				if ([self.device isEqualToString:@"IPHONE"]) {
								xSize = 36;
				}else{
								xSize = 72;
				}
				
    self.spacesBoundary = [[SKNode alloc] init];
				if ([self.device isEqualToString:@"IPHONE"]) {
								[_spacesBoundary setPosition:CGPointMake(20, CGRectGetMinY(self.frame) + screen.size.height / 3.8095238)];
				}else{
								[_spacesBoundary setPosition:CGPointMake(99, CGRectGetMaxY(self.frame)-(1306/2))];
				}
    NSString *answer = [[_tomhaiseanna objectAtIndex:_currentRiddle]answer];
				NSMutableArray *whiteSpacesArray = [[NSMutableArray alloc] init];
				
				NSArray *words = [answer componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
				//find out how many spaces are in the answer string
				int amountOfWhiteSpaces = (int)[words count];
				int currentWhiteSpace = 1;
				//
				float x = 0;
    float y = 0;
				int spacesUsed = 0;
				for (NSString *currentWord in words) {
								int currentWordIndex = 0;
								spacesUsed = spacesUsed + (int)[currentWord length];
								
								for (int i=0; i < [currentWord length]; i++) {
												// create Tiles for each blank space and hide them
												unichar letter = [currentWord characterAtIndex:i];
												NSString *letterString = [NSString stringWithCharacters:&letter length:1];
												letterSpace *lSpace = [[letterSpace alloc ]initWithCharacter:letterString xPosition:x yPosition:y device:self.device];
												[self.spacesArray addObject:lSpace];
												[_spacesBoundary addChild:lSpace];
												x += xSize;
												y += 1;
												if (i == [currentWord length]-1 && currentWhiteSpace < amountOfWhiteSpaces) {
																letterSpace *wSpace = [[letterSpace alloc] initWithCharacter:@" " xPosition:x yPosition:y device:self.device];
																[whiteSpacesArray addObject:wSpace];
																[self.spacesArray addObject:wSpace];
																[_spacesBoundary addChild:wSpace];
																spacesUsed = spacesUsed +1;
																if ([words count] >= currentWordIndex +1 ) {
																				NSString *nextWord = [words objectAtIndex:currentWordIndex +1];
																				if ([nextWord length] + spacesUsed > 8) {
																								x = xSize * 8;
																								y = 8;
																				}
																				else{
																								x += xSize;
																								y += 1;
																				}
																}
																currentWhiteSpace += 1;
												}
								}
				}
    [self addChild:_spacesBoundary];
    
}
-(void)createLetterOptions{
				self.tilesBoundary = [[SKNode alloc] init];
				if ([self.device isEqualToString:@"IPHONE"]) {
								[_tilesBoundary setPosition:CGPointMake(CGRectGetMidX(self.frame)-126, CGRectGetMinY(self.frame)+ screen.size.height / 8)];
				}else{
							 [_tilesBoundary setPosition:CGPointMake(129, CGRectGetMaxY(self.frame)-790)];
				}
				
				NSString *answer = [[_tomhaiseanna objectAtIndex:_currentRiddle]answer];
				NSArray *words = [answer componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
				int answerPosition = 0;
				
				NSArray *spacesArray = [_spacesBoundary children];
				NSMutableArray *spacesArrayWithoutWhiteSpaces = [[NSMutableArray alloc] init];
				for (letterSpace *space in spacesArray) {
								if (space.occupied == FALSE) {
												[spacesArrayWithoutWhiteSpaces addObject:space];
								}
				}
				
				// create Tiles from Answer set wheter first Letter or not and position in Answer
				for (int i=0; i< [words count]; i++) {
								NSString *currentWord = [words objectAtIndex:i];
								for (int j= 0; j<[currentWord length]; j++) {
												letterSpace *lSpace = [spacesArrayWithoutWhiteSpaces objectAtIndex:answerPosition];
												unichar letterChar = [currentWord characterAtIndex:j];
												NSString *letterString = [NSString stringWithCharacters:&letterChar length:1];
												Tile *answerTile = [[Tile alloc] addHiddenTileToSpace:letterString withIndex:answerPosition withPositionInWord:j device:self.device];
												answerTile.targetPoint = CGPointMake(lSpace.position.x +30, lSpace.position.y + 30);
												answerTile.targetPoint = lSpace.position;
												[_tilesCollection addObject:answerTile];
													answerPosition += 1;
								}
				}
				
				// create extra random tiles
				int deficitTiles = (16 - (int)[_tilesCollection count]);
				NSString *randomExtraLetters = [self randomStringWithLength:deficitTiles];
				
				for (int x = 0; x < [randomExtraLetters length]; x++) {
								unichar lChar = [randomExtraLetters characterAtIndex:x];
								NSString *extraLetter = [NSString stringWithCharacters:&lChar length:1];
								Tile *extraTile = [[Tile alloc] addHiddenTileToSpace:extraLetter withIndex:17 withPositionInWord:17 device:self.device];
								[_tilesCollection addObject:extraTile];
				}
				// randomly rearrange the tiles in the tilesCollection Array
				[self shuffleNSMutableArray:_tilesCollection];
				
				int x = 0;
				int y = 0;
				
				float xSize;
				float ySize;
				
				if ([self.device isEqualToString:@"IPHONE"]) {
								xSize = 36;
								ySize = -36;
				}else{
								xSize = 72;
								ySize = -71;
				}

				//set the position of each Tile within the tilesCollection and add to the tilesBoundary
				for (int t = 0; t< [_tilesCollection count]; t++) {
								Tile *tile = [_tilesCollection objectAtIndex:t];

								if (y <8) {
												CGPoint tilePositon = CGPointMake(x, 0);
												tile.position = tilePositon;
												tile.originalPoint = tilePositon;
												[_tilesBoundary addChild:tile];
												
								}
								else{
												CGPoint tileSecondRowPosition = CGPointMake(x -(xSize *8), ySize);
												tile.position = tileSecondRowPosition;
												tile.originalPoint = tileSecondRowPosition;
												[_tilesBoundary addChild:tile];
								}
								x += xSize;
								y += 1;
				}
				
				[self addChild:_tilesBoundary];
				
				NSLog(@"Some String");
}
- (void)shuffleNSMutableArray:(NSMutableArray *)array
{
    NSUInteger count = [array count];
    for (NSUInteger i = 0; i < count; ++i) {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
        [array exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
}

-(NSString *) randomStringWithLength: (int)len{
    NSString *letters = @"aábcdeéfghiílmnoóprstuv";
    NSMutableString *randomString = [NSMutableString stringWithCapacity:len];
    for (int i=0; i<len; i++) {
        [randomString appendFormat:@"%C", [letters characterAtIndex:arc4random_uniform((int)[letters length])]];
    }
    return randomString;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint positionInScene = [touch locationInNode:self];
    [self selectNodeForTouch:positionInScene];
    
}

-(void)selectNodeForTouch:(CGPoint)touchLocation{
				SKNode *node = (SKNode *)[self nodeAtPoint:touchLocation];
				if ([[node name]isEqualToString:@"Q"]) {
								SKLabelNode *labelText = (SKLabelNode *)node;
								SKAction *scaleUp = [SKAction scaleBy:1.3 duration:.3];
								SKAction *scaleDown = [SKAction scaleBy:.768 duration:.3];
								SKAction *sequence = [SKAction sequence:@[scaleUp , scaleDown]];
								[labelText runAction:sequence];
								// the nodes need to be disabled when the menu is shown otherwise the item gets favourited twice.
							/*	[(GameViewController *)self.view.window.rootViewController addToFavourites:_currentTomhais];
								NSLog(@"Favourited");*/
								[(GameViewController *)self.view.window.rootViewController toggleMenu];
				}
				
				if ([[node name]isEqualToString:@"e"]) {
								// add current riddle to favourites and change the icon of the star
								SKLabelNode *labelText = (SKLabelNode *)node;
								labelText.text = @"d";
								labelText.name = @"d";
								labelText.fontColor = [UIColor profileYellowColor];
								SKAction *scaleUp = [SKAction scaleBy:1.3 duration:.3];
								SKAction *scaleDown = [SKAction scaleBy:.768 duration:.3];
								SKAction *sequence = [SKAction sequence:@[scaleUp , scaleDown]];
								[labelText runAction:sequence];
								[(GameViewController *)self.view.window.rootViewController addToFavourites:_currentTomhais];
								NSLog(@"Favourited");
				}
				
    Tile *touchedNode = (Tile *)[self nodeAtPoint:touchLocation];

				
     if ([[touchedNode name]isEqualToString:@"Tile"]) {
       // check if the tile has already been moved
									[self moveTile:touchedNode];
     }
				
				if ([[node name]isEqualToString:@"tileCharacter"]) {
								Tile *tile = (Tile *)[node parent];
								if (tile.moved == NO) {
												[self moveTile:tile];
								}
				}
}

-(void) moveTile:(Tile *)tile{
				// check if the node being touched is a tile
    float usedSpaces = 0;
    NSString *providedAnswer = @"";
				
				if (tile.moved == TRUE) {
								//move the node back to the _tilesBoundary space
								tile.moved = FALSE;
								tile.characterNode.fontColor = neutralColor;
								tile.position = tile.originalPoint;
								letterSpace *currentSpace = [_spacesArray objectAtIndex:tile.letterSpaceIndex];
								currentSpace.occupied = FALSE;
				}else{
								tile.moved = TRUE;
								// moving nodes to space
								for (int i = 0; i < [_spacesArray count]; i++) {
												letterSpace *targetSpace = [_spacesArray objectAtIndex:i];
												if (targetSpace.occupied == FALSE) {
																targetSpace.character = tile.character;
																targetSpace.occupied = TRUE;
																tile.letterSpaceIndex = i;
																if ([self.device isEqualToString:@"IPHONE"]) {
																				tile.position = CGPointMake((_spacesBoundary.position.x + targetSpace.position.x +15)-_tilesBoundary.position.x,( _spacesBoundary.position.y + targetSpace.position.y + 3 + 15)-_tilesBoundary.position.y);
																}else{
																				tile.position = CGPointMake((_spacesBoundary.position.x + targetSpace.position.x +30)-_tilesBoundary.position.x,( _spacesBoundary.position.y + targetSpace.position.y + 6 + 30)-_tilesBoundary.position.y);
																}
																break;
												}
								}
								// finding out what letters have been submitted as an answer
								for (int i= 0; i< [_spacesArray count]; i++) {
												letterSpace *targetSpace = [_spacesArray objectAtIndex:i];
												if (targetSpace.occupied == TRUE) {
																providedAnswer = [providedAnswer stringByAppendingString:targetSpace.character ];
																usedSpaces = usedSpaces +1;
												}
								}
								// check to see if all the spaces have been filled
								if (usedSpaces == [_spacesArray count]) {
												NSString *answer = [[_tomhaiseanna objectAtIndex:_currentRiddle]answer];
												if ([[providedAnswer lowercaseString]isEqual:[answer lowercaseString]]) {
																[self rightAnswerProvided:answer completion:^(BOOL finished) {
																				if (finished) {
																				}
																}];
												}else{
																self.userInteractionEnabled = NO;
																[self setIncorrectTileColor];
																[(GameViewController *)self.view.window.rootViewController loadWrongAnswer];
																NSLog(@"Incorrect Answer provided");
												}
								}
				}
}

-(void)rightAnswerProvided:(NSString*)answer completion:(myCompletion) compblock{
				GameViewController * gvc = (GameViewController *)self.view.window.rootViewController;
				// put back all the moved tiles
				[gvc removeTilesFromSpaces:^(BOOL finished) {
								if (finished) {
												// move all the right tiles to the right spaces in the spaces_boundary Array
												[gvc showAnswer:^(BOOL finished) {
																// animate the answer
																if (finished) {
																				// NSLog
																				[gvc animateRightAnswer:^(BOOL finished) {
																								if (finished) {
																												// show the right answer panel
																												[gvc showRightAnswer:answer];
																								}
																				}];
																}
												}];
								}
				}];
}

-(void) setIncorrectTileColor{
				for (Tile *t in self.tilesCollection) {
								if (t.moved == TRUE) {
												t.characterNode.fontColor = wrongColor;
								}
				}
}

-(void) setNeutralTileColor{
				for (Tile *t in self.tilesCollection) {
								if (t.moved == TRUE) {
												t.characterNode.fontColor = neutralColor;
								}
				}
}

-(void) nextRiddleWithDelay{
				double delayInSeconds = 1.0;
				dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
				dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
							_questionLabel.text = @"Updated Text";
				});
				
								// move to next Question in riddles array - update current question
				
}

-(void) nextRiddle{
				// move to next Question in riddles array - update current question
				
				// reset whether the lives have been used				
				_questionLabel.text = [[_tomhaiseanna objectAtIndex:_currentRiddle]question];
				_currentTomhais = [_tomhaiseanna objectAtIndex:_currentRiddle];
				progressView.progress = _currentRiddle/[_tomhaiseanna count];
				favourite.label.name = @"e";
				favourite.label.text = @"e";
				favourite.label.fontColor = [UIColor textGreyColor];
				
				
				// remove current letter spaces
				for (letterSpace *ls in _spacesArray){
								[ls removeFromParent];
				}
				[_spacesArray removeAllObjects];
				[_tilesCollection removeAllObjects];
				[_spacesBoundary removeAllChildren];
				[_tilesBoundary removeAllChildren];
				[_tilesBoundary removeFromParent];
				
				[self createLetterSpaces];
				[self createLetterOptions];
				GameViewController *gvController = (GameViewController *)self.view.window.rootViewController;
				[gvController removeHintShown];
				gvController.usedFirstLetter = FALSE;
				gvController.hintUsed = FALSE;
				gvController.answerShown = FALSE;
}

-(NSMutableArray *) spacesArray{
    if(!_spacesArray){
        _spacesArray = [NSMutableArray new];
        
    }
    return _spacesArray;
}

-(void) setUpScoreLabels
{
    _score = [[SKLabelNode alloc] initWithFontNamed:@"Open Sans"];
				_score.fontSize = 14;
				_score.position = CGPointMake(371, CGRectGetMaxY(self.frame)-182);
				_score.fontColor = [SKColor whiteColor];
				[self addChild:_score];
    
}



@end



//
//  gameData.m
//  Facts
//
//  Created by Linda Keating on 30/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "gameData.h"

@implementation gameData

static NSString* const gameDataScoreKey = @"Score";
static NSString* const gameDataLevelKey = @"Level";
static NSString* const gameDataRiddlesCompletedKey = @"RiddlesCompleted";
static NSString* const gameDataHintsKey = @"Hints";
static NSString* const gameDataFirstLettersKey = @"FirstLetters";
static NSString* const gameDataAnswersKey = @"Answers";
static NSString* const favouritesArray = @"Favourites";

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeDouble:self.score forKey:gameDataScoreKey];
    [aCoder encodeDouble:self.level forKey:gameDataLevelKey];
    [aCoder encodeDouble:self.riddlesCompleted forKey:gameDataRiddlesCompletedKey];
    [aCoder encodeDouble:self.hints forKey:gameDataHintsKey];
    [aCoder encodeDouble:self.firstLetters forKey:gameDataFirstLettersKey];
    [aCoder encodeDouble:self.answers forKey:gameDataAnswersKey];
    [aCoder encodeObject:self.favourites forKey:favouritesArray];
}
-(instancetype)initWithCoder:(NSCoder *)decoder{
    self = [self init];
    if (self) {
        _score = [decoder decodeDoubleForKey:gameDataScoreKey];
        _level = [decoder decodeDoubleForKey:gameDataLevelKey];
        _riddlesCompleted = [decoder decodeDoubleForKey:gameDataRiddlesCompletedKey];
        _hints = [decoder decodeDoubleForKey:gameDataHintsKey];
        _firstLetters = [decoder decodeDoubleForKey:gameDataFirstLettersKey];
        _answers = [decoder decodeDoubleForKey:gameDataAnswersKey];
        _favourites = [[decoder decodeObjectForKey:favouritesArray] mutableCopy];
    }
    return self;
}

+(instancetype) sharedGameData{
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [self loadInstance];
    });
    return sharedInstance;
}

-(id)init{
    if(self = [super init]){
        _score = 500;
        _riddlesCompleted = 0;
        _level = 1;
        _hints = 3;
        _firstLetters = 3;
        _answers = 3;
    }
    return self;
}

-(void)reset{
    self.score = 500;
    self.level = 1;
    self.riddlesCompleted = 0;
    self.hints = 3;
    self.firstLetters = 3;
    self.answers = 3;
}

+(NSString*)filePath{
    static NSString* filePath = nil;
    if (!filePath) {
        filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES) firstObject]
                    stringByAppendingString:@"gameData"];
    }
    return filePath;
}

+(instancetype)loadInstance{
    NSData* decodeData = [NSData dataWithContentsOfFile:[gameData filePath]];
    if (decodeData) {
        gameData* gameData = [NSKeyedUnarchiver unarchiveObjectWithData:decodeData];
        return gameData;
    }
    return [[gameData alloc] init];
}

-(void)save{
    NSData* encodedData  = [NSKeyedArchiver archivedDataWithRootObject:self];
    [encodedData writeToFile:[gameData filePath] atomically:YES];
}

@end

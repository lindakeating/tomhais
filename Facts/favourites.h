//
//  favourites.h
//  Facts
//
//  Created by Linda Keating on 22/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "favouriteCellView.h"

@interface favourites : UIView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) NSArray *favourites;

-(void) initWithObjects:(NSArray*)scoreResults;

@end

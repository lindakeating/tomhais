//
//  letterSpace.m
//  Facts
//
//  Created by Linda Keating on 28/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "letterSpace.h"
#import "UIColor+ColorExtensions.h"

@implementation letterSpace
-(instancetype) initWithCharacter:(NSString *)character xPosition:(float)x yPosition:(float)y device:(NSString *)device{
    self = [super init];
        if(self){
            
            if ([device isEqualToString:@"IPHONE"]) {
                CGPathRef cPath = [self newRoundedRectPathWithX:0 Y:0 width:30 height:2];
                [self setPath:cPath];
                CFRelease(cPath);
                //[self setPath:CGPathCreateWithRoundedRect(CGRectMake(0, 0, 30, 2), 1, 1, nil)]; // Potential Leak of an object on this line
                _secondLineX = 36*8;
                _secondLineY = -36;
            }else{
                CGPathRef cPath = [self newRoundedRectPathWithX:0 Y:0 width:60 height:3.5];
                [self setPath:cPath];
                CFRelease(cPath);
               // [self setPath:CGPathCreateWithRoundedRect(CGRectMake(0, 0, 60, 3.5), 1, 1, nil)]; // Potential Leak of an object on this line
                _secondLineX = 575;
                _secondLineY = -71;
            }
            self.strokeColor = [UIColor clearColor];
            if ([character  isEqual: @" "]) {
               self.occupied = TRUE;
               self.fillColor = [UIColor clearColor];
               self.character = character;
               self.whiteSpace = TRUE;
            }else{
                self.whiteSpace = FALSE;
                self.occupied = FALSE;
                self.fillColor = [UIColor greenSpacesColor];
            }
            if (y <8) {
                self.position = CGPointMake(x, 0);
            }
            else{
                self.position = CGPointMake(x - _secondLineX, _secondLineY);
            }

            
            self.name = character;
        }
        return self;
    }

-(CGPathRef)newRoundedRectPathWithX:(float)x Y:(float)y width:(float)width height:(float)height{
    CGRect cRect = CGRectMake(x, y, width, height);
    CGPathRef cPath = CGPathCreateWithRoundedRect(cRect, 1, 1, nil);
    return cPath;
}

@end

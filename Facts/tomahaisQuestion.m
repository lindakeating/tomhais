//
//  tomahaisQuestion.m
//  Facts
//
//  Created by Linda Keating on 24/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "tomahaisQuestion.h"

@implementation tomahaisQuestion
-(instancetype) initWithString:(NSString *)tomhais :(float) height :(NSString *)device{
    self = [super init];
    if (self) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        if ([device isEqualToString:@"IPHONE"]) {
            float height = [[UIScreen mainScreen]bounds].size.height;
            CGPathRef cPath = [self newRoundedRectWithX:0 Y:0 width:280 height:height/3.4];
            [self setPath:cPath];
            CFRelease(cPath);
            self.position = CGPointMake(CGRectGetMidX(screenRect) -280/2, CGRectGetMidY(screenRect)-self.frame.size.height/2 + 27);
        }else{
            CGPathRef cPath = [self newRoundedRectWithX:0 Y:0 width:570 height:235];
            [self setPath:cPath];
            CFRelease(cPath);
            //[self setPath:CGPathCreateWithRoundedRect(CGRectMake(0, 0, 570, 235), 16.0f/2.0f, 16.0f/2.0f, nil)];
            self.position = CGPointMake(191.0f/2.0f, 1024-(494.0f));
        }
        
        self.strokeColor = self.fillColor = [UIColor colorWithRed:66.0f/255.0f
                                                            green:74.0f/255.0f
                                                             blue:84.0f/255.0f
                                                            alpha:1];
        
        
        //create drop shadow
        SKShapeNode *shadow = [[SKShapeNode alloc] init];
        if ([device isEqualToString:@"IPHONE"]) {
            CGPathRef cPath = [self newRoundedRectWithX:0 Y:0 width:280 height:140];
            [shadow setPath:cPath];
            CFRelease(cPath);
           // [shadow setPath:CGPathCreateWithRoundedRect(CGRectMake(0, 0, 280, 140), 4, 4, nil)];
        }else{
            CGPathRef cPath = [self newRoundedRectWithX:0 Y:0 width:570 height:235];
            [shadow setPath:cPath];
            CFRelease(cPath);
            //[shadow setPath:CGPathCreateWithRoundedRect(CGRectMake(0, 0, 570, 235), 16.0f/2.0f, 16.0f/2.0f, nil)];
        }
        shadow.strokeColor = shadow.fillColor = [UIColor colorWithRed:0.0f/255.0f
                                                                green:0.0f/255.0f
                                                                 blue:0.0f/255.0f
                                                                alpha:.27];
        shadow.position = CGPointMake(0, -2);
        shadow.zPosition = -2;
        shadow.glowWidth = 2;
        [self addChild:shadow];
        
        //create arrow
      /*  SKShapeNode *arrow = [[SKShapeNode alloc] init];
        [arrow setPath:CGPathCreateWithRoundedRect(CGRectMake(0, 0, 129.2f/2.0f, 129.2f/2.0f), 1, 1, nil)];
        arrow.strokeColor = arrow.fillColor = [UIColor colorWithRed:66.0f/255.0f green:74.0f/255.0f blue:84.0f/255.0f alpha:1];
        arrow.zRotation = M_PI/4.0f;
        arrow.position = CGPointMake(285, -23);*/
        //[self addChild:arrow];
        
        //create arrow Shadow
      /*  SKShapeNode *arrowShadow = [[SKShapeNode alloc] init];
        [arrowShadow setPath:CGPathCreateWithRoundedRect(CGRectMake(0, 0, 129.2f/2.0f, 129.2f/2.0f), 1, 1, nil)];
        arrowShadow.strokeColor = arrowShadow.fillColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:.27f];
        arrowShadow.position = CGPointMake(285, -25);
        arrowShadow.glowWidth = 2;
        arrowShadow.zPosition = -1;
        arrowShadow.zRotation = M_PI/4.0f;*/
       // [self addChild:arrowShadow];

    }
    return self;
}

-(CGPathRef) newRoundedRectWithX:(float)x Y:(float)y width:(float)width height:(float)height{
    CGRect cRect = CGRectMake(x, y, width, height);
    CGPathRef cPath = CGPathCreateWithRoundedRect(cRect, 4, 4, nil);
    return cPath;
}

@end

//
//  favourites.m
//  Facts
//
//  Created by Linda Keating on 22/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "favourites.h"
#import "UIColor+ColorExtensions.h"

@implementation favourites

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void) initWithObjects:(NSArray *)scoreResults {
    self.backgroundColor = [UIColor backgroundColorGrey];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(154, 198, 614, 826)];
    [tableView registerClass:[favouriteCellView class] forCellReuseIdentifier:@"MyReuseIdentifier"];
    tableView.delegate = self;
    tableView.dataSource = self;
    _favourites = scoreResults;
    
    [tableView reloadData];
    
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_favourites count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
    }
    return  cell;
}

@end

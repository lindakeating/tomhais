//
//  lives.h
//  Facts
//
//  Created by Linda Keating on 27/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface lives : SKSpriteNode
    @property NSString* lifeName;
    @property float livesLeft;
    @property SKShapeNode* lifeOneCircle;
    @property SKShapeNode* lifeTwoCircle;
    @property SKShapeNode* lifeThreeCircle;


    -(instancetype) initWithString:(NSString *)lifeLine  lives:(float)live;
    -(void)updateLives:(long)livesLeft;

@end

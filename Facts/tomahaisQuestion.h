//
//  tomahaisQuestion.h
//  Facts
//
//  Created by Linda Keating on 24/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "LnbScene.h"

@interface tomahaisQuestion : SKShapeNode

-(instancetype) initWithString:(NSString *)tomhais :(float)height :(NSString *)device;

@end

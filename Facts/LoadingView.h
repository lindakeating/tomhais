//
//  LoadingView.h
//  Facts
//
//  Created by Linda Keating on 25/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

-(void)setIndicator;

@end

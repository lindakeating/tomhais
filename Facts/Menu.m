//
//  Menu.m
//  Facts
//
//  Created by Linda Keating on 11/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "Menu.h"
#import "gameData.h"
#import "GameViewController.h"
#import "UIColor+ColorExtensions.h"



@implementation Menu{
    NSUserDefaults* defaults;
    NSString *device;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void) initialiseMenu{
    // figure out if it is an IPHONE or an IPAD
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"IPAD";
    }else{
        device = @"IPHONE";
    }
    
    // add a swipe Gesture
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissMenu)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self addGestureRecognizer:swipeRight];
    
    
    defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
    self.backgroundColor = [UIColor backgroundColorGrey];
    self.layer.shadowColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1].CGColor;
    self.layer.shadowOffset = CGSizeMake(-3.5, 0);
    self.layer.shadowRadius = 7.0f;
    self.layer.shadowOpacity = .5f;
    self.layer.masksToBounds = NO;
    
    //textAttributesDirectory
    NSMutableDictionary *attributesDictionaty = [NSMutableDictionary dictionary];
    [attributesDictionaty setObject:@-1 forKey:NSStrokeWidthAttributeName];
    if ([device isEqualToString:@"IPHONE"]) {
        [attributesDictionaty setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:12] forKey:NSFontAttributeName];
    }else{
        [attributesDictionaty setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:24] forKey:NSFontAttributeName];
    }
    [attributesDictionaty setObject:[UIColor textGreyColor] forKey:NSForegroundColorAttributeName];
    [attributesDictionaty setObject:[UIColor textGreyColor] forKey:NSStrokeColorAttributeName];
    
    // load Profile Picture
    UIImageView *profilePic;
    if([device isEqualToString:@"IPHONE"]){
        profilePic = [[UIImageView alloc] initWithFrame:CGRectMake(24, 40, 55, 55)];
        profilePic.layer.cornerRadius = 22.5f;
    }else{
        profilePic = [[UIImageView alloc] initWithFrame:CGRectMake(52, 73, 100, 100)];
        profilePic.layer.cornerRadius = 50.0f;
    }
    
    profilePic.clipsToBounds = YES;
    NSString * documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/profilePic.png", documentsDirectory];
    
    BOOL fileExists = [[NSFileManager defaultManager]fileExistsAtPath:filePath];
    if (!fileExists) {
        [profilePic setImage:[UIImage imageNamed:@"profilePic.png"]];
    }else{
        [profilePic setImage:[UIImage imageWithContentsOfFile:filePath]];
    }
    
    [self addSubview:profilePic];
    
    //load Achievement Icon
    UIImageView *achievementIcon;
    if ([device isEqualToString:@"IPHONE"]) {
        achievementIcon = [[UIImageView alloc] initWithFrame:CGRectMake(24, 112, 55, 55)];
    }else{
       achievementIcon = [[UIImageView alloc] initWithFrame:CGRectMake(52, 199, 100, 100)];
    }
    
    [achievementIcon setImage:[UIImage imageNamed:@"acorn.png"]];
    [self addSubview:achievementIcon];
    
    //load UserName
    UITextView *userName;
    if ([device isEqualToString:@"IPHONE"]) {
        userName = [[UITextView alloc] initWithFrame:CGRectMake(90, 40, 98, 25)];
    }else{
        userName = [[UITextView alloc] initWithFrame:CGRectMake(179, 83, 210, 33)];
    }
    
    userName.backgroundColor = [UIColor clearColor];
    //TODO: add if statement to check if user is already registered
    NSMutableAttributedString *userNameText = [[NSMutableAttributedString alloc] init];

    if([[[defaults dictionaryRepresentation]allKeys]containsObject:@"userId"]){
        NSAttributedString *string = [[NSAttributedString alloc] initWithString:[defaults valueForKey:@"userName"]];
        [userNameText setAttributedString:string];
        
    }else{
        NSAttributedString *string = [[NSAttributedString alloc] initWithString:@"Tomhais"];
        [userNameText setAttributedString:string];
    }
    
    NSRange userNameLength = NSMakeRange(0, userNameText.length);
    [userNameText addAttribute:NSForegroundColorAttributeName value:[UIColor textGreyColor] range:userNameLength];
    if ([device isEqualToString:@"IPHONE"]) {
        [userNameText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AvenirNext-Medium" size:13] range:userNameLength];
    }else{
        [userNameText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AvenirNext-Medium" size:24] range:userNameLength];
    }
    
    [userNameText addAttribute:NSStrokeWidthAttributeName value:@-1 range:userNameLength];
    [userNameText addAttribute:NSStrokeColorAttributeName value:[UIColor textGreyColor] range:userNameLength];
    userName.attributedText = userNameText;
    userName.editable = NO;
    [self addSubview:userName];
    
    //load Achievement Title
    UITextView *achievementStatus;
    if ([device isEqualToString:@"IPHONE"]) {
        achievementStatus = [[UITextView alloc] initWithFrame:CGRectMake(90, 59, 77, 18)];
    }else{
        achievementStatus = [[UITextView alloc] initWithFrame:CGRectMake(179, 117, 165, 30)];
    }
    achievementStatus.backgroundColor = [UIColor clearColor];
    //TODO: add in string from NSDefaults details

    NSMutableAttributedString *achievementStatusText = [[NSMutableAttributedString alloc] initWithString:@"Ollaire" ];
    NSRange achievementTextLength = NSMakeRange(0, achievementStatusText.length);
    if ([device isEqualToString:@"IPHONE"]) {
         [achievementStatusText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AvenirNext-Regular" size:9] range:achievementTextLength];
    }else{
         [achievementStatusText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AvenirNext-Regular" size:18] range:achievementTextLength];
    }
   
    [achievementStatusText addAttribute:NSForegroundColorAttributeName value:[UIColor textGreyColor] range:achievementTextLength];
    achievementStatus.attributedText = achievementStatusText;
    achievementStatus.editable = NO;
    [self addSubview:achievementStatus];
    
    // load Points
    UITextView *pointsStatus;
    NSMutableAttributedString *pointText = [[NSMutableAttributedString  alloc] initWithString:[NSString stringWithFormat:@"%li", [gameData sharedGameData].score]];
    NSRange pointsTextLength = NSMakeRange(0, pointText.length);
    if ([device isEqualToString:@"IPHONE"]) {
        pointsStatus = [[UITextView alloc] initWithFrame:CGRectMake(90, 73, 77, 18)];
        [pointText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AvenirNext-Regular" size:10] range:pointsTextLength];
        
    }else{
        pointsStatus = [[UITextView alloc] initWithFrame:CGRectMake(179, 137, 165, 30)];
        [pointText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"AvenirNext-Regular" size:18] range:pointsTextLength];
    }
    pointsStatus.backgroundColor = [UIColor clearColor];
    [pointText addAttribute:NSForegroundColorAttributeName value:[UIColor textGreyColor] range:pointsTextLength];
    pointsStatus.attributedText = pointText;
    [self addSubview:pointsStatus];
    
    //add Bar
    UIView *line;
    if ([device isEqualToString:@"IPHONE"]) {
        line = [[UIView alloc] initWithFrame:CGRectMake(20, 190, 175, 2)];
    }else{
        line = [[UIView alloc] initWithFrame:CGRectMake(62, 345, 362, 3)];
    }
    line.backgroundColor = [UIColor horizontalGreyColor];
    [self addSubview:line];
    
    
    //Home Text
    UITextView *homeTextView;
    if ([device isEqualToString:@"IPHONE"]) {
        homeTextView = [[UITextView alloc] initWithFrame:CGRectMake(90, 211, 106, 24)];
    }else{
        homeTextView = [[UITextView alloc] initWithFrame:CGRectMake(178, 393, 227, 33)];
    }
    homeTextView.backgroundColor = [UIColor clearColor];
    NSMutableAttributedString *homeText = [[NSMutableAttributedString alloc] initWithString:@"Baile" attributes:attributesDictionaty];
    [homeTextView setAttributedText:homeText];
    homeTextView.editable = NO;
    [self addSubview:homeTextView];
    
    //Favourites Text
    UITextView *favouritesTextView;
    if ([device isEqualToString:@"IPHONE"]) {
        favouritesTextView = [[UITextView alloc] initWithFrame:CGRectMake(90, 259, 106, 24)];
    }else{
        favouritesTextView = [[UITextView alloc] initWithFrame:CGRectMake(178, 480, 227, 33)];
    }
    favouritesTextView.backgroundColor = [UIColor clearColor];
    NSMutableAttributedString *favouritesText = [[NSMutableAttributedString alloc] initWithString:@"Ceanáin" attributes:attributesDictionaty];
    [favouritesTextView setAttributedText:favouritesText];
    favouritesTextView.editable = NO;
    [self addSubview:favouritesTextView];
    
    //Winners Board
    UITextView *leaderBoardTextview;
    if ([device isEqualToString:@"IPHONE"]) {
        leaderBoardTextview = [[UITextView alloc] initWithFrame:CGRectMake(90, 305, 117, 24)];
    }else{
        leaderBoardTextview = [[UITextView alloc] initWithFrame:CGRectMake(179, 563, 227, 33)];
    }
    leaderBoardTextview.backgroundColor = [UIColor clearColor];
    NSMutableAttributedString *leaderText = [[NSMutableAttributedString alloc] initWithString:@"Clár na Saoithe" attributes:attributesDictionaty];
    [leaderBoardTextview setAttributedText:leaderText];
    leaderBoardTextview.editable = NO;
    [self addSubview:leaderBoardTextview];
    
    //Hints Text
    UITextView *hintsTextView;
    if ([device isEqualToString:@"IPHONE"]) {
        hintsTextView = [[UITextView alloc] initWithFrame:CGRectMake(90, 354, 106, 24)];
    }else{
        hintsTextView = [[UITextView alloc] initWithFrame:CGRectMake(178, 649, 227, 33)];
    }
    hintsTextView.backgroundColor = [UIColor clearColor];
    NSMutableAttributedString *hintsText = [[NSMutableAttributedString alloc] initWithString:@"Línte Tharrthála" attributes:attributesDictionaty];
    [hintsTextView setAttributedText:hintsText];
    hintsTextView.editable = NO;
    [self addSubview:hintsTextView];
    
    //Share Text
    UITextView *shareView;
    if ([device isEqualToString:@"IPHONE"]) {
        shareView = [[UITextView alloc]initWithFrame:CGRectMake(90, 399, 106, 24)];
    }else{
        shareView = [[UITextView alloc]initWithFrame:CGRectMake(178, 733, 227, 33)];
    }
    shareView.backgroundColor = [UIColor clearColor];
    NSMutableAttributedString *shareText = [[NSMutableAttributedString alloc] initWithString:@"Roinn" attributes:attributesDictionaty];
    [shareView setAttributedText:shareText];
    shareView.editable = NO;
    [self addSubview:shareView];
    
    //iconAttributesDirectory
    NSMutableDictionary *iconAttributes = [NSMutableDictionary dictionary];
    if ([device isEqualToString:@"IPHONE"]) {
        [iconAttributes setObject:[UIFont fontWithName:@"tomhais-icons" size:30] forKey:NSFontAttributeName];
    }else{
        [iconAttributes setObject:[UIFont fontWithName:@"tomhais-icons" size:60] forKey:NSFontAttributeName];
    }
    [iconAttributes setObject:[UIColor textGreyColor] forKey:NSForegroundColorAttributeName];
    NSShadow *shadow = [[NSShadow alloc] init];
    [shadow setShadowBlurRadius:2.0f];
    [shadow setShadowColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:.5]];
    [shadow setShadowOffset:CGSizeMake(0, 2.0f)];
    [iconAttributes setObject:shadow forKey:NSShadowAttributeName];
    
    //Home Icon
    UIButton *homeIconButton;
    if ([device isEqualToString:@"IPHONE"]) {
        homeIconButton = [[UIButton alloc] initWithFrame:CGRectMake(36, 211, 30, 30)];
    }else{
        homeIconButton = [[UIButton alloc] initWithFrame:CGRectMake(75, 379, 68, 70)];
    }
     NSMutableAttributedString *homeIconText = [[NSMutableAttributedString alloc] initWithString:@"h" attributes:iconAttributes];
    [homeIconButton setAttributedTitle:homeIconText forState:UIControlStateNormal];
    [homeIconButton addTarget:self.window.rootViewController action:@selector(goToHomeScreen) forControlEvents:UIControlEventTouchUpInside];
    [homeIconButton addTarget:self action:@selector(scaleButton:) forControlEvents:UIControlEventTouchDown];
    [self addSubview:homeIconButton];
    
    //Favourites Icon
    UIButton *favouritesButton;
    if ([device isEqualToString:@"IPHONE"]) {
        favouritesButton = [[UIButton alloc] initWithFrame:CGRectMake(36, 260, 30, 30)];
    }else{
        favouritesButton = [[UIButton alloc] initWithFrame:CGRectMake(75, 465, 68, 65)];
    }
    NSMutableAttributedString *favouritesIcon = [[NSMutableAttributedString alloc] initWithString:@"e" attributes:iconAttributes];
   // NSMutableAttributedString *favouritesIcon = [[NSMutableAttributedString alloc] initWithString:@"\"" attributes:iconAttributes];
    [favouritesButton setAttributedTitle:favouritesIcon forState:UIControlStateNormal];
    [favouritesButton addTarget:self.window.rootViewController action:@selector(goToFavourites) forControlEvents:UIControlEventTouchUpInside];
    [favouritesButton addTarget:self action:@selector(scaleButton:) forControlEvents:UIControlEventTouchDown];
    [self addSubview:favouritesButton];
    
    //Tropy Icon
    UIButton *trophyButton;
    if ([device isEqualToString:@"IPHONE"]) {
        trophyButton = [[UIButton alloc] initWithFrame:CGRectMake(36, 309, 30, 30)];
    }else{
        trophyButton = [[UIButton alloc] initWithFrame:CGRectMake(75, 549, 68, 70)];
    }
    NSMutableAttributedString *trophyIcon = [[NSMutableAttributedString alloc] initWithString:@"b" attributes:iconAttributes];
   // NSMutableAttributedString *trophyIcon = [[NSMutableAttributedString alloc] initWithString:@"v" attributes:iconAttributes];
    [trophyButton setAttributedTitle:trophyIcon forState:UIControlStateNormal];
    [trophyButton addTarget:self.window.rootViewController action:@selector(goToLeaderBoard) forControlEvents:UIControlEventTouchUpInside];
    [trophyButton addTarget:self action:@selector(scaleButton:) forControlEvents:UIControlEventTouchDown];
    [self addSubview:trophyButton];
    
    //LifeLines Icon
    UIButton *hintsButton;
    if ([device isEqualToString:@"IPHONE"]) {
        hintsButton = [[UIButton alloc] initWithFrame:CGRectMake(36, 356, 30, 30)];
    }else{
        hintsButton = [[UIButton alloc] initWithFrame:CGRectMake(75, 638, 68, 70)];
    }
    NSMutableAttributedString *lifeLineText = [[NSMutableAttributedString alloc] initWithString:@"h"];
    NSRange iconLength = NSMakeRange(0, 1);
    [lifeLineText addAttribute:NSForegroundColorAttributeName value:[UIColor textGreyColor] range:NSMakeRange(0,1)];
    if ([device isEqualToString:@"IPHONE"]) {
        [lifeLineText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"hint-icon" size:27] range:iconLength];
    }else{
        [lifeLineText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"hint-icon" size:55] range:iconLength];
    }
    [lifeLineText addAttribute:NSShadowAttributeName value:shadow range:iconLength];
    [hintsButton setAttributedTitle:lifeLineText forState:UIControlStateNormal];
    [hintsButton addTarget:self.window.rootViewController action:@selector(goToLifelines) forControlEvents:UIControlEventTouchUpInside];
    [hintsButton addTarget:self action:@selector(scaleButton:) forControlEvents:UIControlEventTouchDown];
    [self addSubview:hintsButton];
    
    //Share Icon
    UIButton *shareButton;
    if ([device isEqualToString:@"IPHONE"]) {
        shareButton = [[UIButton alloc] initWithFrame:CGRectMake(36, 403, 30, 30) ];
    }else{
        shareButton = [[UIButton alloc] initWithFrame:CGRectMake(75, 718, 68, 65) ];
    }
    shareButton.backgroundColor = [UIColor clearColor];
    NSMutableAttributedString *shareIconString = [[NSMutableAttributedString alloc] initWithString:@"a" attributes:iconAttributes];
    [shareButton setAttributedTitle:shareIconString forState:UIControlStateNormal];
    [shareButton addTarget:self.window.rootViewController action:@selector(shareScreen) forControlEvents:UIControlEventTouchUpInside];
    [shareButton addTarget:self action:@selector(scaleButton:) forControlEvents:UIControlEventTouchDown];
    [self addSubview:shareButton];
    
    if ([device isEqualToString:@"IPHONE"]) {
        [UIView animateWithDuration:.6f animations:^{
            [self setFrame:CGRectMake(105, 18, 216, 549)];
        }];
    }else{
        [UIView animateWithDuration:.6f animations:^{
            [self setFrame:CGRectMake(313, 33, 460, 992)];
        }];
    } 
}

-(void) dismissMenu{
    [(GameViewController *)self.window.rootViewController unloadMenu];
}

-(void)scaleButton:(UIButton*)button{
    [UIView animateKeyframesWithDuration:0.5 delay:0.0 options:UIViewKeyframeAnimationOptionCalculationModePaced animations:^{
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:.25 animations:^{
            button.transform = CGAffineTransformScale(button.transform, 1.3f, 1.3f);
        }];
        [UIView addKeyframeWithRelativeStartTime:0.8 relativeDuration:0.2 animations:^{
            button.transform = CGAffineTransformScale(button.transform, 0.8f, 0.8f);
        }];
    } completion:^(BOOL finished) {
        NSLog(@"Animation complete!");
    }];
}

@end

//
//  wrongAnswerNotification.h
//  Facts
//
//  Created by Linda Keating on 09/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LnbScene.h"

@interface wrongAnswerNotification : UIView

-(void)loadWrongAnswerNotification;

@end

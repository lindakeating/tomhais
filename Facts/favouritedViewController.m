//
//  favouritedViewController.m
//  Facts
//
//  Created by Linda Keating on 28/05/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "favouritedViewController.h"

#import "DBManager.h"
#import "favouritedCell.h"
#import "iphoneFavouritedCell.h"

#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)
#define IDIOM UI_USER_INTERFACE_IDIOM()
#define IPHONE UIUserInterfaceIdiomPhone

@interface favouritedViewController ()


@property (strong, nonatomic) NSArray *favourites;
@property (nonatomic, strong) DBManager *dbManager;
typedef void (^CompletionBlock)();

-(void)loadData;
-(void)reloadDataWithCompletions:(CompletionBlock)completionBlock;

@end

@implementation favouritedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"tomhaisdb.sql"];
    self.tableView.backgroundColor = [UIColor colorWithRed:38.0f/255.0f green:42.0f/255.0f blue:46.0f/255.0f alpha:1];
    [self loadData];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self deselectAllRows];
}

-(void)viewDidAppear:(BOOL)animated{
    [self.tableView reloadData];
}

-(void)deselectAllRows{
    for (NSIndexPath * indexPath in [self.tableView indexPathsForSelectedRows]) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadData {
    NSString *query = @"select * from favourites";
    
    if (self.favourites != nil) {
        self.favourites = nil;
    }
    
    self.favourites = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
   /* [self reloadDataWithCompletions:^{
         self.tableView.backgroundColor = [UIColor colorWithRed:28.0f/255.0f green:30.0f/255.0f blue:35.0f/255.0f alpha:1];
    }];*/
    [self reloadTableViewContent];
}

- (void)reloadTableViewContent {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    });
}

/*-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}*/

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.favourites.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IDIOM == IPHONE ) {
        return [self iphoneCellAtIndexPath:indexPath];
    }else{
        return [self basicCellAtIndexPath:indexPath];
    }
}

-(iphoneFavouritedCell *) iphoneCellAtIndexPath:(NSIndexPath *)indexPath{
    iphoneFavouritedCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"iphoneCell" forIndexPath:indexPath];
    [self configureIPhoneCell:cell atIndexPath:indexPath];
    [cell.answer setTextColor:[UIColor colorWithRed:150.0f/255.0f green:166.0f/255.0f blue:173.0f/255.0f alpha:1]];
    return cell;
}

-(favouritedCell *)basicCellAtIndexPath:(NSIndexPath *)indexPath {
    favouritedCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"favouriteCell" forIndexPath:indexPath];
    [self configureBasicCell:cell atIndexPath:indexPath];
    [cell.favouriteText setTextColor:[UIColor colorWithRed:216.0f/255.0f green:216.0f/255.0f blue:216.0f/255.0f alpha:1]];
    [cell.answer setTextColor:[UIColor colorWithRed:150.0f/255.0f green:166.0f/255.0f blue:173.0f/255.0f alpha:1]];
    return cell;
}

-(void)configureIPhoneCell:(iphoneFavouritedCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    NSInteger indexOfTomhaisText = [self.dbManager.arrColumnNames indexOfObject:@"tomhaisText"];
    NSString *tomhaisText = [[self.favourites objectAtIndex:indexPath.row] objectAtIndex:indexOfTomhaisText];
    [self setTomhaisForiPhoneCell:cell item:tomhaisText];
    [self setAnswerForiPhoneCell:cell item:tomhaisText];
}

-(void)configureBasicCell:(favouritedCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    NSInteger indexOfTomhaisText = [self.dbManager.arrColumnNames indexOfObject:@"tomhaisText"];
    NSString *tomhaisText = [[self.favourites objectAtIndex:indexPath.row] objectAtIndex:indexOfTomhaisText];
    [self setTomhaisForCell:cell item:tomhaisText];
    [self setAnswerForCell:cell item:tomhaisText]; // change this later
}

-(void)setTomhaisForiPhoneCell:(iphoneFavouritedCell *)cell item:(NSString *)item{
    [cell.question setText:item];
}

-(void)setAnswerForiPhoneCell:(iphoneFavouritedCell *)cell item:(NSString *)item{
    [cell.answer setText:item];
}

-(void)setTomhaisForCell:(favouritedCell *)cell item:(NSString *)item{

    [cell.favouriteText setText:item];
}

-(void)setAnswerForCell:(favouritedCell *)cell item:(NSString *)item{

    [cell.answer setText:item];
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
 if (IS_IOS8) {
        tableView.estimatedRowHeight = 82;
        return UITableViewAutomaticDimension;
    }else{
        return [self heightForFavouriteCellAtIndexPath:indexPath];
  }
    
}

-(CGFloat)heightForFavouriteCellAtIndexPath:(NSIndexPath *)indexPath{

    if (IDIOM == IPHONE) {
        static iphoneFavouritedCell *sizingCell = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sizingCell = [self.tableView dequeueReusableCellWithIdentifier:@"iphoneCell"];
        });
        [self configureIPhoneCell:sizingCell atIndexPath:indexPath];
        return [self calculateHeightForConfiguredSizingCell:sizingCell];

    }else{
        static favouritedCell *sizingCell = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sizingCell = [self.tableView dequeueReusableCellWithIdentifier:@"favouriteCell"];
        });
        [self configureBasicCell:sizingCell atIndexPath:indexPath];
        return [self calculateHeightForConfiguredSizingCell:sizingCell];
    }

}

-(CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell{
    sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableView.frame), CGRectGetHeight(sizingCell.bounds));
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    NSLog(@"Bounds after layout %@", NSStringFromCGRect(sizingCell.bounds));
    
    return size.height + 1.0f;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 155.0f;
}


/*

-(favouritedCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    favouritedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"favouriteCell" forIndexPath:indexPath];
    NSInteger indexOfTomhaisText = [self.dbManager.arrColumnNames indexOfObject:@"tomhaisText"];
    cell.favouriteText.text = [NSString stringWithFormat:@"%@", [[self.favourites objectAtIndex:indexPath.row] objectAtIndex:indexOfTomhaisText]];
    
    return cell;
    
}
 
 */

-(void)reloadDataWithCompletions:(CompletionBlock)completionBlock{
    [self.tableView reloadData];
    completionBlock();
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

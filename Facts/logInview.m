//
//  logInview.m
//  Facts
//
//  Created by Linda Keating on 06/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "logInview.h"
#import "GameViewController.h"
#import "UIColor+ColorExtensions.h"
#include <Accelerate/Accelerate.h>
#import "FXBlurView.h"

@implementation logInview{
    UIView *imageView;
    UIImage *picture;
    UIView *flippedView;
    NSString *device;
    CGPoint originalCenter;
    CGRect screen;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
typedef enum {
    kUserName,
    kUserPassword
} Fields;

-(void)loadLogInView {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidChangeFrameNotification object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow) name:UIKeyboardDidShowNotification object:nil];
    
    // figure out if it is an IPHONE or an IPAD
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"IPAD";
    }else{
        device = @"IPHONE";
    }
    
    screen = [[UIScreen mainScreen] bounds];
    
    originalCenter = self.center;
    
    _warningAttributes = [NSMutableDictionary dictionary];
    [_warningAttributes setObject:[UIFont fontWithName:@"AvenirNext-DemiBold" size:12] forKey:NSFontAttributeName];
    [_warningAttributes setObject:[UIColor warningRedColor] forKey:NSForegroundColorAttributeName];
    

        // BLUR View
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        
        UIToolbar *fakeToolbar;
        
        if ([device isEqualToString:@"IPHONE"]) {
            fakeToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 285, screen.size.height / 1.1428571428714)];
        }else{
            fakeToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 637, 867)];
        }
        fakeToolbar.autoresizingMask = self.autoresizingMask;
        fakeToolbar.barStyle = UIBarStyleBlack;
        fakeToolbar.translucent = YES;
        fakeToolbar.layer.cornerRadius = 10.0f;
        fakeToolbar.layer.masksToBounds = YES;
        [self insertSubview:fakeToolbar atIndex:0];
  
    
   /* FXBlurView *blurView;
    
    if ([device isEqualToString:@"IPHONE"]) {
        blurView = [[FXBlurView alloc] initWithFrame:CGRectMake(0, 0, 285, screen.size.height / 1.1428571428714)];
    }else{
        blurView = [[FXBlurView alloc] initWithFrame:CGRectMake(0, 0, 637, 867)];
    }
    
    blurView.blurRadius = 30.f;
    blurView.tintColor = [UIColor clearColor];
    blurView.dynamic = NO;
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    blurView.underlyingView = window.rootViewController.view;
    blurView.alpha = 0.8f;
    [self addSubview:blurView];*/
    
    
    //clarú
    UITextView *claru;
    NSMutableDictionary *claruAttributes = [NSMutableDictionary dictionary];
    if ([device isEqualToString:@"IPHONE"]) {
        claru = [[UITextView alloc] initWithFrame:CGRectMake(75, 38, 130, 20)];
        [claruAttributes setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:12] forKey:NSFontAttributeName];
    }else{
        claru = [[UITextView alloc] initWithFrame:CGRectMake(170, 105, 297, 30)];
        [claruAttributes setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:18] forKey:NSFontAttributeName];
    }
    claru.backgroundColor = [UIColor clearColor];
    [claruAttributes setObject:[UIColor textGreyColor]  forKey:NSForegroundColorAttributeName];
    [claruAttributes setObject:@(4.5f) forKey:NSKernAttributeName];
    NSMutableParagraphStyle *center = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    center.alignment = NSTextAlignmentCenter;
    [claruAttributes setObject:center forKey:NSParagraphStyleAttributeName];
    NSMutableAttributedString *claruText = [[NSMutableAttributedString alloc] initWithString:@"CLARÚ" attributes:claruAttributes];
    [claru setAttributedText:claruText];
    claru.editable = NO;
    [self addSubview:claru];
    
    
    //description text
    UITextView *description;
    NSMutableDictionary *descriptionAttributes = [NSMutableDictionary dictionary];
    if ([device isEqualToString:@"IPHONE"]) {
        description = [[UITextView alloc] initWithFrame:CGRectMake(35, 67, 211, 68)];
        [descriptionAttributes setObject:[UIFont fontWithName:@"AvenirNext-UltraLight" size:15] forKey:NSFontAttributeName ];
    }else{
       description = [[UITextView alloc] initWithFrame:CGRectMake(78, 157, 480, 55)];
       [descriptionAttributes setObject:[UIFont fontWithName:@"AvenirNext-UltraLight" size:18] forKey:NSFontAttributeName ];
    }
    description.backgroundColor = [UIColor clearColor];
    [descriptionAttributes setObject:[UIColor textGreyColor] forKey:NSForegroundColorAttributeName];
    [descriptionAttributes setObject:center forKey:NSParagraphStyleAttributeName];
    NSMutableAttributedString *descText = [[NSMutableAttributedString alloc] initWithString:@"Le do scór a fheiceáil ar Chlár na mBuaiteoirí, beidh ort cuntas imreora a chruthú" attributes:descriptionAttributes];
    [description setAttributedText:descText];
    description.editable = NO;
    [self addSubview:description];
    
    //Upload Image
    if ([device isEqualToString:@"IPHONE"]) {
        imageView = [[UIView alloc] initWithFrame:CGRectMake(101, 157, 80, 80)];
        imageView.layer.cornerRadius = 40;
    }else{
        imageView = [[UIView alloc] initWithFrame:CGRectMake(246, 230, 145, 145)];
        imageView.layer.cornerRadius = 72.5;
    }
    
    imageView.layer.backgroundColor = [UIColor greenSpacesColor].CGColor;
    
    UIImage *btnImage = [UIImage imageNamed:@"camera_icon.png"];
    UIButton *cameraButton;
    if ([device isEqualToString:@"IPHONE"]) {
         cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(26, 24, 26.5, 20.5)];
    }else{
         cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(48.2, 43.7, btnImage.size.width, btnImage.size.height)];
    }
    [cameraButton setImage:btnImage forState:UIControlStateNormal];
    [cameraButton addTarget:self action:@selector(flipView) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:cameraButton];
    
    
    UITextView *iomha;
    NSMutableDictionary *iomhaAttr = [NSMutableDictionary dictionary];
    if ([device isEqualToString:@"IPHONE"]) {
        iomha = [[UITextView alloc] initWithFrame:CGRectMake(14, 45, 52.2, 19.5)];
        [iomhaAttr setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:9.26] forKey:NSFontAttributeName];
    }else{
        iomha = [[UITextView alloc] initWithFrame:CGRectMake(26.1f, 86.8, 93, 19.5)];
        [iomhaAttr setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:9.26] forKey:NSFontAttributeName];
    }
    [iomha setBackgroundColor:[UIColor clearColor]];
    [iomhaAttr setObject:[UIColor colorWithRed:14.0f/255.0f green:19.0f/255.0f blue:35.0f/255.0f alpha:1] forKey:NSForegroundColorAttributeName];
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paraStyle.alignment = NSTextAlignmentCenter;
    [iomhaAttr setObject:@(1.3f) forKey:NSKernAttributeName];
    [iomhaAttr setObject:paraStyle forKey:NSParagraphStyleAttributeName];
    NSMutableAttributedString *iomhaString = [[NSMutableAttributedString alloc] initWithString:@"ÍOMHA" attributes:iomhaAttr];
    [iomha setAttributedText:iomhaString];
    iomha.editable = NO;
    [imageView addSubview:iomha];
    [self addSubview:imageView];
    
    //Flipped View
    if ([device isEqualToString:@"IPHONE"]) {
        flippedView = [[UIView alloc] initWithFrame:CGRectMake(35, 181, 213, 30)];
    }else{
        flippedView = [[UIView alloc] initWithFrame:CGRectMake(143, 266, 349, 55)];
    }

    flippedView.hidden = YES;
    
    UIButton *uploadPic;
    if ([device isEqualToString:@"IPHONE"]) {
        uploadPic = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 97, 30)];
    }else{
        uploadPic = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 162, 48)];
    }
    uploadPic.backgroundColor = [UIColor greenSpacesColor];
    uploadPic.layer.cornerRadius = 2.0f;
    [uploadPic addTarget:self action:@selector(takePicture) forControlEvents:UIControlEventTouchUpInside];
    [uploadPic setImage:btnImage forState:UIControlStateNormal];
    [flippedView addSubview:uploadPic];
    [self addSubview:flippedView];
    
    UIButton *selectPic;
    if ([device isEqualToString:@"IPHONE"]) {
        selectPic = [[UIButton alloc] initWithFrame:CGRectMake(110, 0, 97, 30)];
    }else{
        selectPic = [[UIButton alloc] initWithFrame:CGRectMake(178, 0, 162, 48)];
    }

    selectPic.backgroundColor =  [UIColor greenSpacesColor];
    selectPic.layer.cornerRadius = 2.0f;
    [selectPic addTarget:self action:@selector(selectPicture) forControlEvents:UIControlEventTouchUpInside];
    [selectPic setImage:btnImage forState:UIControlStateNormal];
    [flippedView addSubview:selectPic];
    
    //UserName entry box
    NSAttributedString *ainmnPlaceHolder;
    if ([device isEqualToString:@"IPHONE"]) {
        _userName = [[UITextField alloc] initWithFrame:CGRectMake(34.5, 256, 212, 27)];
         _userName.layer.borderWidth = 1.5f;
        _userName.layer.cornerRadius = 2.0f;
    }else{
       _userName = [[UITextField alloc] initWithFrame:CGRectMake(148, 402, 341, 48)];
       _userName.layer.borderWidth = 3.0f;
      _userName.layer.cornerRadius = 4.0f;
    }
    ainmnPlaceHolder = [[NSAttributedString alloc]initWithString:@"Ainm Imreora"
                                                      attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:80.0f/255.0f
                                                                                                                  green:227.0f/255.0f
                                                                                                                   blue:194.0f/255.0f
                                                                                                                  alpha:.5],
                                                                   NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-DemiBold"
                                                                                                       size:12]}];
    _userName.tag = kUserName;
    _userName.borderStyle = UITextBorderStyleLine;
    _userName.layer.borderColor = [[UIColor greenSpacesColor] CGColor];
    [_userName setBackgroundColor:[UIColor colorWithRed:26.0f/255.0f green:29.0f/255.0f blue:33.0f/255.0f alpha:1]];
    [_userName setAttributedPlaceholder:ainmnPlaceHolder];
    [_userName setTextColor:[UIColor greenSpacesColor]];
    _userName.layer.sublayerTransform = CATransform3DMakeTranslation(10, 2, 2);
    [_userName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_userName setDelegate:self];
    [self addSubview:_userName];
    
    //UserName warning Box
    if ([device isEqualToString:@"IPHONE"]) {
        _userNameWarning = [[UITextField alloc] initWithFrame:CGRectMake(35, 285, 200, 16)];
    }else{
        _userNameWarning = [[UITextField alloc] initWithFrame:CGRectMake(154, 455, 268, 16)];
    }
    [self addSubview:_userNameWarning];
    
    //Password entry box
    if ([device isEqualToString:@"IPHONE"]) {
        _userPassword = [[UITextField alloc] initWithFrame:CGRectMake(35, 300, 212, 27)];
        _userPassword.layer.borderWidth = 1.5f;
    }else{
        _userPassword = [[UITextField alloc] initWithFrame:CGRectMake(148, 477, 341, 49)];
        _userPassword.layer.borderWidth = 3.0f;
    }
    _userPassword.tag = kUserPassword;
    [_userPassword setBackgroundColor:[UIColor colorWithRed:26.0f/255.0f green:29.0f/255.0f blue:33.0f/255.0f alpha:1]];
    _userPassword.borderStyle = UITextBorderStyleLine;
    _userPassword.layer.borderColor = [[UIColor greenSpacesColor] CGColor];
    NSAttributedString *passwordPlaceHolder = [[NSAttributedString alloc]initWithString:@"Pasfhocal"
                                                                             attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:80.0f/255.0f
                                                                                                                                         green:227.0f/255.0f
                                                                                                                                          blue:194.0f/255.0f
                                                                                                                                         alpha:.5],
                                                                                                      NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-DemiBold"
                                                                                                                                          size:12]}];
    [_userPassword setAttributedPlaceholder:passwordPlaceHolder];
    _userPassword.layer.sublayerTransform = CATransform3DMakeTranslation(10, 2, 2);
    [_userPassword setTextColor:[UIColor greenSpacesColor]];
    _userPassword.layer.cornerRadius = 4.0f;
    [_userPassword addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _userPassword.secureTextEntry = YES;
    [_userPassword setDelegate:self];
    [self addSubview:_userPassword];
    
    if ([device isEqualToString:@"IPHONE"]) {
        _userPasswordWarning = [[UITextField alloc] initWithFrame:CGRectMake(35, 330, 200, 16)];
    }else{
        _userPasswordWarning = [[UITextField alloc] initWithFrame:CGRectMake(154, 531, 268, 16)];
    }
    [self addSubview:_userPasswordWarning];
    
    //Submit Button
    if ([device isEqualToString:@"IPHONE"]) {
        _submitButton = [[UIButton alloc] initWithFrame:CGRectMake(50, 347, 184, 30)];
    }else{
        _submitButton = [[UIButton alloc] initWithFrame:CGRectMake(207, 556, 213.5, 40.8)];
    }

    [_submitButton setBackgroundColor:[UIColor greenSpacesColor]];
    NSMutableAttributedString *seol = [[NSMutableAttributedString alloc] initWithString:@"SEOL"
                                                                            attributes:@{
                                                                                         NSForegroundColorAttributeName:[UIColor foregroundColorGrey],
                                                                                         NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Bold" size:11.15]
                                                                                         }];
    [_submitButton setAttributedTitle:seol forState:UIControlStateNormal];
    _submitButton.layer.cornerRadius = 4.0f;
    [_submitButton addTarget:self action:@selector(submitUser) forControlEvents:UIControlEventTouchUpInside];
     _submitButton.userInteractionEnabled = NO;
    [self addSubview:_submitButton];
    
    if ([device isEqualToString:@"IPHONE"]) {
        [self setFrame:CGRectMake(20, -481, 280, 481)];
    }else{
        [self setFrame:CGRectMake(109/2, -1734/2, 1291/2, 1734/2)];
    }
    
   [UIView animateWithDuration:0.6f
                         delay:0.1f
                       options:UIViewAnimationOptionCurveEaseInOut
                    animations:^{
                        if ([device isEqualToString:@"IPHONE"]) {
                            [self setFrame:CGRectMake(19, 56, 285, screen.size.height)];
                        }else{
                            [self setFrame:CGRectMake(109/2, 222/2, 1291/2, 1734/2)];
                        }
                        [self setAlpha:1.0f];
                    }
                    completion:^(BOOL finished){
                      // self.backgroundColor = [UIColor colorWithRed:38.0f/255.0f green:42.0f/255.0f blue:46.0f/255.0f alpha:1];
                    }];
    
    // add a swipe Gesture
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissLoginView)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self addGestureRecognizer:swipeRight];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissLoginView)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissLoginView)];
    [swipeUp setDirection:UISwipeGestureRecognizerDirectionUp];
    [self addGestureRecognizer:swipeUp];
}

-(void)dismissLoginView{
    // Animate the submitView away
    [UIView animateWithDuration:0.6f delay:0.01f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         if ([device isEqualToString:@"IPHONE"]){
                             [self setCenter:CGPointMake(168.5, -270)];
                         }else{
                             [self setCenter:CGPointMake(384, -512)];
                         }
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
}

-(void) checkToEnableSubmitButton {
    if (_userName.text.length >= 2 && _userPassword.text.length >= 8 ) {
        _submitButton.userInteractionEnabled = YES;
    }
}

-(void) textFieldDidChange:(UITextField *)textField{
    // write into the relevant text message underneath the Text Field
    NSAttributedString *noWarning = [[NSAttributedString alloc] initWithString:@"" attributes:_warningAttributes];
    if (textField.tag == kUserName) {
        //check if the user has put in less than 2 characters
        if (textField.text.length < 2) {
            //write in that the
            NSAttributedString  *warning = [[NSAttributedString alloc] initWithString:@"* Ar a laghad dhá caractar" attributes:_warningAttributes];
            [_userNameWarning setAttributedText:warning];
        }
        else{
            //clear the warning sign
            [_userNameWarning setAttributedText:noWarning];
            [self checkToEnableSubmitButton];
        }
    }
    else if(textField.tag == kUserPassword){
        if (textField.text.length < 8) {
            //write in that the password must be 8 characters or more
            NSAttributedString *passWarning = [[NSAttributedString alloc] initWithString:@"* Ar a laghad ocht caracter" attributes:_warningAttributes];
            [_userPasswordWarning setAttributedText:passWarning];
        }
        else{
            // clear the warning sign
            [_userPasswordWarning setAttributedText:noWarning];
            [self checkToEnableSubmitButton];
        }
    }
}

-(void)submitUser{
    
    if ([self viewWithTag:100]) {
        UIImageView *pictureTakenView = (UIImageView *)[self viewWithTag:100];
        picture = pictureTakenView.image;
    }
    //TODO: Submit the details to the QuickBlox people
    [(GameViewController *)self.window.rootViewController createNewUser:_userName.text
                                                               Password:_userPassword.text];

    // Animate the submitView away
    [UIView animateWithDuration:0.6f delay:0.01f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         if ([device isEqualToString:@"IPHONE"]){
                             [self setCenter:CGPointMake(168.5, -270)];
                         }else{
                             [self setCenter:CGPointMake(384, -512)];
                         }
    }
                     completion:^(BOOL finished) {
                         [(GameViewController *)self.window.rootViewController loadLoadingView];
                         [self removeFromSuperview];
    }];
}


-(void)takePicture{
    [(GameViewController *)self.window.rootViewController takePicture:imageView];
}

-(void)selectPicture{
    [(GameViewController *)self.window.rootViewController selectPicture:imageView];
}

-(void)flipView {
    [UIView transitionFromView:imageView
                        toView:flippedView
                      duration:1.0
                       options:UIViewAnimationOptionTransitionFlipFromLeft | UIViewAnimationOptionShowHideTransitionViews
                    completion:^(BOOL finished) {
                            nil;
    }];
}

-(void) reverseFlipView{
    [UIView transitionFromView:flippedView
                        toView:imageView
                      duration:1.0
                       options:UIViewAnimationOptionTransitionFlipFromRight | UIViewAnimationOptionShowHideTransitionViews
                    completion:^(BOOL finished) {
                           nil;
                       }];
}

-(void)keyboardDidShow{
    if ([device isEqualToString:@"IPHONE"]) {
        [UIView animateWithDuration:.4f animations:^{
            self.center = CGPointMake(originalCenter.x, originalCenter.y -130);
        }];
    }
}

@end

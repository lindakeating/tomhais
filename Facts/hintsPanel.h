//
//  hintsPanel.h
//  Facts
//
//  Created by Linda Keating on 09/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface hintsPanel : UIView

@property (nonatomic, retain) UIButton *removeView;
@property (nonatomic, retain) UIButton *firstIcon;
@property (nonatomic, retain) UIButton *secondIcon;
@property (nonatomic, retain) UIButton *thirdIcon;


-(void)loadHintsPanel:(BOOL)hintUsed
      firstLetterUsed:(BOOL)usedFirstLetter
          answerShown:(BOOL)answerShown
       hintsRemaining:(int)hintsRemaining
 firstLetterRemaining:(int)firstLetterRemaing
     answersRemaining:(int)answersRemaining;

@end

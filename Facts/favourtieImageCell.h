//
//  favourtieImageCell.h
//  Facts
//
//  Created by Linda Keating on 01/06/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface favourtieImageCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIImageView *customImageView;

@end

//
//  Tile.m
//  Facts
//
//  Created by Linda Keating on 23/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "Tile.h"
#import "UIColor+ColorExtensions.h"

@implementation Tile

-(instancetype) initWithString:(NSString *)character xPosition:(float)x yPosition:(float)y letterSpaceIndex:(float)l{
    self = [super init];
    if(self){
        self.name = @"Tile";
        self.letterSpaceIndex = l;
        self.character = character;
        CGPathRef cPath = [self newRoundedRectWithX:0 Y:0 width:60 height:60 cornerRadius:4 cornerHeight:4];
        [self setPath:cPath];
        CFRelease(cPath);
       // [self setPath:CGPathCreateWithRoundedRect(CGRectMake(0, 0, 60, 60), 4, 4, nil)];
        self.strokeColor = self.fillColor = [UIColor colorWithRed:66.0f/255.0f
                                                            green:74.0f/255.0f
                                                             blue:84.0f/255.0f
                                                            alpha:1];
        if (y <8) {
            self.position = CGPointMake(x, 0);
            self.originalPoint = CGPointMake(x, 0);
        }
        else{
            self.position = CGPointMake(x -575, -71);
            self.originalPoint = CGPointMake(x - 575, -71);
        }
        
        
        SKLabelNode *characterNode = [[SKLabelNode alloc] initWithFontNamed:@"Avenir-Medium"];
        characterNode.text = character;
        characterNode.fontSize = 40;
        characterNode.fontColor = [UIColor whiteGreyColor];
        characterNode.position = CGPointMake(30, 15);
        [self addChild:characterNode];

        NSLog(@"A New Title has been initiated");
    }
    return self;
}

-(instancetype) addHiddenTileToSpace:(NSString *)letter withIndex:(int)i withPositionInWord:(int)p device:(NSString *)device{
    if(self == [super init] ){
        self.name = @"Tile";
       // self.hidden = YES;
        if (p == 0) {
            self.firstLetter = YES;
        }
        else{
            self.firstLetter = NO;
        }
        self.positionInAnswer = i;
        self.character = letter;
        if ([device isEqualToString:@"IPHONE"]) {
            CGPathRef cPath = [self newRoundedRectWithX:-15 Y:-15 width:30 height:30 cornerRadius:2 cornerHeight:2];
            [self setPath:cPath];
            CFRelease(cPath);
           // [self setPath:CGPathCreateWithRoundedRect(CGRectMake(-15, -15, 30, 30), 2, 2, nil)];
        }else{
            CGPathRef cPath = [self newRoundedRectWithX:-30 Y:-30 width:60 height:60 cornerRadius:4 cornerHeight:4];
            [self setPath:cPath];
            CFRelease(cPath);
          // [self setPath:CGPathCreateWithRoundedRect(CGRectMake(-30, -30, 60, 60), 4, 4, nil)];
        }
        
        self.strokeColor = self.fillColor = [UIColor colorWithRed:66.0f/255.0f
                                                            green:74.0f/255.0f
                                                             blue:84.0f/255.0f
                                                            alpha:1];
        _characterNode = [[SKLabelNode alloc] initWithFontNamed:@"Avenir-Medium"];
        _characterNode.text = [letter uppercaseString];
        
        _characterNode.fontColor = [UIColor whiteGreyColor];
        if ([device isEqualToString:@"IPHONE"]) {
            _characterNode.fontSize = 20;
            _characterNode.position = CGPointMake(0, -7.5);
        }else{
            _characterNode.position = CGPointMake(0, -15);
            _characterNode.fontSize = 40;
        }
        _characterNode.name = @"tileCharacter";
        [self addChild:_characterNode];
    }
    return self;
}

-(void) addAnimations{
   
}

-(CGPathRef) newRoundedRectWithX:(float)x Y:(float)y width:(float)width height:(float)height cornerRadius:(float)cornerRadius cornerHeight:(float)cornerHeight{
    CGRect cRect = CGRectMake(x, y, width, height);
    CGPathRef cPath = CGPathCreateWithRoundedRect(cRect, cornerRadius, cornerHeight, nil);
    return cPath;
}

@end

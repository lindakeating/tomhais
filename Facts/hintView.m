//
//  hintView.m
//  Facts
//
//  Created by Linda Keating on 26/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "hintView.h"
#import "UIColor+ColorExtensions.h"

@implementation hintView{
    NSString *device;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)loadHintWithStyles:(NSString *)hintText {
    
    // figure out if it is an IPHONE or an IPAD
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"IPAD";
    }else{
        device = @"IPHONE";
    }
    self.layer.borderColor = [UIColor greenSpacesColor].CGColor;
    self.layer.cornerRadius = 2.0f;
    
    UITextView *hint;

    if ([device isEqualToString:@"IPHONE"]) {
        self.layer.borderWidth = 2.0f;
        hint = [[UITextView alloc] initWithFrame:CGRectMake(0, -6, self.frame.size.width, self.frame.size.height)];
        [hint setFont:[UIFont fontWithName:@"Avenir Next" size:18]];
    }else{
      self.layer.borderWidth = 3.5f;
      hint = [[UITextView alloc] initWithFrame:CGRectMake(0, -2, self.frame.size.width, self.frame.size.height)];
      [hint setFont:[UIFont fontWithName:@"Avenir Next" size:32]];
    }
    [hint setText:hintText];
    [hint setTextColor:[UIColor greenSpacesColor]];
    [hint setBackgroundColor:[UIColor clearColor]];
    hint.textAlignment = NSTextAlignmentCenter;
    [self addSubview:hint];
    
    [UIView animateWithDuration:0.6f delay:0.0f usingSpringWithDamping:4.0f initialSpringVelocity:6.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        if ([device isEqualToString:@"IPHONE"]) {
            CGRect screen = [[UIScreen mainScreen]bounds];
            [self setFrame:CGRectMake(20, screen.size.height / 1.64, 282, 28)];
           // [self setFrame:CGRectMake(20, 292, 282, 28)];
        }else{
          [self setFrame:CGRectMake(205/2, 1033/2, 1109/2, 95/2)];
        }

    } completion:^(BOOL finished) {
        nil;
    }];
    
}

@end

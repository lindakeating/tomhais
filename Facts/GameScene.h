//
//  GameScene.h
//  Facts
//

//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "LnbScene.h"

@interface GameScene : SKScene

@property (nonatomic)LnbScene *lnbScene;

@end

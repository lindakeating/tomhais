//
//  iphoneFavouritedCell.h
//  Facts
//
//  Created by Linda Keating on 03/06/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "favouriteLabel.h"

@interface iphoneFavouritedCell : UITableViewCell
@property (nonatomic, weak) IBOutlet favouriteLabel *question;
@property (nonatomic, weak) IBOutlet favouriteLabel *answer;

@end

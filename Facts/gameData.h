//
//  gameData.h
//  Facts
//
//  Created by Linda Keating on 30/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface gameData : NSObject <NSCoding>
@property (assign, nonatomic) long score;
@property (assign, nonatomic) long level;
@property (assign, nonatomic) long riddlesCompleted;
@property (assign, nonatomic) long hints;
@property (assign, nonatomic) long firstLetters;
@property (assign, nonatomic) long answers;
@property (strong, nonatomic) NSMutableArray *favourites;

+(instancetype)sharedGameData;
-(void)reset;
-(void)save;

@end

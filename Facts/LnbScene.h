//
//  LnbScene.h
//  Facts
//
//  Created by Linda Keating on 22/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h>
#import "tomhaisObject.h"
#import "Tile.h"
#import "tomahaisQuestion.h"
#import "button.h"
#import "lives.h"
#import "letterSpace.h"
#import "gameData.h"

typedef void(^myCompletion)(BOOL);

@interface LnbScene : SKScene 

-(void)nextRiddle;
-(void)nextRiddleWithDelay;
-(void)setNeutralTileColor;

@property (nonatomic, retain) UILabel *teidealTitle;
@property (nonatomic, retain) UILabel *questionLabel;
@property (nonatomic, retain) UILabel *scoreIcon;
@property (nonatomic, strong) SKShapeNode *selectedNode;
@property (nonatomic, strong) NSMutableArray *spacesArray;
@property (nonatomic, strong) SKNode *spacesBoundary;
@property (nonatomic, strong) SKNode *tilesBoundary;
@property (nonatomic, strong) tomhaisObject *currentTomhais;
@property (nonatomic, strong) NSMutableArray *tomhaiseanna;
@property (nonatomic, strong) lives *hintLives;
@property (nonatomic, strong) lives *firstLetterLives;
@property (nonatomic, strong) lives *answerLives;
@property (nonatomic, strong) NSMutableArray* tilesCollection;
@property (nonatomic, strong) SKLabelNode* score;
@property (nonatomic) float currentRiddle;
@property (nonatomic, strong) NSString *device;

@end

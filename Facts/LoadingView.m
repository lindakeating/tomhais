//
//  LoadingView.m
//  Facts
//
//  Created by Linda Keating on 25/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "LoadingView.h"
#import <SpinKit/RTSpinKitView.h>

@implementation LoadingView{
    NSString *device;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setIndicator{    
    // figure out if it is an IPHONE or an IPAD
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"IPAD";
    }else{
        device = @"IPHONE";
    }
    
    UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurView.frame = self.bounds;
    blurView.layer.opacity = .6f;
    blurView.layer.backgroundColor = [UIColor colorWithRed:38.0f/255.0f green:42.0f/255.0f blue:46.0f/255.0f alpha:.8f].CGColor;
    [self addSubview:blurView];
    
   // [self setBackgroundColor:[UIColor colorWithRed:79.0f/255.0f green:193.0f/255.0f blue:233.0f/255.0f alpha:1]];
    RTSpinKitView *spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleBounce color:[UIColor whiteColor] spinnerSize:100];
    if ([device isEqualToString:@"IPHONE"]) {
        spinner.center = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)-145);
    }else{
        spinner.center = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)-300);
    }

    [spinner startAnimating];
    [self addSubview:spinner];
    
    UITextView *textView;
    if ([device isEqualToString:@"IPHONE"]) {
        textView = [[UITextView alloc] initWithFrame:CGRectMake(20, 190, 282, 30)];
    }else{
        textView = [[UITextView alloc] initWithFrame:CGRectMake(214, 422, 352, 44)];
    }
    
    [textView setText:@"Loading Leaderboard"];
    [textView setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:18]];
    [textView setTextAlignment:NSTextAlignmentCenter];
    [textView setTextColor:[UIColor whiteColor]];
    [textView setBackgroundColor:[UIColor clearColor]];
    textView.editable = NO;
    [self addSubview:textView];
    
}
@end

//
//  favouriteLabel.m
//  Facts
//
//  Created by Linda Keating on 31/05/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "favouriteLabel.h"

@implementation favouriteLabel

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    
    // If this is a multiline label, need to make sure
    // preferredMaxLayoutWidth always matches the frame width
    // (i.e. orientation change can mess this up)
    
    if (self.numberOfLines == 0 && bounds.size.width != self.preferredMaxLayoutWidth) {
        self.preferredMaxLayoutWidth = self.bounds.size.width;
        [self setNeedsUpdateConstraints];
        NSLog(@"Label Bounds after update constraints %@", NSStringFromCGRect(self.bounds));
        self.font = [UIFont fontWithName:@"Avenir-Medium" size:17];
    }
}

@end

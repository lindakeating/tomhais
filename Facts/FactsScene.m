//
//  FactsScene.m
//  Facts
//
//  Created by Linda Keating on 09/12/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "FactsScene.h"
#import "GameScene.h"

@implementation FactsScene {
    NSUserDefaults* defaults;
    NSString* musicPath;
    
    NSInteger playerLives;
    NSInteger playerLevel;
    int maximumTime;
    SKLabelNode *timerLevel;
    
    NSMutableArray *statements;
    int randomQuestion;
    int questionNumber;
    int totalRightQuestions;
}

-(id) initWithSize:(CGSize)size inLevel:(NSInteger)level withPlayerLives:(int)lives {
    if (self = [super initWithSize:size]) {
        self.backgroundColor = [SKColor colorWithRed:0.35 green:0.25 blue:0.5 alpha:1.0];
        defaults = [NSUserDefaults standardUserDefaults];
        
        playerLives = lives;
        playerLevel = level;
        
        maximumTime = 30;
        
        questionNumber = 1;
        totalRightQuestions = 0;
        
        statements = [[NSMutableArray alloc] init];
        NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"LevelDescription" ofType:@"plist"];
        NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
        
        if ([dictionary objectForKey:@"Questions"] != nil) {
            NSMutableArray *array = [dictionary objectForKey:@"Questions"];
            
            for (int i = 0; i < [array count]; i++) {
                NSMutableDictionary *questions = [array objectAtIndex:i];
                factObject *stat = [factObject new];
                stat.factID = [[questions objectForKey:@"id"] intValue];
                stat.statement = [questions objectForKey:@"statement"];
                stat.isCorrect = [[questions objectForKey:@"isCorrect"]integerValue];
                stat.additionalInfo = [questions objectForKey:@"additionalInfo"];
                [statements addObject:stat];
            }
        }
        
        CGRect labelFrame = CGRectMake(120, 300, 500, 100);
        _questionLabel = [[UILabel alloc] initWithFrame:labelFrame];
        
        randomQuestion = [self getRandomNumberBetween:0 to:([statements count] -(int)1)];
        
        NSString *labelText = [[statements objectAtIndex:randomQuestion]statement];
        [_questionLabel setText:labelText];
        [_questionLabel setTextColor:[UIColor whiteColor]];
        [_questionLabel setFont:[UIFont fontWithName:NULL size:23]];
        [_questionLabel setTextAlignment:NSTextAlignmentCenter];
        [_questionLabel setNumberOfLines:0];
       
        
    }
    
    
    return self;
}

-(id) initWithSize:(CGSize)size{
    if (self = [super initWithSize:size]) {
        NSLog(@"FactsScene");
    }
    return self;
}

-(void) didMoveToView:(SKView *)view {
    // play Music
     musicPath = [[NSBundle mainBundle] pathForResource:@"music" ofType:@"mp3"];
    _musicPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:musicPath] error:NULL];
    _musicPlayer.numberOfLoops = -1;
    _musicPlayer.volume = 0.7;
    
    long musicFlag = [defaults integerForKey:@"music"];
    if (musicFlag == 1) {
        [_musicPlayer play];
    }
    else{
        [_musicPlayer stop];
    }
    
    // Add background
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"background.png"];
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    background.size = CGSizeMake(self.frame.size.width, self.frame.size.height);
    [self addChild:background];
    
    // Add Front Image
    SKSpriteNode *frontImage = [SKSpriteNode spriteNodeWithImageNamed:@"transparentCenterBorder.png"];
    frontImage.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    frontImage.size = CGSizeMake(600, 450);
    [self addChild:frontImage];
    
    heartArray = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i< playerLives; i++) {
        SKSpriteNode* liveImage = [SKSpriteNode spriteNodeWithImageNamed:@"hearth.png"];
       // liveImage.scale = .6;
        liveImage.position = CGPointMake(CGRectGetMaxX(self.frame) - 40 - (i* 50), CGRectGetMaxY(self.frame) -40);
        [heartArray insertObject:liveImage atIndex:i];
        [self addChild:liveImage];
    }
    
    _trueButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _trueButton.frame = CGRectMake(CGRectGetMidX(self.frame)-460, CGRectGetMidY(self.frame)+500, 335, 106);
    _trueButton.backgroundColor = [UIColor clearColor];
    [_trueButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal ];
    UIImage *buttonTrueImageNormal = [UIImage imageNamed:@"trueBtn.png"];
    UIImage *strechableButtonTrueImageNormal = [buttonTrueImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [_trueButton setBackgroundImage:strechableButtonTrueImageNormal forState:UIControlStateNormal];
    [_trueButton addTarget:self action:@selector(presentCorrectWrongMenu:) forControlEvents:UIControlEventTouchUpInside];
    [_trueButton setTag:1];
    [self.view addSubview:_trueButton];
    
    _falseButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _falseButton.frame = CGRectMake(CGRectGetMidX(self.frame)-110, CGRectGetMidY(self.frame) + 500, 335, 106);
    _falseButton.backgroundColor = [UIColor clearColor];
    [_falseButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *buttonFalseImageNormal = [UIImage imageNamed:@"falseBtn.png"];
    UIImage *stretchableButtonFalseImageNormal = [buttonFalseImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [_falseButton setBackgroundImage:stretchableButtonFalseImageNormal forState:UIControlStateNormal];
    [_falseButton addTarget:self action:@selector(presentCorrectWrongMenu:) forControlEvents:UIControlEventTouchUpInside];
    [_falseButton setTag:0];
    [self.view addSubview:_falseButton];
    
    

    timerLevel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    timerLevel.text = @"30";
    timerLevel.fontSize = 70;
    timerLevel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)+250);
    [self addChild:timerLevel];
    
    [self.view addSubview:_questionLabel];
    
    
    
   SKAction *wait = [SKAction waitForDuration:1];
    SKAction *updateTimer = [SKAction runBlock:^{
        [self updateTimer];
    }];
    
    SKAction *updateTimerS = [SKAction sequence:@[wait, updateTimer]];
    [self runAction:[SKAction repeatActionForever:updateTimerS]];
    

}

-(void)presentCorrectWrongMenu:(UIButton*)sender{
    
    int userData = sender.tag;
    
    
    //background
    _backgroundStatement = [SKSpriteNode spriteNodeWithImageNamed:@"background.png"];
    _backgroundStatement.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    _backgroundStatement.size = CGSizeMake(768, 1024);
    _backgroundStatement.zPosition = 10;
    _backgroundStatement.alpha = 0.0;
    [self addChild:_backgroundStatement];
    
    _nextQuestion = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _nextQuestion.frame = CGRectMake(CGRectGetMidX(self.frame)-10, CGRectGetMidY(self.frame)+90, 200, 70.0);
    _nextQuestion.backgroundColor = [UIColor clearColor];
    [_nextQuestion setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_nextQuestion setTitle:@"Tap Here to Continue" forState:UIControlStateNormal];
    [_nextQuestion addTarget:self action:@selector(nextquestion) forControlEvents:UIControlEventTouchUpInside];
    _nextQuestion.alpha = 1.0;
    [self.view addSubview:_nextQuestion];
    
    
    [_backgroundStatement runAction:[SKAction fadeAlphaTo:1.0f duration:0.2f]];
    _trueButton.alpha = 0.0;
    _falseButton.alpha = 0.0;
    
    if(([[statements objectAtIndex:randomQuestion] isCorrect] == 0 && userData == 0 ) || ([[statements objectAtIndex:randomQuestion] isCorrect] == 1 && userData == 1)){
        
        if ([[statements objectAtIndex:randomQuestion] isCorrect] == 0) {
            _questionLabel.text = [[statements objectAtIndex:randomQuestion] additionalInfo];
            
            _correct = [SKSpriteNode spriteNodeWithImageNamed:@"correct.png"];
            _correct.scale = .6;
            _correct.zPosition = 10;
            _correct.position = CGPointMake(CGRectGetMidX(self.frame), 800);
            _correct.alpha = 1.0;
            
            totalRightQuestions++;
            
            [self touchWillProduceASound:@"True"];
            [self addChild:_correct];
        }
        else{
            if ([[statements objectAtIndex:randomQuestion] isCorrect] == 0) {
                _questionLabel.text = [[statements objectAtIndex:randomQuestion] additionalInfo];
                
                _wrong = [SKSpriteNode spriteNodeWithImageNamed:@"wrong.png"];
                _wrong.scale = .6;
                _wrong.zPosition = 10;
                _wrong.position = CGPointMake(CGRectGetMidX(self.frame), 800);
                _wrong.alpha = 1.0;
                
                [self removePlayerLife];
                
                [self touchWillProduceASound:@"False"];
                [self addChild:_wrong];
            }
        }
    }
}

-(void)nextquestion{
    [self resetTimer];
    
    questionNumber++;
    _currentLevelLabel.text = [[NSString alloc] initWithFormat:@"Level %ld of 10", (long)questionNumber];
    
    _wrong.alpha = 0.0;
    _correct.alpha = 0.0;
    _backgroundStatement.alpha = 0.0;
    _nextQuestion.alpha = 0.0;
    
    [statements removeObject:[statements objectAtIndex:randomQuestion]];
    
    randomQuestion = [self getRandomNumberBetween:0 to:([statements count] -1)];
    [_questionLabel setText:[[statements objectAtIndex:randomQuestion]statement]];
    
    _trueButton.alpha = 1.0;
    _falseButton.alpha = 1.0;
    
    if (questionNumber == 10 && totalRightQuestions > 7) {
        int nextLevel = playerLevel+2;
        [defaults setInteger:nextLevel forKey:@"actualPlayerLevel"];
        [self removeUIViews];
        SKTransition* transition = [SKTransition doorwayWithDuration:2];
        LevelSelect* levelSelect = [[LevelSelect alloc] initWithSize:CGSizeMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame))];
        [self.scene.view presentScene:levelSelect transition:transition];
        
        
    }
    
}

-(void) resetTimer{
    maximumTime = 60;
    [_timerLevel setText:@"60"];
}


-(int)getRandomNumberBetween:(int)from to:(int)to{
    return (int)from + arc4random() % (to-from+1);
}

-(void) updateTimer{
    maximumTime --;
    if(maximumTime == 0){
        if (playerLives < 1) {
            [self touchWillProduceASound:@"False"];
            [self moveToHome];
        }else {
            [self presentCorrectWrongMenu:_trueButton];
            [self touchWillProduceASound:@"False"];
            [self removePlayerLife];
        }
    }
    [_timerLevel setText:[[NSNumber numberWithInt:maximumTime]stringValue]];
}

-(void)removePlayerLife{
    if(playerLives > 1){
        for (NSInteger i = 0; i< playerLives; i++) {
            SKSpriteNode* node = [heartArray objectAtIndex:i];
            if(i == (playerLives -1)){
                node.alpha = .1;
            }
        }
        playerLives--;
    }else{
        [self moveToHome];
    }
}
-(void)moveToHome{
    SKTransition* transition = [SKTransition fadeWithDuration:2];
    GameScene* myscene = [[GameScene alloc] initWithSize:CGSizeMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame))];
    [self.scene.view presentScene:myscene transition:transition];
}

-(void) removeUIViews{
    [_trueButton removeFromSuperview];
    [_falseButton removeFromSuperview];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *touch in touches ) {
        [self touchWillProduceASound:@"False"];
    }
}

-(void) touchWillProduceASound:(NSString*)answer{
    long soundFlag = [defaults integerForKey:@"sound"];

    if (soundFlag == 1) {
        SKAction* sound;
        if ([answer isEqualToString:@"False"]) {
            sound = [SKAction playSoundFileNamed:@"wrong.mp3" waitForCompletion:YES];
        }else{
            sound = [SKAction playSoundFileNamed:@"right.mp3" waitForCompletion:YES];
        }
        [self runAction:sound];
    }
    

}


@end

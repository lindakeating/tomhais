//
//  button.m
//  Facts
//
//  Created by Linda Keating on 24/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "button.h"
#import "UIColor+ColorExtensions.h"

@implementation button
-(instancetype) initWithString:(NSString *)character fontNamed:(NSString *)font size:(float)size{
    self = [super init];
    if (self) {
        [self setSize:CGSizeMake(size, size)];
        //icon Text
        _label = [[SKLabelNode alloc] initWithFontNamed:font];
        _label.name = character;
        _label.fontSize = size;
        _label.fontColor = [UIColor textGreyColor];
        
        [_label setText:character];
        _label.position = CGPointMake(0, 0);
        [self addChild:_label];
        
    }
    return self;
}

-(instancetype) initWithString:(NSString *)character fontNamed:(NSString *) font size:(float)size withShadow:(BOOL)yes{
    self = [super init];
    if (self) {
        [self setSize:CGSizeMake(size, size)];
        //icon Text
        SKLabelNode *label = [[SKLabelNode alloc] initWithFontNamed:font];
        label.fontSize = size;
        label.name = character;
        label.fontColor = [UIColor textGreyColor];
        label.text = character;
        label.position = CGPointMake(0, 0);
        [self addChild:label];
        
        //icon Shadow
        SKLabelNode *labelShadow = [[SKLabelNode alloc] initWithFontNamed:font];
        labelShadow.fontSize = size;
        labelShadow.fontColor = [UIColor colorWithRed:0.0f/255.0f
                                                green:0.0f/255.0f
                                                 blue:0.0f/255.0f
                                                alpha:.2];
        labelShadow.text = character;
        labelShadow.position = CGPointMake(0, -2);
        labelShadow.zPosition = -1;
        [self addChild:labelShadow];
        
    }
    return self;
}

@end

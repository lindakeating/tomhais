//
//  button.h
//  Facts
//
//  Created by Linda Keating on 24/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface button : SKSpriteNode
@property (nonatomic, retain) SKLabelNode *label;

-(instancetype) initWithString:(NSString *)character fontNamed:(NSString *) font size:(float)size;
-(instancetype) initWithString:(NSString *)character fontNamed:(NSString *) font size:(float)size withShadow:(BOOL)yes;

@end

//
//  logInview.h
//  Facts
//
//  Created by Linda Keating on 06/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface logInview : UIView <UITextFieldDelegate >
@property (nonatomic) UITextField *userName;
@property (nonatomic) UITextField *userNameWarning;
@property (nonatomic) UITextField *userPassword;
@property (nonatomic) UITextField *userPasswordWarning;
@property (nonatomic) UIButton *submitButton;
@property (nonatomic) NSMutableDictionary *warningAttributes;

-(void) loadLogInView;
-(void) reverseFlipView;

@end

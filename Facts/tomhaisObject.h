//
//  tomhaisObject.h
//  Facts
//
//  Created by Linda Keating on 21/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface tomhaisObject : NSObject

@property(nonatomic,readwrite) int tomhaisId;
@property(nonatomic,readwrite,retain) NSString *gameType;
@property(nonatomic,readwrite,retain) NSString *question;
@property(nonatomic,readwrite,retain) NSString *answer;
@property(nonatomic,readwrite,retain) NSString *hint;
@property(nonatomic,readwrite) NSInteger favourite;
@property(nonatomic,readwrite) NSInteger isCorrect;

@end

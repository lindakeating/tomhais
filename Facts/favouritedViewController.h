//
//  favouritedViewController.h
//  Facts
//
//  Created by Linda Keating on 28/05/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface favouritedViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

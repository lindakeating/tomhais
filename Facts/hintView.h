//
//  hintView.h
//  Facts
//
//  Created by Linda Keating on 26/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameViewController.h"

@interface hintView : UIView

-(void) loadHintWithStyles:(NSString *)hintText;

@end

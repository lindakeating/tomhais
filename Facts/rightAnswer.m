//
//  rightAnswer.m
//  Facts
//
//  Created by Linda Keating on 14/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "rightAnswer.h"
#import "GameViewController.h"
#import "UIColor+ColorExtensions.h"
#import "FXBlurView.h"

#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)

@implementation rightAnswer{
    NSString *device;
    StarsView *starsView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void) initWithAnswer:(NSString *)answer {
    
    // figure out if it is an IPHONE or an IPAD
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"IPAD";
    }else{
        device = @"IPHONE";
    }
    
    CGRect screen = [[UIScreen mainScreen]bounds];
    
    if(NSFoundationVersionNumber >NSFoundationVersionNumber_iOS_7_1){
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
        CGRect blurBounds;
        if ([device isEqualToString:@"IPHONE"]) {
            blurBounds = CGRectMake(19, 67, 284, screen.size.height / 1.18);
            visualEffectView.layer.cornerRadius = 4.0f;
        }else{
            blurBounds = CGRectMake(60, 120, 637, 867);
            visualEffectView.layer.cornerRadius = 10.0f;
        }
        
        visualEffectView.frame = blurBounds;
        
        visualEffectView.layer.masksToBounds = YES;
        [self addSubview:visualEffectView];
    }else{
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        UIToolbar *fakeToolbar;
        
        if ([device isEqualToString:@"IPHONE"]) {
            fakeToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(19, 67, 284, screen.size.height / 1.18)];
            fakeToolbar.autoresizingMask = self.autoresizingMask;
            fakeToolbar.barStyle = UIBarStyleBlack;
            fakeToolbar.translucent = YES;
            fakeToolbar.layer.cornerRadius = 4.0f;
            fakeToolbar.layer.masksToBounds = YES;
            [self addSubview:fakeToolbar];
        }else{
            fakeToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(60, 120, 637, 867)];
            fakeToolbar.autoresizingMask = self.autoresizingMask;
            fakeToolbar.translucent = YES;
            fakeToolbar.barStyle = UIBarStyleBlack;
            fakeToolbar.layer.cornerRadius = 10.0f;
            fakeToolbar.layer.masksToBounds = YES;
            [self addSubview:fakeToolbar];
        }
    }
    
   // self.backgroundColor = [UIColor colorWithRed:38.0f/255.0f green:42.0f/255.0f blue:46.0f/255.0f alpha:1];

    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    //write in Score
    UILabel *trophyLabel;
    NSMutableAttributedString *trophy;
    if ([device isEqualToString:@"IPHONE"]) {
        trophyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, self.frame.size.width, 110)];
        trophy = [[NSMutableAttributedString alloc] initWithString:@"b"
                                                        attributes:@{
                                                                     NSFontAttributeName:[UIFont fontWithName:@"tomhais-icons" size:60],
                                                                     NSForegroundColorAttributeName:[UIColor textGreyColor],
                                                                     NSParagraphStyleAttributeName:paragraphStyle
                                                                     }];
    }else{
        trophyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 261, self.frame.size.width, 110)];
        trophy = [[NSMutableAttributedString alloc] initWithString:@"b"
                                    attributes:@{
                                           NSFontAttributeName:[UIFont fontWithName:@"tomhais-icons" size:100],
                                           NSForegroundColorAttributeName:[UIColor textGreyColor],
                                           NSParagraphStyleAttributeName:paragraphStyle
                                            }];
    }
    
    trophyLabel.backgroundColor = [UIColor clearColor];
    trophyLabel.attributedText = trophy;
    [self addSubview:trophyLabel];
    
    UILabel *scoreLabel;
    NSAttributedString *score;
    NSString *newScore = [NSString stringWithFormat:@" %li", [gameData sharedGameData].score + 20];
    
    if([device isEqualToString:@"IPHONE"]){
        scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 10)];
        score = [[NSAttributedString alloc] initWithString:newScore
                                                attributes:@{
                                                             NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Medium" size:12],
                                                             NSForegroundColorAttributeName:[UIColor whiteColor],
                                                             NSParagraphStyleAttributeName:paragraphStyle
                                                             }];
        [scoreLabel setCenter:CGPointMake(CGRectGetMidX(trophyLabel.frame), CGRectGetMidY(trophyLabel.frame)-10)];
    }else{
        scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 20)];
        score = [[NSAttributedString alloc] initWithString:newScore
                                            attributes:@{
                                                 NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Medium" size:24],
                                                 NSForegroundColorAttributeName:[UIColor whiteColor],
                                                 NSParagraphStyleAttributeName:paragraphStyle
                                                         }];
        [scoreLabel setCenter:CGPointMake(CGRectGetMidX(trophyLabel.frame), CGRectGetMidY(trophyLabel.frame)-15)];
    }
    
    scoreLabel.backgroundColor = [UIColor clearColor];
    scoreLabel.attributedText = score;
    
    [self addSubview:scoreLabel];
    
    // write in Stars
   /* UITextView *starsView;
    NSMutableAttributedString *startText;
    NSShadow *textShadow = [[NSShadow alloc] init];
    
    if ([device isEqualToString:@"IPHONE"]) {
        starsView = [[UITextView alloc] initWithFrame:CGRectMake(0, 200, self.frame.size.width, 32)];
        startText = [[NSMutableAttributedString alloc] initWithString:@"ddddd"
                                                           attributes:@{
                                                                        NSFontAttributeName:[UIFont fontWithName:@"tomhais-icons" size:23],
                                                                        NSForegroundColorAttributeName:[UIColor greenSpacesColor],
                                                                        NSShadowAttributeName:textShadow,
                                                                        NSKernAttributeName: @5.0f,
                                                                        NSParagraphStyleAttributeName:paragraphStyle
                                                                        }];
    }else{
        starsView = [[UITextView alloc] initWithFrame:CGRectMake(222, 418, 335, 72)];
        startText = [[NSMutableAttributedString alloc] initWithString:@"ddddd"
                                                           attributes:@{
                                                                        NSFontAttributeName:[UIFont fontWithName:@"tomhais-icons" size:55],
                                                                        NSForegroundColorAttributeName:[UIColor greenSpacesColor],
                                                                        NSShadowAttributeName:textShadow,
                                                                        NSKernAttributeName: @10.0f,
                                                                        NSParagraphStyleAttributeName:paragraphStyle
                                                                        }];
    }
    
    starsView.backgroundColor = [UIColor clearColor];
    [textShadow setShadowColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:.5]];
    [textShadow setShadowBlurRadius:2];
    [textShadow setShadowOffset:CGSizeMake(0, 1)];
    
    [starsView setAttributedText:startText];
    [self addSubview:starsView];*/
    
    starsView = [[StarsView alloc] initWithFrame:CGRectMake(60, CGRectGetMidY(self.frame) - 140, self.frame.size.width -80, self.frame.size.height /8)];
    [starsView addLoopStarsAnimation];
    [self addSubview:starsView];
    
    
    // write in the Tick
    UILabel *tickView;
    NSMutableAttributedString *tickText;
    if ([device isEqualToString:@"IPHONE"]) {
       tickView = [[UILabel alloc] initWithFrame:CGRectMake(0, 260, self.frame.size.width, 50)];
        tickText = [[NSMutableAttributedString alloc] initWithString:@"t" attributes:@{
                                                                                       NSFontAttributeName:[UIFont fontWithName:@"hint-icon" size:50],
                                                                                       NSParagraphStyleAttributeName:paragraphStyle,
                                                                                       NSForegroundColorAttributeName:[UIColor greenSpacesColor]}];
    }else{
       tickView = [[UILabel alloc] initWithFrame:CGRectMake(335.2, 528, 98.5, 99)];
       tickText = [[NSMutableAttributedString alloc] initWithString:@"t" attributes:@{
                                                                         NSFontAttributeName:[UIFont fontWithName:@"hint-icon" size:100],
                                                                         NSForegroundColorAttributeName:[UIColor greenSpacesColor]
        }];
    }

    [tickView setAttributedText:tickText];
    [self addSubview:tickView];
    
    // write in the Answer
    UILabel *answerView;
    NSMutableAttributedString *answerText;
    if ([device isEqualToString:@"IPHONE"]) {
        answerView = [[UILabel alloc] initWithFrame:CGRectMake(0, 320, self.frame.size.width, 49)];
        
        answerText = [[NSMutableAttributedString alloc] initWithString:answer
                                                            attributes:@{
                                                                         NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Medium" size:24],
                                                                         NSForegroundColorAttributeName:[UIColor greenSpacesColor],
                                                                         NSParagraphStyleAttributeName:paragraphStyle
                                                                         }];
    }else{
        answerView = [[UILabel alloc] initWithFrame:CGRectMake(143.5, 675.5, 481.5, 49)];
        
        answerText = [[NSMutableAttributedString alloc] initWithString:answer
                                                            attributes:@{
                                                                   NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Medium" size:36],
                                                                   NSForegroundColorAttributeName:[UIColor greenSpacesColor],
                                                                   NSParagraphStyleAttributeName:paragraphStyle
        }];
    }

    [answerView setAttributedText:answerText];
    [self addSubview:answerView];
    
    //Submit Button
    UIButton *submitButton;
    NSMutableAttributedString *seol;
    NSMutableAttributedString *seolPressed;
    
    if ([device isEqualToString:@"IPHONE"]) {
        submitButton = [[UIButton alloc] initWithFrame:CGRectMake(82, 395, 157, 32)];
        seol = [[NSMutableAttributedString alloc] initWithString:@"An chéad tomhais eile"
                                                                                 attributes:@{
                                                                                              NSForegroundColorAttributeName:[UIColor foregroundColorGrey],
                                                                                              NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Bold" size:13]
                                                                                              }];
        seolPressed = [[NSMutableAttributedString alloc] initWithString:@"An chéad tomhais eile"
                                                      attributes:@{
                                                                   NSForegroundColorAttributeName:[UIColor textGreyColor],
                                                                   NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Bold" size:13]
                                                                   }];
        
        
        
        
        
        submitButton.layer.cornerRadius = 2.0f;
    }else{
        submitButton = [[UIButton alloc] initWithFrame:CGRectMake(196.5, 771, 377, 48)];
        seol = [[NSMutableAttributedString alloc] initWithString:@"An chéad tomhais eile"
                                                                                 attributes:@{
                                                                                              NSForegroundColorAttributeName:[UIColor foregroundColorGrey],
                                                                                              NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Bold" size:20]
                                                                                              }];
        seolPressed = [[NSMutableAttributedString alloc] initWithString:@"An chéad tomhais eile"
                                                             attributes:@{
                                                                          NSForegroundColorAttributeName:[UIColor textGreyColor],
                                                                          NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Bold" size:20]
                                                                          }];
        submitButton.layer.cornerRadius = 4.0f;
    }
    
    [submitButton setBackgroundColor:[UIColor greenSpacesColor]];
    [submitButton setAttributedTitle:seol forState:UIControlStateNormal];
    [submitButton setAttributedTitle:seolPressed forState:UIControlStateSelected];
    [submitButton addTarget:self action:@selector(nextRiddle) forControlEvents:UIControlEventTouchUpInside];
    [submitButton setUserInteractionEnabled:YES];
    [self addSubview:submitButton];
    
    [UIView animateWithDuration:.3f
                          delay:.6f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
        [self setFrame:CGRectMake(0, 0, 980, 1024)];
    }
     completion:^(BOOL finished) {
                GameViewController *gvc = (GameViewController *)self.window.rootViewController;
                [gvc setNextRiddleValues];
     }];
}

-(void) nextRiddle{
    [UIView animateWithDuration:.6f animations:^{
        [self setFrame:CGRectMake(980, 0, 980, 1024)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
   /* [UIView animateWithDuration:.6f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self setFrame:CGRectMake(980, 0, 980, 1024)];
        //[self setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];*/
}

@end

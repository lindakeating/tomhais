//
//  favouriteCellView.m
//  Facts
//
//  Created by Linda Keating on 24/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "favouriteCellView.h"
#import "UIColor+ColorExtensions.h"

@implementation favouriteCellView{
    NSString *device;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // figure out if it is an IPHONE or an IPAD
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            device = @"IPAD";
        }else{
            device = @"IPHONE";
        }
        
        self.backgroundColor = [UIColor backgroundColorGrey];
        
        self.position = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 35, 35)];
        self.position.textColor = [UIColor whiteColor];
        self.position.font = [UIFont fontWithName:@"AvenirNext-Medium" size:18];
        self.position.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.position];
        
        self.profilePic = [[UIImageView alloc] initWithFrame:CGRectMake(36, 4, 35, 35)];
        [self addSubview:self.profilePic];
        
        if ([device isEqualToString:@"IPHONE"]) {
            self.userName = [[UILabel alloc] initWithFrame:CGRectMake(85, 5, 142,35)];
        }else{
            self.userName = [[UILabel alloc] initWithFrame:CGRectMake(85, 5, 280,35)];
        }
        self.userName.textColor = [UIColor whiteColor];
        self.userName.font = [UIFont fontWithName:@"AvenirNext-Medium" size:18.0f];
        [self addSubview:self.userName];
        
        if ([device isEqualToString:@"IPHONE"]) {
            self.points = [[UILabel alloc] initWithFrame:CGRectMake(208, 5, 45, 35)];
        }else{
            self.points = [[UILabel alloc] initWithFrame:CGRectMake(365, 5, 45, 35)];
        }
        self.points.textColor = [UIColor whiteColor];
        self.points.font = [UIFont fontWithName:@"AvenirNext-Medium" size:18.0f];
        self.points.textAlignment = NSTextAlignmentRight;
        [self addSubview:self.points];
        
    }
    return self;
}

@end

//
//  quickBloxConnection.h
//  Facts
//
//  Created by Linda Keating on 05/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Quickblox/Quickblox.h>

@interface quickBloxConnection : NSObject

@property (nonatomic) QBASession *currentSession;

-(void)connectToQuickBlox;

@end

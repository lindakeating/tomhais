//
//  favouritesViewController.m
//  Facts
//
//  Created by Linda Keating on 24/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "favouritesViewController.h"
#import "favouriteCellView.h"
#import "UIColor+ColorExtensions.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface favouritesViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) favouriteCellView *favouriteCell;
@property (strong, nonatomic) NSIndexPath *highlightedRowIndex;

@end

@implementation favouritesViewController{
    NSString *device;
    CGRect screen;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // figure out if it is an IPHONE or an IPAD
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"IPAD";
    }else{
        device = @"IPHONE";
    }
    
    screen = [[UIScreen mainScreen]bounds];
    
    NSString *reuseID = @"reuseID";
    self.view.backgroundColor = [UIColor backgroundColorGrey];
    if ([device isEqualToString:@"IPHONE"]) {
        self.view.frame = CGRectMake(0, 52, 320, screen.size.height);
    }else{
        self.view.frame = CGRectMake(0, 97, 768, 1021);
    }
    
    UILabel *trophyLabel;
    if ([device isEqualToString:@"IPHONE"]) {
        trophyLabel = [[UILabel alloc] initWithFrame:CGRectMake(140, 18, 40, 34)];
        [trophyLabel setFont:[UIFont fontWithName:@"tomhais-icons" size:35]];
    }else{
        trophyLabel = [[UILabel alloc] initWithFrame:CGRectMake(360, 30, 50, 50)];
        [trophyLabel setFont:[UIFont fontWithName:@"tomhais-icons" size:50]];
    }
    [trophyLabel setText:@"b"];
    [trophyLabel setTextColor:[UIColor textGreyColor]];
    [trophyLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:trophyLabel];
    
    UIView *topHorizontalLine;
    if ([device isEqualToString:@"IPHONE"]) {
        topHorizontalLine = [[UIView alloc] initWithFrame:CGRectMake(20, 62.5, 271, 1)];
    }else{
        topHorizontalLine = [[UIView alloc] initWithFrame:CGRectMake(167, 100, 464, 1)];
    }
    [topHorizontalLine setBackgroundColor:[UIColor horizontalGreyColor]];
    [self.view addSubview:topHorizontalLine];
    
    UIView *bottomHorizontalLine;
    if ([device isEqualToString:@"IPHONE"]) {
        bottomHorizontalLine = [[UIView alloc] initWithFrame:CGRectMake(20, screen.size.height / 1.25, 271, 1)];
    }else{
        bottomHorizontalLine = [[UIView alloc] initWithFrame:CGRectMake(167, 780, 464, 1)];
    }
    [bottomHorizontalLine setBackgroundColor:[UIColor horizontalGreyColor]];
    [self.view addSubview:bottomHorizontalLine];
     
    
    // Do any additional setup after loading the view.
    if ([device isEqualToString:@"IPHONE"]) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(25, 75, 261, self.view.frame.size.height - 200)];
    }else{
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(174, 130, 450, 626)];
    }
    [self.tableView registerClass:[favouriteCellView class] forCellReuseIdentifier:reuseID];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView setBackgroundColor:[UIColor backgroundColorGrey]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    //scrollbars
    [self.tableView setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
    [self.view addSubview:self.tableView];
   
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self.tableView flashScrollIndicators];
    NSInteger row = 0;
    for (QBCOCustomObject *object in _favouriteResults) {
        if (object.userID == self.user.ID) break;
        row++;
    }
    self.highlightedRowIndex = [NSIndexPath indexPathForRow:row inSection:0];
     [self.tableView scrollToRowAtIndexPath:self.highlightedRowIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
  
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.favouriteResults count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    favouriteCellView *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseID"];
    
    if(cell == nil){
        cell = [[favouriteCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuseID"];
    }
    
    QBCOCustomObject *favouriteObject = [_favouriteResults objectAtIndex:indexPath.row];

    cell.position.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row + 1];
    
    if ([favouriteObject.fields valueForKey:@"imageUrl"]) {
        [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:[favouriteObject.fields valueForKey:@"imageUrl"]] placeholderImage:[UIImage imageNamed:@"profilePic"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [cell.profilePic.layer setCornerRadius:cell.profilePic.frame.size.width/2];
            [cell.profilePic setClipsToBounds:YES];
        }];
        
       // [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:[favouriteObject.fields valueForKey:@"imageUrl"]] placeholderImage:[UIImage imageNamed:@"profilePic"]];
        
       /* UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[favouriteObject.fields valueForKey:@"imageUrl"]]]];
        [cell.profilePic setImage:image];*/
    }else{
        [cell.profilePic setImage:[UIImage imageNamed:@"profilePic"]];
    }
    cell.userName.text = [favouriteObject.fields valueForKey:@"userName"];
    cell.points.text = [NSString stringWithFormat:@"%@" ,[favouriteObject.fields valueForKey:@"points"]];
    
    if (favouriteObject.userID == self.user.ID) {
        UIView *bgColor = [[UIView alloc] init];
        bgColor.backgroundColor = [UIColor colorWithRed:173.0f/255.0f green:146.0f/255.0f blue:237.0f/255.0f alpha:.5];
        self.highlightedRowIndex = indexPath;
        [cell setSelectedBackgroundView:bgColor];
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    }
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

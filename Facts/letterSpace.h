//
//  letterSpace.h
//  Facts
//
//  Created by Linda Keating on 28/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface letterSpace : SKShapeNode

-(instancetype) initWithCharacter:(NSString *)character xPosition:(float)x yPosition:(float)y device:(NSString *)device;
  @property(nonatomic, assign) BOOL occupied;
  @property(nonatomic, assign) BOOL whiteSpace;
  @property(nonatomic, retain) NSString *character;
  @property (atomic) float secondLineX;
  @property (atomic) float secondLineY;


@end

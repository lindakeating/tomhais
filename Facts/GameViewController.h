//
//  GameViewController.h
//  Facts
//

//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

#import "gameData.h"
#import "logInview.h"
#import "favouritesViewController.h"
#import "wrongAnswerNotification.h"
#import "hintsPanel.h"
#import "Menu.h"
#import "rightAnswer.h"
#import "quickBloxConnection.h"
#import "LoadingView.h"
#import "hintView.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "SavedRiddlesViewController.h"

typedef void(^myCompletion)(BOOL);

@interface GameViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic) rightAnswer *rightAnswerView;
@property (nonatomic) logInview *loginView;
@property (nonatomic) wrongAnswerNotification *wrongAnswerNotification;
@property (nonatomic) Menu *menu;
@property (nonatomic) UIVisualEffectView *blurView;
@property (nonatomic) QBUUser *user;
@property (nonatomic) NSUserDefaults *defaults;
@property (nonatomic) favouritesViewController *favouriteVC;
@property (nonatomic) LoadingView *loadingView;
@property (nonatomic) hintsPanel *hintsPanel;
@property (nonatomic) BOOL usedFirstLetter;
@property (nonatomic) BOOL answerShown;
@property (nonatomic) BOOL hintUsed;
@property (copy, nonatomic) myCompletion myCompletion;
@property (nonatomic) SavedRiddlesViewController *savedRiddleVC;


-(void) showModal;
-(void) loadLoginView;
-(void) loadWrongAnswer;
-(void) showHintsPanel;
-(void) toggleMenu;
-(void) goToHomeScreen;
-(void) goToFavourites;
-(void) goToLeaderBoard;
-(void) goToLifelines;
-(void) nextRiddle;
-(void) showRightAnswer:(NSString*)answer;
-(void) loadLoadingView;
-(void) checkSessionAndSignInWithUserName:(NSString *)userName password:(NSString *)password;
-(void) checkSessionAndSignInWithUserName:(NSString *)userName password:(NSString *)password profilePic:(UIImageView *)profilePic;
-(void) showHint;
-(void) removeHintsPanel;
-(void) removeHintShown;
-(void) showFirstLetter;
-(void) showAnswer;
-(void) showAnswer:(myCompletion) compblock;
-(void)takePicture:(UIView*)imageView;
-(void) selectPicture:(UIView *)imageView;
-(void) removeWrongAnswer;
-(void) unloadMenu;
-(void) animateRightAnswer;
-(void) animateRightAnswer:(myCompletion)compblock;
-(void)removeTilesFromSpaces:(myCompletion)finished;
-(void) setNextRiddleValues;
-(void)shareScreen;
-(void) addToFavourites:(tomhaisObject*)tomhaisObject;


-(void)createNewUser:(NSString*)userName Password:(NSString*)password;

@end

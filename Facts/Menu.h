//
//  Menu.h
//  Facts
//
//  Created by Linda Keating on 11/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Menu : UIView

-(void) initialiseMenu;

@end

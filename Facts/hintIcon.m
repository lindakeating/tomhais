//
//  hintIcon.m
//  Facts
//
//  Created by Linda Keating on 25/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "hintIcon.h"
#import "UIColor+ColorExtensions.h"

@implementation hintIcon

-(instancetype) initWithString:(NSString *)character size:(float)size lives:(float)live{
    self = [super init];
    if (self) {
        // outer circle
        SKShapeNode *circle = [[SKShapeNode alloc] init];
        CGRect circleRect = CGRectMake(0, 0, size, size);
        circle.path = [UIBezierPath bezierPathWithOvalInRect:circleRect].CGPath;
        circle.strokeColor = [UIColor textGreyColor];
        circle.lineWidth = 3.5;
        [self addChild:circle];
        
        // center icon

    }    
    return self;
}

@end

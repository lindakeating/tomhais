//
//  hintsPanel.m
//  Facts
//
//  Created by Linda Keating on 09/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "hintsPanel.h"
#import "button.h"
#import "GameViewController.h"
#import "UIColor+ColorExtensions.h"

@implementation hintsPanel{
    NSString *device;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)loadHintsPanel:(BOOL)hintUsed
      firstLetterUsed:(BOOL)usedFirstLetter
          answerShown:(BOOL)answerShown
       hintsRemaining:(int)hintsRemaining
 firstLetterRemaining:(int)firstLetterRemaing
     answersRemaining:(int)answersRemaining
{
    // figure out if it is an IPHONE or an IPAD
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"IPAD";
    }else{
        device = @"IPHONE";
    }
    
    CGRect screen = [[UIScreen mainScreen]bounds];
    
    UIColor *inactiveColor = [UIColor inactiveGreyColor];
    self.layer.cornerRadius = 4.0f;
    
    //hintsPanel Shadow
    UIView *shadow = [[UIView alloc] initWithFrame:CGRectMake(0, +2, self.layer.bounds.size.width, self.layer.bounds.size.height)];
    shadow.layer.cornerRadius = 4.0f;
    shadow.layer.backgroundColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:.27].CGColor;
    [self addSubview:shadow];
    
    //grey Rectangle Panel
    UIView *panel = [[UIView alloc] initWithFrame:self.layer.bounds];
    panel.layer.cornerRadius = 4.0f;
    panel.layer.backgroundColor = [UIColor colorWithRed:66.0f/255.0f
                                                  green:74.0f/255.0f
                                                   blue:84.0f/255.0f alpha:1].CGColor;
    [self addSubview:panel];
   
    CGFloat thirds = self.layer.bounds.size.width/3;
    CGFloat x = thirds/2;
    
    // first Life Icon
    CGPoint firstIconPosition = CGPointMake(x, 50);
    _firstIcon = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 85/2, 91/2)];
    _firstIcon.layer.position = firstIconPosition;
    [_firstIcon setTitle:@"b" forState:UIControlStateNormal];
    _firstIcon.titleLabel.font = [UIFont fontWithName:@"hint-icon" size:42];
    //firstIcon.font = [UIFont fontWithName:@"hint-icon" size:42];
    [_firstIcon setTitleColor:[UIColor textGreyColor] forState:UIControlStateNormal];
    [_firstIcon addTarget:self action:@selector(giveHint) forControlEvents:UIControlEventTouchUpInside];
    if (hintUsed || (int)hintsRemaining < 1) {
        _firstIcon.userInteractionEnabled = NO;
        [_firstIcon setTitleColor:inactiveColor forState:UIControlStateNormal];
   
    }
    [self addSubview:_firstIcon];
    
    // second Life Icon
    CGPoint secondIconPostion = CGPointMake(thirds + x, 50);
    _secondIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 85/2, 91/2)];
    _secondIcon.layer.position = secondIconPostion;
    [_secondIcon setTitle:@"f" forState:UIControlStateNormal];
    _secondIcon.titleLabel.font = [UIFont fontWithName:@"hint-icon" size:42];
    [_secondIcon setTitleColor:[UIColor textGreyColor] forState:UIControlStateNormal];
    [_secondIcon addTarget:self action:@selector(showFirstLetter) forControlEvents:UIControlEventTouchUpInside];
    if (usedFirstLetter || (int)firstLetterRemaing <1) {
        _secondIcon.userInteractionEnabled = NO;
        [_secondIcon setTitleColor:inactiveColor forState:UIControlStateNormal];
    }
    [self addSubview:_secondIcon];
    
    // third Life Icon
    CGPoint thirdIconPostion = CGPointMake(thirds + thirds + x, 50);
    _thirdIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 85/2, 91/2)];
    _thirdIcon.layer.position = thirdIconPostion;
    [_thirdIcon setTitle:@"t" forState:UIControlStateNormal];
    _thirdIcon.titleLabel.font = [UIFont fontWithName:@"hint-icon" size:42];
    if (answerShown || answersRemaining <1) {
        _thirdIcon.userInteractionEnabled = NO;
        [_thirdIcon setTitleColor:inactiveColor forState:UIControlStateNormal];
         }else{
    [_thirdIcon setTitleColor: [UIColor textGreyColor] forState:UIControlStateNormal];
         }
    [_thirdIcon addTarget:self action:@selector(showAnswer) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_thirdIcon];
    
    //first Life points
    CGPoint firstLifePointsPosition;
    if ([device isEqualToString:@"IPHONE"]) {
        firstLifePointsPosition = CGPointMake(x, 140);
    }else{
        firstLifePointsPosition = CGPointMake(x, 110);
    }
    UITextView *firstLifePointsTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 268/2, 57/2)];
    firstLifePointsTextView.layer.position = firstLifePointsPosition;
    firstLifePointsTextView.text = @"10 pointe";
    if (hintUsed || hintsRemaining <1) {
        firstLifePointsTextView.textColor = inactiveColor;
    }else{
      firstLifePointsTextView.textColor = [UIColor greenSpacesColor];
    }
    firstLifePointsTextView.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:13];
    NSLog(@"Font Names %@", [UIFont fontNamesForFamilyName:@"Avenir Next"]);
    firstLifePointsTextView.textAlignment = NSTextAlignmentCenter;
    firstLifePointsTextView.backgroundColor = [UIColor clearColor];
    [self addSubview:firstLifePointsTextView];
    
    //second Life points
    CGPoint secondLifePointsPosition;
    if ([device isEqualToString:@"IPHONE"]) {
        secondLifePointsPosition = CGPointMake(thirds + x, 140);
    }else{
        secondLifePointsPosition = CGPointMake(thirds + x, 110);
    }
    UITextView *secondLifePointsTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 268/2, 57/2)];
    secondLifePointsTextView.layer.position = secondLifePointsPosition;
    secondLifePointsTextView.text = @"20 pointe";
    if (usedFirstLetter || firstLetterRemaing <1) {
        secondLifePointsTextView.textColor = inactiveColor;
    }else{
    secondLifePointsTextView.textColor = [UIColor greenSpacesColor];
    }
    secondLifePointsTextView.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:13];
    secondLifePointsTextView.textAlignment = NSTextAlignmentCenter;
    secondLifePointsTextView.backgroundColor = [UIColor clearColor];
    [self addSubview:secondLifePointsTextView];
    
    //third Life points
    CGPoint thirdLifePointsPosition;
    if ([device isEqualToString:@"IPHONE"]) {
        thirdLifePointsPosition = CGPointMake(thirds + thirds + x, 140);
    }else{
        thirdLifePointsPosition = CGPointMake(thirds + thirds + x, 110);
    }
    UITextView *thirdLifePointsTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 268/2, 57/2)];
    thirdLifePointsTextView.layer.position = thirdLifePointsPosition;
    thirdLifePointsTextView.text = @"50 pointe";
    if (answerShown || answersRemaining < 1) {
        thirdLifePointsTextView.textColor = inactiveColor;
    }else{
    thirdLifePointsTextView.textColor = [UIColor greenSpacesColor];
    }
    thirdLifePointsTextView.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:13];
    thirdLifePointsTextView.textAlignment = NSTextAlignmentCenter;
    thirdLifePointsTextView.backgroundColor = [UIColor clearColor];
    [self addSubview:thirdLifePointsTextView];
    
    //first Life explanation
    CGPoint firstLifeExplanPosition;
    UITextView *firstLifeExplainView;
    if ([device isEqualToString:@"IPHONE"]){
        firstLifeExplanPosition = CGPointMake(x, 100);
        firstLifeExplainView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 70, 60)];
        firstLifeExplainView.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:12];
    }else{
        firstLifeExplanPosition = CGPointMake(x, 88);
        firstLifeExplainView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 378/2, 60/2)];
        firstLifeExplainView.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:15];
    }

    firstLifeExplainView.layer.position = firstLifeExplanPosition;
    firstLifeExplainView.text = @"Tabhair dom leide";
    if (hintUsed || hintsRemaining < 1) {
        firstLifeExplainView.textColor = inactiveColor;
    }else{
    firstLifeExplainView.textColor = [UIColor whiteGreyColor];
    }
    firstLifeExplainView.textAlignment = NSTextAlignmentCenter;
    firstLifeExplainView.backgroundColor = [UIColor clearColor];
    [self addSubview:firstLifeExplainView];
    
    CGPoint secondLifeExplanPos;
    UITextView *secondLifeExplanView;
    if ([device isEqualToString:@"IPHONE"]) {
        secondLifeExplanPos = CGPointMake(thirds + x, 100);
        secondLifeExplanView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 70, 60)];
        secondLifeExplanView.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:12];
    }else{
        secondLifeExplanPos = CGPointMake(thirds + x, 88);
        secondLifeExplanView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 378/2, 60/2)];
        secondLifeExplanView.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:15];
    }
    secondLifeExplanView.layer.position = secondLifeExplanPos;
    secondLifeExplanView.text = @"Nochtaigh na chéad litreacha";
    if (usedFirstLetter || firstLetterRemaing <1) {
      secondLifeExplanView.textColor = inactiveColor;
    }else{
        secondLifeExplanView.textColor = [UIColor whiteGreyColor];
    }
    secondLifeExplanView.textAlignment = NSTextAlignmentCenter;
    secondLifeExplanView.backgroundColor = [UIColor clearColor];
    [self addSubview:secondLifeExplanView];
    
    CGPoint thirdLifeExplanPos;
    UITextView *thirdLifeExplanView;
    if ([device isEqualToString:@"IPHONE"]) {
        thirdLifeExplanPos = CGPointMake(thirds + thirds + x, 100);
        thirdLifeExplanView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 70, 60)];
        thirdLifeExplanView.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:12];
    }else{
        thirdLifeExplanPos = CGPointMake(thirds + thirds + x, 88);
        thirdLifeExplanView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 378/2, 60/2)];
        thirdLifeExplanView.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:15];
    }
    thirdLifeExplanView.layer.position = thirdLifeExplanPos;
    thirdLifeExplanView.text = @"Tabhair dom an freagra";
    thirdLifeExplanView.textAlignment = NSTextAlignmentCenter;
    if (answerShown || answersRemaining <1) {
        thirdLifeExplanView.textColor = inactiveColor;
    }else{
    thirdLifeExplanView.textColor = [UIColor whiteGreyColor];
    }
    thirdLifeExplanView.backgroundColor = [UIColor clearColor];
    [self addSubview:thirdLifeExplanView];
    
    _removeView = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 20, 20)];
    [_removeView setBackgroundColor:[UIColor clearColor]];
    [_removeView setTitle:@"X" forState:UIControlStateNormal];
    [_removeView.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:20]];
    [_removeView addTarget:self.window.rootViewController action:@selector(removeHintsPanel) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_removeView];
    
    //Animate OnScreen
    [UIView animateWithDuration:0.6f animations:^{
        
        //_hintsPanel = [[hintsPanel alloc]initWithFrame:CGRectMake(35, self.view.bounds.size.height + 60, 282, 60)];
        if ([device isEqualToString:@"IPHONE"]) {
            [self setFrame:CGRectMake(19, screen.size.height / 1.57377, 284, 164)];
          // [self setFrame:CGRectMake(19, 305, 284, 164)];
        }else{
           [self setFrame:CGRectMake(99, 753, 562, 139)];
        }
        
    }];
    
}

-(void)giveHint{
    _firstIcon.userInteractionEnabled = NO;
    [UIView animateWithDuration:.3f delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _firstIcon.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished) {
        [(GameViewController *)self.window.rootViewController showHint];
        [UIView animateWithDuration:.3f delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            _firstIcon.transform = CGAffineTransformMakeScale(1, 1);
            [_firstIcon setTintColor:[UIColor inactiveGreyColor]];
        } completion:^(BOOL finished) {
            //show Hint
            [(GameViewController *)self.window.rootViewController removeHintsPanel];
        }];
    }];
}

-(void)showFirstLetter{
    _secondIcon.userInteractionEnabled = NO;
    [UIView animateWithDuration:.3f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _secondIcon.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished){
        [(GameViewController *)self.window.rootViewController showFirstLetter];
        [UIView animateWithDuration:.3f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            _secondIcon.transform = CGAffineTransformMakeScale(1, 1);
            [_secondIcon setTitleColor:[UIColor inactiveGreyColor]
                              forState:UIControlStateNormal];
        } completion:^(BOOL finished) {
            [(GameViewController *)self.window.rootViewController removeHintsPanel];
        }];

    }];
    
}

-(void)showAnswer {
    GameViewController *gVC = (GameViewController *)self.window.rootViewController;
    _thirdIcon.userInteractionEnabled = NO;
    
    [UIView animateWithDuration:.3f
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _thirdIcon.transform = CGAffineTransformMakeScale(1.2, 1.2);
                     }
                     completion:^(BOOL finished){
                         [gVC showAnswer];
                         [UIView animateWithDuration:.3f
                                               delay:0.0
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              _thirdIcon.transform = CGAffineTransformMakeScale(1, 1);
                                              [_thirdIcon setTitleColor:[UIColor inactiveGreyColor]
                                                               forState:UIControlStateNormal];
                                          }
                                          completion:^(BOOL finished) {
                                              [(GameViewController *)self.window.rootViewController removeHintsPanel];
                                          }];
                     }];
}

@end

//
//  favouriteCellView.h
//  Facts
//
//  Created by Linda Keating on 24/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface favouriteCellView : UITableViewCell
@property (strong, nonatomic) UILabel *position;
@property (strong, nonatomic) UIImageView *profilePic;
@property (strong, nonatomic) UILabel *userName;
@property (strong, nonatomic) UILabel *points;

@end

//
//  factObject.h
//  Facts
//
//  Created by Linda Keating on 17/12/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface factObject : NSObject

@property(nonatomic,readwrite) int factID;
@property(nonatomic,readwrite,retain) NSString *statement;
@property(nonatomic,readwrite) NSInteger isCorrect;
@property(nonatomic,readwrite,retain) NSString *additionalInfo;

@end

//
//  favouritedCell.h
//  Facts
//
//  Created by Linda Keating on 28/05/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "favouriteLabel.h"

@interface favouritedCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *favouriteStar;

@property (nonatomic, weak) IBOutlet favouriteLabel *favouriteText;
@property (nonatomic, weak) IBOutlet favouriteLabel *answer;

@end


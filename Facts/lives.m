//
//  lives.m
//  Facts
//
//  Created by Linda Keating on 27/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "lives.h"
#import "UIColor+ColorExtensions.h"

@implementation lives
    -(instancetype) initWithString:(NSString *)lifeLine  lives:(float)live{
        
        UIColor *usedColor = [UIColor whiteGreyColor];
        UIColor *unusedColor = [UIColor greenSpacesColor];
        self = [super init];
        if (self) {
            [self setSize:CGSizeMake(80, 20)];
            // first Life
            SKShapeNode *circle = [[SKShapeNode alloc] init];
            CGRect circleRect = CGRectMake(0, 0, 6, 6);
            circle.path = [UIBezierPath bezierPathWithOvalInRect:circleRect].CGPath;
            if (live < 3) {
                circle.strokeColor = circle.fillColor = usedColor;
            }
            else{
                circle.strokeColor = circle.fillColor = unusedColor;
            }
            circle.position = CGPointMake(0, 0);
            circle.name = @"life1";
            [self addChild:circle];
            
            // second Life
            SKShapeNode *circle2 = [[SKShapeNode alloc] init];
            CGRect circleRect2 = CGRectMake(0, 0, 6, 6);
            circle2.path = [UIBezierPath bezierPathWithOvalInRect:circleRect2].CGPath;
            if (live <2) {
                circle2.strokeColor = circle2.fillColor = usedColor;
            }else{
                circle2.strokeColor = circle2.fillColor = unusedColor;
            }

            circle2.position = CGPointMake(CGRectGetMinX(circle.frame)+10, 0);
            circle2.name = @"life2";
            [self addChild:circle2];
            
            // third Life
            SKShapeNode *circle3 = [[SKShapeNode alloc] init];
            CGRect circleRect3 = CGRectMake(0, 0, 6, 6);
            circle3.path = [UIBezierPath bezierPathWithOvalInRect:circleRect3].CGPath;
            if (live <1) {
                circle3.strokeColor = circle3.fillColor = usedColor;
            }else{
                circle3.strokeColor = circle3.fillColor = unusedColor;
            }
            circle3.position = CGPointMake(CGRectGetMinX(circle2.frame)+10, 0);
            circle3.name = @"life3";
            [self addChild:circle3];
            
            self.lifeName = lifeLine;
            self.livesLeft = live;
            self.lifeOneCircle = circle;
            self.lifeTwoCircle = circle2;
            self.lifeThreeCircle = circle3;
            
        }
        
        return self;
    }

-(void)updateLives:(long)livesLeft {
    if (livesLeft == 2) {
        self.lifeOneCircle.strokeColor =  self.lifeOneCircle.fillColor = [UIColor whiteGreyColor];
    }else if (livesLeft == 1){
        self.lifeTwoCircle.strokeColor =  self.lifeTwoCircle.fillColor = [UIColor whiteGreyColor];
    }else{
        self.lifeThreeCircle.strokeColor = self.lifeThreeCircle.fillColor = [UIColor whiteGreyColor];
    }
}




@end

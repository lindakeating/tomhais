//
//  GameViewController.m
//  Facts
//
//  Created by Linda Keating on 09/12/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "GameViewController.h"
#import "favouritesViewController.h"
#import "GameScene.h"
#import "quickBloxConnection.h"
#import "rightAnswer.h"
#import "LnbScene.h"
#import "UIColor+ColorExtensions.h"
#import "DBManager.h"
#import "favouritedViewController.h"



GameScene *gameScene;
LnbScene *lnbScene;
hintView *hView;
NSString *device;
@interface GameViewController()

@property (nonatomic, strong) DBManager *dbManager;

@end

@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end

@implementation GameViewController{
    UIView *loginImageView;
    UIImage *image;
    CGRect screen;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // figure out if it is an IPHONE or an IPAD
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"IPAD";
    }else{
        device = @"IPHONE";
    }
    screen = [[UIScreen mainScreen]bounds];
    
    // Configure the view.
    SKView * skView = (SKView *)self.view;
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    skView.ignoresSiblingOrder = YES;
    
    _defaults = [NSUserDefaults standardUserDefaults];
    
   /* // Create and configure the scene.
    LnbScene *scene = [LnbScene unarchiveFromFile:@"LnbScene"];
    scene.scaleMode = SKSceneScaleModeResizeFill;
   // scene.backgroundColor = [SKColor blueColor];
    lnbScene = scene;
    
    // Present the scene.
    [skView presentScene:scene];*/
    
    NSLog(@"moveToLnb");
    lnbScene = [[LnbScene alloc] initWithSize:CGSizeMake(CGRectGetMaxX(self.view.frame), CGRectGetMaxY(self.view.frame))];
    lnbScene.device = device;
    [skView presentScene:lnbScene];
    
    // Initialize the dbManager object
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"tomhaisdb.sql"];

}


- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

-(void) showModal{
    UIAlertView *alert = [[UIAlertView alloc] init];
    UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, 300, 300)];
    lblText.text = @"User came Online\n";
    lblText.font = [UIFont systemFontOfSize:15.0f];
    lblText.numberOfLines = 2;
    lblText.backgroundColor = [UIColor clearColor];
    lblText.textColor = [UIColor whiteColor];
    lblText.center = CGPointMake(140, 45);
    [alert addSubview:lblText];
    [alert show];
    
}

-(void)loadLoginView {
    //check to see if session has timedOut and create a new session
    [self checkSession];
    _loginView = [[logInview alloc] initWithFrame:self.view.frame];
    [_loginView loadLogInView];
    [self.view addSubview:_loginView];
}


-(void)checkSession{
    NSDate *sessionExpiryDate = [QBBaseModule sharedModule].tokenExpirationDate;
    NSDate *currentDate = [NSDate date];
    if ([sessionExpiryDate compare:currentDate] == NSOrderedAscending) {
        // create a new Session
        [QBApplication sharedApplication].applicationId = 19363;
        [QBConnection registerServiceKey:@"fMsynce3COFf8rY"];
        [QBConnection registerServiceSecret:@"gxAnD2OVOgxjC4S"];
        [QBSettings setAccountKey:@"CZ3BMs9RTzH8xphF66eV"];
        [QBRequest createSessionWithSuccessBlock:^(QBResponse *response, QBASession *session) {
            NSLog(@"Session Recreated");
        } errorBlock:^(QBResponse *response) {
            NSLog(@"%@", response.error);
        }];
    }
}

-(void) checkSessionAndSignInWithUserName:(NSString *)userName password:(NSString *)password{
    NSDate *sessionExpiryDate = [QBBaseModule sharedModule].tokenExpirationDate;
    NSDate *currentDate = [NSDate date];
    if ([sessionExpiryDate compare:currentDate] == NSOrderedAscending) {
        // create a new Session
        [QBApplication sharedApplication].applicationId = 19363;
        [QBConnection registerServiceKey:@"fMsynce3COFf8rY"];
        [QBConnection registerServiceSecret:@"gxAnD2OVOgxjC4S"];
        [QBSettings setAccountKey:@"CZ3BMs9RTzH8xphF66eV"];
        
        [QBRequest createSessionWithSuccessBlock:^(QBResponse *response, QBASession *session) {
            [self signInUser:userName Password:password];
        } errorBlock:^(QBResponse *response) {
            NSLog(@"%@", response.error);
        }];
    }
    else{
        [self signInUser:userName Password:password];
    }
}

-(void) checkSessionAndSignInWithUserName:(NSString *)userName password:(NSString *)password profilePic:(UIImageView *)profilePic {
    NSDate *sessionExpiryDate = [QBBaseModule sharedModule].tokenExpirationDate;
    NSDate *currentDate = [NSDate date];
    if ([sessionExpiryDate compare:currentDate] == NSOrderedAscending) {
        // create a new Session
        [QBApplication sharedApplication].applicationId = 19363;
        [QBConnection registerServiceKey:@"fMsynce3COFf8rY"];
        [QBConnection registerServiceSecret:@"gxAnD2OVOgxjC4S"];
        [QBSettings setAccountKey:@"CZ3BMs9RTzH8xphF66eV"];
        
        [QBRequest createSessionWithSuccessBlock:^(QBResponse *response, QBASession *session) {
            [self signInUser:userName Password:password];
        } errorBlock:^(QBResponse *response) {
            NSLog(@"%@", response.error);
        }];
    }
    else{
        [self signInUser:userName Password:password];
    }
}

-(void)loadWrongAnswer{
    if ([device isEqualToString:@"IPHONE"]) {
       _wrongAnswerNotification = [[wrongAnswerNotification alloc] initWithFrame:CGRectMake(self.view.frame.size.width+ 20, 292, 282, 25)];
    }else{
       _wrongAnswerNotification = [[wrongAnswerNotification alloc] initWithFrame:CGRectMake(self.view.frame.size.width+102, 1033/2, 1109/2, 95/2)];
    }
    [_wrongAnswerNotification loadWrongAnswerNotification];
    
    [self.view addSubview:_wrongAnswerNotification];
     NSLog(@"Login View frame= %@", NSStringFromCGRect(_wrongAnswerNotification.frame));
}

-(void)removeWrongAnswer {
    LnbScene *lbScene = lnbScene;
    [lbScene setNeutralTileColor];
    lbScene.userInteractionEnabled = YES;
    NSLog(@"wrong answer remove");
}

-(void)createNewUser:(NSString*)userName Password:(NSString*)password{
    QBUUser *user = [QBUUser user];
    user.password = password;
    user.login = userName;
    [QBRequest signUp:user
         successBlock:^(QBResponse *response, QBUUser *user) {
             NSLog(@"Response success description = %@", response.description);
             // save the details to NSDefaults
             [_defaults setObject:userName forKey:@"userName"];
             [_defaults setObject:password forKey:@"password"];
             [_defaults setInteger:user.ID forKey:@"userId"];
             [_defaults synchronize];
             [self signInUser:user.login Password:password];
             
         } errorBlock:^(QBResponse *response) {
             // show login Screen again - with error message something went wrong
             NSLog(@"Response failure description = %@", response.description);
             NSLog(@"Response failure = %@", response.error.description);
         }];
}

-(void)signInUser:(NSString*)userName Password:(NSString*)password{
    QBUUser *user = [QBUUser user];
    user.password = password;
    user.login = userName;
    [QBRequest logInWithUserLogin:userName
                         password:password
                     successBlock:^(QBResponse *response, QBUUser *user) {
                         // send off current score to QuickBlox
                         _user = user;
                         if (image) {
                             // upload the image to blob
                             NSData *imageData = UIImagePNGRepresentation(image);
                             [QBRequest TUploadFile:imageData
                                           fileName:userName
                                        contentType:@"image/png"
                                           isPublic:YES successBlock:^(QBResponse *response, QBCBlob *blob) {
                                               NSString *url =[blob publicUrl];
                                               // report Score to QuickBlox with image URL
                                               [self reportScoreToQuickBloxWithUrl:url];
                                           } statusBlock:^(QBRequest *request, QBRequestStatus *status) {
                                               // handle progress
                                           } errorBlock:^(QBResponse *response) {
                                               NSLog(@"error: %@", response.error);
                                           }];
                             NSLog(@"Image Found");
                         }else{
                             [self reportScoreToQuickBlox];
                         }
                     }
                       errorBlock:^(QBResponse *response) {
                           NSLog(@"Failure response = %@", response.error.description);
                       }];
}

-(void) reportScoreToQuickBloxWithUrl:(NSString*)url{
    NSNumber *score = [[NSNumber alloc] initWithLong:[gameData sharedGameData].score];
    QBCOCustomObject *object = [QBCOCustomObject customObject];
    object.className = @"leaderBoard";
    [object.fields setObject:score forKey:@"points"];
    [object.fields setObject:_user.login forKey:@"userName"];
    [object.fields setObject:url forKey:@"imageUrl"];
    [QBRequest createObject:object successBlock:^(QBResponse *response, QBCOCustomObject *object) {
        [self showLeaderBoard];
    } errorBlock:^(QBResponse *response) {
        //log the error
    }];
}

-(void) reportScoreToQuickBlox{
    // if user exists already update score else create score
    NSMutableDictionary *getRequest = [NSMutableDictionary dictionary];
    NSNumber *userId = [NSNumber numberWithLong:_user.ID];
    [getRequest setObject:userId
                   forKey:@"user_id"];
    [QBRequest objectsWithClassName:@"leaderBoard"
                    extendedRequest:getRequest
                       successBlock:^(QBResponse *response, NSArray *objects, QBResponsePage *page) {
                           // if ther objects array contains no records create a new Score Record
                           [self updateOrCreateScore:objects];
                       } errorBlock:^(QBResponse *response) {
                           // log the error
                       }];
}

-(void) updateOrCreateScore:(NSArray*)userObjects {
    NSNumber *score = [[NSNumber alloc] initWithLong:[gameData sharedGameData].score];
    if ([userObjects count] == 0) {
        // create a new record
        QBCOCustomObject *object = [QBCOCustomObject customObject];
        object.className = @"leaderBoard";
        [object.fields setObject:score
                          forKey:@"points"];
        [object.fields setObject:_user.login
                          forKey:@"userName"];
        [QBRequest createObject:object
                   successBlock:^(QBResponse *response, QBCOCustomObject *object) {
                       //retrieve all score records
                       [self showLeaderBoard];
                   }
                     errorBlock:^(QBResponse *response) {
                         // log the error
        }];
    }
    else{
        // update the record
        QBCOCustomObject *updateObject = [QBCOCustomObject customObject];
        updateObject.className = @"leaderBoard";
        [updateObject.fields setObject:score
                                forKey:@"points"];
        QBCOCustomObject *cObject = [userObjects objectAtIndex:0];
        updateObject.ID = cObject.ID;
        
        [QBRequest updateObject:updateObject
                   successBlock:^(QBResponse *response, QBCOCustomObject *object) {
                       // got to the leader Board once object has been updated
                       [self showLeaderBoard];
                   } errorBlock:^(QBResponse *response) {
                       // log the error
                   }];
      }
}


-(void)showHintsPanel{
    if([device isEqualToString:@"IPHONE"]){
    _hintsPanel = [[hintsPanel alloc]initWithFrame:CGRectMake(19, self.view.bounds.size.height + 60, 284, self.view.bounds.size.height /2.9)];
    }else{
    _hintsPanel = [[hintsPanel alloc]initWithFrame:CGRectMake(198/2, self.view.bounds.size.height + (227/2), 1130/2, 277/2)];
    }
    [_hintsPanel loadHintsPanel:_hintUsed
                firstLetterUsed:(BOOL)_usedFirstLetter
                    answerShown:(BOOL)_answerShown
                 hintsRemaining:(int)[gameData sharedGameData].hints
           firstLetterRemaining:(int)[gameData sharedGameData].firstLetters
               answersRemaining:(int)[gameData sharedGameData].answers];
    [self.view addSubview:_hintsPanel];
    [gameScene.lnbScene setUserInteractionEnabled:NO];    
}

-(void)removeHintsPanel{
    //Animate OnScreen
    [gameScene.lnbScene setUserInteractionEnabled:YES];
    [UIView animateWithDuration:0.6f
                     animations:^{
                         if ([device isEqualToString:@"IPHONE"]) {
                            [self.hintsPanel setFrame:CGRectMake(19, self.view.bounds.size.height + 60, 284, 164)];
                         }else{
                            [self.hintsPanel setFrame:CGRectMake(198/2, self.view.bounds.size.height + (227/2), 1130/2, 277/2)];
                         }
                    }
                     completion:^(BOOL finished) {
                         [_hintsPanel removeFromSuperview];
     }];
}

-(void)toggleMenu {
    [gameScene.lnbScene setUserInteractionEnabled:NO];
     UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    _blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    _blurView.frame = self.view.bounds;
    _blurView.layer.opacity = .0f;
    _blurView.layer.backgroundColor = [UIColor colorWithRed:38.0f/255.0f
                                                      green:42.0f/255.0f
                                                       blue:46.0f/255.0f
                                                      alpha:.8f].CGColor;
    [UIView animateWithDuration:.3f
                     animations:^{
                         [_blurView.layer setOpacity:.6f];
                     }];
    [self.view addSubview:_blurView];
    
    
    UIVisualEffectView *visualEffectView;
    visualEffectView.frame = self.view.bounds;
    
    if([device isEqualToString:@"IPHONE"]){
       _menu = [[Menu alloc] initWithFrame:CGRectMake(self.view.bounds.size.width + 216, 18, 216, 549)];
    }else{
       _menu = [[Menu alloc] initWithFrame:CGRectMake(self.view.bounds.size.width + 460, 33, 460, 992)];
    }
    [_menu initialiseMenu];
    [self.view addSubview:_menu];
    
}

-(void)goToHomeScreen{
    [self removefavouriteVC];
    [self removeSavedRiddles];
    [self unloadMenu];
}

-(void)goToFavourites{
    [self removefavouriteVC];
    [self unloadMenu];
   /* _savedRiddleVC = [[SavedRiddlesViewController alloc] init];
    [self.view addSubview:_savedRiddleVC.view];*/
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    favouritedViewController *favVC = [storyboard instantiateViewControllerWithIdentifier:@"favouritesStoryboard"];
    [self presentViewController:favVC animated:YES completion:nil];
        
}


-(void)goToLeaderBoard{
    [self unloadMenu];
    // check if the user has already registered
    if ([[[_defaults dictionaryRepresentation]allKeys]containsObject:@"userId"]) {
        // load loading View
        [self loadLoadingView];
        // load leaderBoard View
        [self checkSessionAndSignInWithUserName:[_defaults valueForKey:@"userName"] password:[_defaults valueForKey:@"password"]];
       // [self signInUser:[_defaults valueForKey:@"userName"] Password:[_defaults valueForKey:@"password"]];
            }else{
        // register the user
        [self loadLoginView];
    }
}

-(void) loadLoadingView{
    if ([device isEqualToString:@"IPHONE"]) {
        _loadingView = [[LoadingView alloc] initWithFrame:CGRectMake(0, 52, 320, screen.size.height)];
    }else{
        _loadingView = [[LoadingView alloc] initWithFrame:CGRectMake(0, 97, 768, 1021)];
    }
    
    [_loadingView  setIndicator];
    [self.view addSubview:_loadingView];
}
-(void) removeSavedRiddles{
    [_savedRiddleVC.view removeFromSuperview];
    [_savedRiddleVC removeFromParentViewController];
}

-(void) removefavouriteVC{
    [_favouriteVC.view removeFromSuperview];
    [_favouriteVC removeFromParentViewController];
}

-(void) showLeaderBoard {
    // retrieve all the scores into an array
    NSMutableDictionary *getRequest = [NSMutableDictionary dictionary];
    [getRequest setObject:@"points" forKey:@"sort_desc"];
    [QBRequest objectsWithClassName:@"leaderBoard"
                    extendedRequest:getRequest
                       successBlock:^(QBResponse *response, NSArray *objects, QBResponsePage *page) {
                           // load the new view Controller
                           [_loadingView removeFromSuperview];
                           _favouriteVC = [[favouritesViewController alloc] init];
                           _favouriteVC.favouriteResults = objects;
                           _favouriteVC.user = _user;
                           [self.view addSubview:_favouriteVC.view];
                       } errorBlock:^(QBResponse *response) {
                           //Log the Error
                       }];
}

-(void)goToLifelines{
    [self removefavouriteVC];
    [self unloadMenu];
    [self showHintsPanel];
}

-(void) unloadMenu{
    [gameScene.lnbScene setUserInteractionEnabled:YES];
    [UIView animateWithDuration:.6f animations:^{
        if ([device isEqualToString:@"IPHONE"]) {
            [_menu setFrame:CGRectMake(self.view.bounds.size.width, 18, 216,549)];
        }else{
           [_menu setFrame:CGRectMake(self.view.bounds.size.width, 33, 460, 992)];
        }
        [_blurView.layer setOpacity:.0f];
    } completion:^(BOOL finished) {
        [_menu removeFromSuperview];
        [_blurView removeFromSuperview];
    }];
}

-(void)unloadMenu:(myCompletion)completed{
    [gameScene.lnbScene setUserInteractionEnabled:YES];
    [UIView animateWithDuration:.6f animations:^{
        [_menu setFrame:CGRectMake(self.view.bounds.size.width, 33, 460, 992)];
        [_blurView.layer setOpacity:.0f];
    } completion:^(BOOL finished) {
        [_menu removeFromSuperview];
        [_blurView removeFromSuperview];
        completed(YES);
    }];
    
}

-(void) showRightAnswer:(NSString*)answer{
    _rightAnswerView = [[rightAnswer alloc] initWithFrame:CGRectMake(self.view.bounds.size.width, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [_rightAnswerView initWithAnswer:answer];
    [self.view addSubview:_rightAnswerView];
}
-(void) nextRiddle{
    // show next Riddle on LnbScene
    LnbScene *lScene = gameScene.lnbScene;
    [lScene nextRiddle];
    //close the window that is shown now.
    /*[UIView animateWithDuration:.6f animations:^{
        [_rightAnswerView setFrame:CGRectMake(self.view.bounds.size.width, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    }completion:^(BOOL finished) {
        [_rightAnswerView removeFromSuperview];
    }];*/
    
}

-(void)showHint{
    if ([device isEqualToString:@"IPHONE"]) {
        hView = [[hintView alloc] initWithFrame:CGRectMake(self.view.frame.size.width, CGRectGetMinY(self.view.frame) + (self.view.frame.size.height / 1.64), 282, self.view.frame.size.height / 17.142)];
    }else{
      hView = [[hintView alloc] initWithFrame:CGRectMake(self.view.frame.size.width+102, 1033/2, 1109/2, 95/2)];
    }
    LnbScene *lScene = gameScene.lnbScene;
    [lScene.hintLives   updateLives:[gameData sharedGameData].hints -1] ;
    [hView loadHintWithStyles:lScene.currentTomhais.hint];
    [self.view addSubview:hView];
     _hintUsed = TRUE;
    [gameData sharedGameData].hints -= 1;
    [[gameData sharedGameData]save];
    //update the amount of hints the User Has left
}

-(void)removeHintShown {
    [hView removeFromSuperview];
}
-(void) showFirstLetter{
    // show the first letter in the page
    LnbScene *scene = lnbScene;
    NSArray *tilesArray = [scene.tilesBoundary children];
    NSArray *spaceArray = [scene.spacesBoundary children];
    NSMutableArray *spacesArrayWithOutWhiteSpaces = [[NSMutableArray alloc] init];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) , ^{
        // update the score to Game Data
        [gameData sharedGameData].score -= 20;
        [gameData sharedGameData].firstLetters -= 1;
        [[gameData sharedGameData]save];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            for (letterSpace *lSpace in spaceArray) {
                if (lSpace.whiteSpace == FALSE) {
                    [spacesArrayWithOutWhiteSpaces addObject:lSpace];
                }
            }
            for (int i=0; i<[tilesArray count]; i++) {
                Tile *tile = [tilesArray objectAtIndex:i];
                if (tile.firstLetter == YES) {
                    [tile removeFromParent];
                    NSArray *ar = [tile children];
                    SKLabelNode *labelNode = [ar objectAtIndex:0];
                    labelNode.userInteractionEnabled = NO;
                    [tile setUserInteractionEnabled:NO];
                    letterSpace *lSpace = [spacesArrayWithOutWhiteSpaces objectAtIndex:tile.positionInAnswer];
                    [scene.tilesCollection removeObject:tile];
                    lSpace.occupied = TRUE;
                    lSpace.character = tile.character;
                    [scene.spacesBoundary addChild:tile];
                    CGPoint targetPoint;
                    if([device isEqualToString:@"IPHONE"]){
                        targetPoint = CGPointMake(tile.targetPoint.x +15, tile.targetPoint.y +3 +15);
                    }else{
                        targetPoint = CGPointMake(tile.targetPoint.x +30, tile.targetPoint.y +6 +30);
                    }
                    tile.position = targetPoint;
                    tile.characterNode.fontColor = [UIColor greenSpacesColor];
                    tile.moved = TRUE;
                    tile.name = @"firstLetterTile";
                }
            }
            
            // update the score on the LnBInterface
            [scene.score setText:[NSString stringWithFormat:@"%ld" , [gameData sharedGameData].score -20]];
            
            // update the amount of lives left on the hints icons indicators
            [scene.firstLetterLives   updateLives:[gameData sharedGameData].firstLetters] ;
            
            // set the button to greyed out on the hints panel
            _usedFirstLetter = TRUE;
        });
    });
}

// take the tiles that have been moved and remove them from the blanks
-(void)removeTilesFromSpaces:(myCompletion)finished{
    LnbScene *scene = lnbScene;
    NSArray *tilesArray = [scene.tilesBoundary children];
    for (Tile *tile in tilesArray) {
        if (tile.moved == YES) {
            tile.position = tile.originalPoint;
        }
    }
    finished(YES);
}

// show Answer Hints version
-(void) showAnswer {
   [self showAnswer:^(BOOL finished) {
       if (finished) {
           [self animateRightAnswer];
       }
   }];
}

// show Answer when the right answer has been provided
-(void) showAnswer:(myCompletion) compblock{
    LnbScene *scene = lnbScene;
    NSArray *tilesArray = [scene.tilesBoundary children];
    
      for (Tile *tile in tilesArray) {
          if (tile.positionInAnswer != 17) {
              [tile removeFromParent];
              tile.name = @"answerTile";
              [scene.spacesBoundary addChild:tile];
              CGPoint targetPoint;
              if ([device isEqualToString:@"IPHONE"]) {
                  targetPoint = CGPointMake(tile.targetPoint.x + 15, tile.targetPoint.y + 6 + 15);
              }else{
                  targetPoint = CGPointMake(tile.targetPoint.x + 30, tile.targetPoint.y + 6 + 30);
              }
              tile.position = targetPoint;
          }
      }
    compblock(YES);
}

-(NSMutableArray *)orderAnswerTiles{
    LnbScene *scene = lnbScene;
    NSArray *letterArray = [scene.spacesBoundary children];
    NSMutableArray *tilesArray = [[NSMutableArray alloc] init];
    
    
    for (NSObject *obj in letterArray) {
        if ([obj isKindOfClass:[Tile class]]) {
            Tile *t = (Tile *)obj;
            t.characterNode.fontColor = [UIColor greenSpacesColor];
            [tilesArray addObject:t];
        }
    }
    
    NSMutableArray *orderedTilesArray = [[NSMutableArray alloc] initWithCapacity:[tilesArray count]];
    
    for (int i=0; i<[tilesArray count]; i++) {
        [orderedTilesArray addObject:[NSNull null]];
    }
    
    for (Tile *tile in tilesArray) {
        tile.alpha = 0.0f;
        [orderedTilesArray replaceObjectAtIndex:tile.positionInAnswer withObject:tile];
    }
    return orderedTilesArray;
}


// animate the right ansewr user submitted version
-(void) animateRightAnswer:(myCompletion)compblock{
    NSMutableArray *orderedTilesArray = [self orderAnswerTiles];
    
    NSMutableArray *sequence = [[NSMutableArray alloc] init];
    SKAction *fadeIn = [SKAction fadeInWithDuration:.08f];
    [sequence addObject:fadeIn];
    SKAction *sequenceArray = [SKAction sequence:sequence];
    
    int i = 0;
    [self runTileAnimationWithSubmittedRightAnswer:i tilesArray:orderedTilesArray animation:sequenceArray];
}

-(void)runTileAnimationWithSubmittedRightAnswer:(int)tileNumber tilesArray:(NSMutableArray *)tilesArray animation:(SKAction *)animationSequence {
    Tile *t = [tilesArray objectAtIndex:tileNumber];
    
    [t runAction:animationSequence completion:^{
        [self runNextTileAnimationAndPanel:tileNumber tilesArray:tilesArray animationSequence:animationSequence];
    }];
}

-(void) runNextTileAnimationAndPanel:(int)tileNumber tilesArray:(NSMutableArray *)tilesArray animationSequence:(SKAction *)animationSequence{
    tileNumber = tileNumber + 1;
    if (tileNumber < [tilesArray count]) {
        [self runTileAnimationWithSubmittedRightAnswer:tileNumber tilesArray:tilesArray animation:animationSequence];
    }else{
        [self finishShowAnswerWithRightAnswerSubmitted:^(BOOL finished) {
            if (finished) {
                // persist the score to the GameData
                [gameData sharedGameData].score += 20;
                [gameData sharedGameData].riddlesCompleted += 1;
                [[gameData sharedGameData]save];
            }
        }];
    }
}

-(void) animateRightAnswer{
    NSMutableArray *orderedTilesArray = [self orderAnswerTiles];
    
    NSMutableArray *sequence = [[NSMutableArray alloc] init];
    SKAction *fadeIn = [SKAction fadeInWithDuration:.08f];
    [sequence addObject:fadeIn];
    SKAction *sequenceArray = [SKAction sequence:sequence];
    
    int i = 0;
    [self runTileAnimation:i tilesArray:orderedTilesArray animationSequence:sequenceArray];

}

-(void)runTileAnimation:(int)tileNumber tilesArray:(NSMutableArray *)tilesArray animationSequence:(SKAction *)animationSequence{
    Tile *t = [tilesArray objectAtIndex:tileNumber];
    
     [t runAction:animationSequence
       completion:^{
           [self runNextTileAnimation:tileNumber tilesArray:tilesArray animationSequence:animationSequence];
       }];
               
}

-(void) runNextTileAnimation:(int)tileNumber tilesArray:(NSMutableArray *)tilesArray animationSequence:(SKAction *)animationSequence{
    tileNumber = tileNumber + 1;
    if (tileNumber < [tilesArray count]) {
        [self runTileAnimation:tileNumber tilesArray:tilesArray animationSequence:animationSequence];
    }else{
        [self finishShowAnswer:^(BOOL finished) {
            if (finished) {
                // persist the score to the GameData
                [gameData sharedGameData].score -= 50;
                [gameData sharedGameData].answers -= 1;
                [[gameData sharedGameData]save];
            }
        }];
    }
}

// for when the right answer is submitted
-(void) finishShowAnswerWithRightAnswerSubmitted:(myCompletion) compblock{
    
    // show the right answer Panel
    LnbScene *scene = lnbScene;
    NSString *answer = scene.currentTomhais.answer;
    [self showRightAnswer:answer];
    compblock(YES);
    
}

-(void) finishShowAnswer:(myCompletion) compblock{
    LnbScene *scene = (LnbScene *)gameScene.lnbScene;
    scene.currentRiddle += 1;
    [scene.answerLives updateLives:[gameData sharedGameData].answers -1];
    [scene.score setText:[NSString stringWithFormat:@"%ld" , [gameData sharedGameData].score -50]];
    [scene nextRiddle];
    _usedFirstLetter = FALSE;
    _hintUsed = FALSE;
    _answerShown = FALSE;
    compblock(YES);
}

-(void) takePicture:(UIView *)imageView{
    loginImageView = imageView;
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker
                       animated:YES
                     completion:^{
                         nil;
                     }];
}

-(void) selectPicture:(UIView *)imageView{
    loginImageView = imageView;
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *unrotatedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    image = [self rotateUIImage:unrotatedImage];
    
    UIImageView *pictureTakenView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, loginImageView.frame.size.width , loginImageView.frame.size.height)];
    pictureTakenView.tag = 100; // use this as an identifier to get the image when uploading to QuickBlox
    pictureTakenView.layer.cornerRadius = 72.5;
    pictureTakenView.clipsToBounds = YES;
    [pictureTakenView setImage:image];
    [loginImageView addSubview:pictureTakenView];
     [_loginView reverseFlipView];
    
    // You have the image. You can use this to present the image in the next view like you require in `#3`.
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 // save the image to the file
                                 NSString *directoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                                 [UIImagePNGRepresentation(image) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", @"profilePic", @"png"]] atomically:YES];
                             }];
}

-(UIImage*)rotateUIImage:(UIImage*)src {
    
    // No-op if the orientation is already correct
    if (src.imageOrientation == UIImageOrientationUp) return src ;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (src.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, src.size.width, src.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, src.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, src.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (src.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, src.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, src.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, src.size.width, src.size.height,
                                             CGImageGetBitsPerComponent(src.CGImage), 0,
                                             CGImageGetColorSpace(src.CGImage),
                                             CGImageGetBitmapInfo(src.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (src.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,src.size.height,src.size.width), src.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,src.size.width,src.size.height), src.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

-(void)setNextRiddleValues {
    LnbScene *scene = lnbScene;
    scene.currentRiddle += 1;
    [scene.score setText:[NSString stringWithFormat:@"%ld" , [gameData sharedGameData].score]];
    [scene nextRiddle];
    _usedFirstLetter = FALSE;
    _hintUsed = FALSE;
    _answerShown = FALSE;
}

#pragma mark - Sharing

-(void)shareScreen{
    [self unloadMenu:^(BOOL finished) {
        if (finished) {
            UIImage *screen = [self takeScreenShot];
            [self showShareModal:screen];
        }
    }];
}

-(UIImage *)takeScreenShot{
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];
    //[self.view.window.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

-(void)showShareModal:(UIImage *)img{
    NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/app/id933060855"];
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        NSLog(@"service available");
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [mySLComposerSheet setInitialText:@"Stuck on a particular problem"];
        [mySLComposerSheet addURL:url];
        [mySLComposerSheet addImage:img];
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result){
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        [self presentViewController:mySLComposerSheet animated:YES completion:^{
            // do something after the view is shown;
        }];
    }
}

-(void) addToFavourites:(tomhaisObject*)tomhaisObject{
   /* NSMutableArray *favourites = [[NSMutableArray alloc] initWithArray:[gameData sharedGameData].favourites];
    [favourites addObject:[NSNumber numberWithInt:tomhaisObject.tomhaisId]];
    [gameData sharedGameData].favourites = favourites;
    [[gameData sharedGameData]save];*/
    
    NSString *query = [NSString stringWithFormat:@"insert into favourites values(null, %d, '%@' , %d)" , tomhaisObject.tomhaisId ,tomhaisObject.question, 1];
    
    [self.dbManager executeQuery:query];
    
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
    }else{
        NSLog(@"Could not execute query");
    }
}

@end

//
//  wrongAnswerNotification.m
//  Facts
//
//  Created by Linda Keating on 09/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "wrongAnswerNotification.h"
#import "GameViewController.h"
#import "UIColor+ColorExtensions.h"

@implementation wrongAnswerNotification{
    NSString *device;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)loadWrongAnswerNotification {
    
    // figure out if it is an IPHONE or an IPAD
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"IPAD";
    }else{
        device = @"IPHONE";
    }
    self.backgroundColor = [UIColor backgroundColorGrey];
    self.layer.borderColor = [UIColor colorWithRed:251.0f/255.0f
                                             green:88.0f/255.0f
                                              blue:114.0f/255.0f
                                             alpha:.7].CGColor;
    self.layer.cornerRadius = 2.0f;
    
    UITextView *micheart;
    if ([device isEqualToString:@"IPHONE"]) {
        micheart = [[UITextView alloc] initWithFrame:CGRectMake(0, -6, self.frame.size.width, self.frame.size.height + 4)];
    }else{
        micheart = [[UITextView alloc] initWithFrame:CGRectMake(0, -4, self.frame.size.width, self.frame.size.height)];
    }

    [micheart setText:@"Mícheart! triail arís."];

    [micheart setTextColor:[UIColor warningRedColor]];
    [micheart setBackgroundColor:[UIColor clearColor]];
     micheart.textAlignment = NSTextAlignmentCenter;
     micheart.editable = NO;
    [self addSubview:micheart];

    if ([device isEqualToString:@"IPHONE"]) {
        self.layer.borderWidth = 2.0f;
        [micheart setFont:[UIFont fontWithName:@"Avenir Next" size:18]];
        [UIView animateWithDuration:0.6f
                              delay:.0f
             usingSpringWithDamping:2.0f
              initialSpringVelocity:1.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [self setFrame:CGRectMake(20, 292, 282, 27)];
                             [self setAlpha:1.0f];
                         } completion:^(BOOL finished) {
                             nil;
      }];
    }else{
        self.layer.borderWidth = 3.5f;
        [micheart setFont:[UIFont fontWithName:@"Avenir Next" size:32]];
        [UIView animateWithDuration:0.6f
                              delay:.0f
             usingSpringWithDamping:2.0f
              initialSpringVelocity:1.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [self setFrame:CGRectMake(205/2, 1033/2, 1109/2, 95/2)];
                             [self setAlpha:1.0f];
                         } completion:^(BOOL finished) {
                             nil;
       }];
    }


    
   [UIView animateWithDuration:0.5f delay:2.6f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [(GameViewController *)self.window.rootViewController removeWrongAnswer];
        [self removeFromSuperview];
    }];
    
    
}

@end

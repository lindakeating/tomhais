//
//  Tile.h
//  Facts
//
//  Created by Linda Keating on 23/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Tile : SKShapeNode

-(instancetype) initWithString:(NSString *)character xPosition:(float)x yPosition:(float)y letterSpaceIndex:(float)l;
-(instancetype) addHiddenTileToSpace:(NSString *)letter withIndex:(int)i withPositionInWord:(int)p device:(NSString *)device;

-(void) addAnimations;

  @property(nonatomic, assign) BOOL moved;
  @property(nonatomic, assign) BOOL firstLetter;
  @property (nonatomic, assign) CGPoint originalPoint;
  @property (nonatomic, assign) CGPoint targetPoint;
  @property (nonatomic, assign) float letterSpaceIndex;
  @property (nonatomic, strong) NSString *character;
  @property (nonatomic, assign) int positionInAnswer;
@property (nonatomic, retain) SKLabelNode *characterNode;

@end

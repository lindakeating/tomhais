//
//  GameScene.m
//  Facts
//
//  Created by Linda Keating on 09/12/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "GameScene.h"
#import "OptionsScene.h"
#import "FactsScene.h"
#import "LevelSelect.h"



@implementation GameScene {
    UIButton *startButton;
    UIButton *optionsButton;
    UIButton *exitButton;
    UIButton *lnbButton;
    NSString *device;
}

-(void)didMoveToView:(SKView *)view {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"IPAD";
    }else{
        device = @"IPHONE";
    }
    
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"LevelDescription" ofType:@"plist"];
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    

        NSMutableArray *array = [dictionary objectForKey:@"Questions"];
        
        for(int i = 0; i < [array count]; i++){
            NSMutableDictionary *questions = [array objectAtIndex:i];
            NSLog(@"ID %@", [questions objectForKey:@"id"]);
            NSLog(@"%@", [questions objectForKey:@"statement"]);
            NSLog(@"%@", [questions objectForKey:@"isCorrect"]);
            NSLog(@"%@", [questions objectForKey:@"additionalInfo"]);
        }

    /* Setup your scene here */
    
    startButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    startButton.frame = CGRectMake(CGRectGetMidX(self.frame)-230, CGRectGetMidY(self.frame)+180, 200, 70.0);
    startButton.backgroundColor = [UIColor clearColor];
    [startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *buttonImageNormal = [UIImage imageNamed:@"StartBtn.png"];
    UIImage *stretchableButtonImageNormal = [buttonImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [startButton setBackgroundImage:stretchableButtonImageNormal forState:UIControlStateNormal];
    [startButton addTarget:self action:@selector(moveToGame) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:startButton];
    
    optionsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    optionsButton.frame = CGRectMake(CGRectGetMidX(self.frame)-230, CGRectGetMidY(self.frame)+270, 200, 70.0);
    optionsButton.backgroundColor = [UIColor clearColor];
    [optionsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *buttonOptionsImageNormal = [UIImage imageNamed:@"OptionsBtn.png"];
    UIImage *stretchableButtonOptionsImageNormal = [buttonOptionsImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [optionsButton setBackgroundImage:stretchableButtonOptionsImageNormal forState:UIControlStateNormal];
    [optionsButton addTarget:self action:@selector(moveToOptions) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:optionsButton];
    
    exitButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    exitButton.frame = CGRectMake(CGRectGetMidX(self.frame)-230, CGRectGetMidY(self.frame)+360, 200, 70.0);
    exitButton.backgroundColor = [UIColor clearColor];
    [exitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *buttonExitImageNormal = [UIImage imageNamed:@"ExitBtn.png"];
    UIImage *stretchableButtonExitImageNormal = [buttonExitImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [exitButton setBackgroundImage:stretchableButtonExitImageNormal forState:UIControlStateNormal];
    [exitButton addTarget:self action:@selector(endApplication) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:exitButton];
    
    lnbButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    lnbButton.frame = CGRectMake(CGRectGetMidX(self.frame)-230, CGRectGetMidY(self.frame)-160, 200, 70.0);
    lnbButton.backgroundColor = [UIColor clearColor];
    [lnbButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *buttonLnbImageNormal = [UIImage imageNamed:@"ExitBtn.png"];
    UIImage *stretchableButtonLnbImageNormal = [buttonLnbImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [lnbButton setBackgroundImage:stretchableButtonLnbImageNormal forState:UIControlStateNormal];
    [lnbButton addTarget:self action:@selector(moveToLnb) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:lnbButton];
    
    
    
    
    
    SKLabelNode *myLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    myLabel.text = @"Facts";
    myLabel.fontSize = 65;
    myLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                   CGRectGetMidY(self.frame));
    
    [self addChild:myLabel];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

-(void)moveToGame{
    NSLog(@"MoveToGame");
    LevelSelect* factsScene = [[LevelSelect alloc]initWithSize:CGSizeMake((CGRectGetMaxX(self.frame)), CGRectGetMaxY(self.frame))];
    SKTransition* transition = [SKTransition revealWithDirection:SKTransitionDirectionUp duration:.5];
    [self removeMenuButtons];
    [self.scene.view presentScene:factsScene transition:transition];
}

-(void)moveToOptions{
    NSLog(@"moveToOptions");
    OptionsScene* optionsScene = [[OptionsScene alloc]initWithSize:CGSizeMake((CGRectGetMaxX(self.frame)), CGRectGetMaxY(self.frame))];
    SKTransition* transition = [SKTransition revealWithDirection:SKTransitionDirectionLeft duration:1];
    [self removeMenuButtons];
    [self.scene.view presentScene:optionsScene transition:transition];
}

-(void)moveToLnb{
    NSLog(@"moveToLnb");
    _lnbScene = [[LnbScene alloc]initWithSize:CGSizeMake((CGRectGetMaxX(self.frame)), CGRectGetMaxY(self.frame))];
    SKTransition* transition = [SKTransition revealWithDirection:SKTransitionDirectionLeft duration:1];
    [self removeMenuButtons];
    _lnbScene.device = device;
    [self.scene.view presentScene:_lnbScene transition:transition];
}

-(void) endApplication{
    [self removeMenuButtons];
    exit(0);
}

-(void)removeMenuButtons{
    [startButton removeFromSuperview];
    [optionsButton removeFromSuperview];
    [exitButton removeFromSuperview];
    [lnbButton removeFromSuperview];
}



@end

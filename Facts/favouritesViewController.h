//
//  favouritesViewController.h
//  Facts
//
//  Created by Linda Keating on 24/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Quickblox/Quickblox.h>
#import <QuartzCore/QuartzCore.h>



@interface favouritesViewController : UIViewController

@property (strong, nonatomic) NSArray *favouriteResults;
@property ( nonatomic) QBResponse *qbResponse;
@property (nonatomic) QBUUser *user;

@end

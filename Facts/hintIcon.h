//
//  hintIcon.h
//  Facts
//
//  Created by Linda Keating on 25/01/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface hintIcon : SKShapeNode

-(instancetype) initWithString:(NSString *)character size:(float)size lives:(float)lives;

@end

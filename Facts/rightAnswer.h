//
//  rightAnswer.h
//  Facts
//
//  Created by Linda Keating on 14/02/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "gameData.h"
#import "StarsView.h"


typedef void(^myCompletion)(BOOL);

@interface rightAnswer : UIView

@property (nonatomic) UIButton *submitButton;

-(void) initWithAnswer:(NSString *)answer;

@end

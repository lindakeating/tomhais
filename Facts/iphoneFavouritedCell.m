//
//  iphoneFavouritedCell.m
//  Facts
//
//  Created by Linda Keating on 03/06/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "iphoneFavouritedCell.h"

@implementation iphoneFavouritedCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

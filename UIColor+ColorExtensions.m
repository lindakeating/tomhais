//
//  UIColor+ColorExtensions.m
//  Facts
//
//  Created by Linda Keating on 15/04/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "UIColor+ColorExtensions.h"

@implementation UIColor (ColorExtensions)

+(UIColor *) backgroundColorGrey{
    return [UIColor colorWithRed:38.0f/255.0f green:42.0f/255.0f blue:46.0f/255.0f alpha:1];
}
+(UIColor *) foregroundColorGrey{
    return [UIColor colorWithRed:73.0f/255.0f green:79.0f/255.0f blue:89.0f/255.0f alpha:1];
}
+(UIColor *) greenSpacesColor{
    return [UIColor colorWithRed:80.0f/255.0f green:227.0f/255.0f blue:194.0f/255.0f alpha:1];
}
+(UIColor *) warningRedColor{
    return [UIColor colorWithRed:251.0f/255.0f green:88.0f/255.0f blue:114.0f/255.0f alpha:1];
}
+(UIColor *) textGreyColor{
    return [UIColor colorWithRed:150.0f/255.0f green:166.0f/255.0f blue:173.0f/255.0f alpha:1];
}
+(UIColor *) profileYellowColor{
    return [UIColor colorWithRed:243.0f/255.0f green:233.0f/255.0f blue:112.0f/255.0f alpha:1];
}

+(UIColor *) inactiveGreyColor{
    return [UIColor colorWithRed:84.0f/255.0f green:99.0f/255.0f blue:106.0f/255.0f alpha:1];
}

+(UIColor *) horizontalGreyColor{
    return [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1];
}

+(UIColor *) whiteGreyColor{
    return [UIColor colorWithRed:216.0f/255.0f green:216.0f/255.0f blue:216.0f/255.0f alpha:1];
}

@end

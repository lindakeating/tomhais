//
//  SavedRiddlesViewController.m
//  Facts
//
//  Created by Linda Keating on 25/03/2015.
//  Copyright (c) 2015 LindaKeating. All rights reserved.
//

#import "SavedRiddlesViewController.h"
#import "SavedRiddleCell.h"
#import "gameData.h"
#import "UIColor+ColorExtensions.h"


@interface SavedRiddlesViewController () <UITableViewDataSource , UITableViewDelegate>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) SavedRiddleCell *savedRiddle;
@property (strong, nonatomic) NSMutableDictionary *riddleDictionary;
@property (strong, nonatomic) NSMutableArray *favouritesArray;

@end

@implementation SavedRiddlesViewController{
    NSString *device;
    CGRect screen;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    // Get riddles from PList
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"tomhaiseanna"
                                                          ofType:@"plist"];
    _riddleDictionary  = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    // Get the Favourites from the User Defaults
   _favouritesArray = [[NSMutableArray alloc] initWithArray:[gameData sharedGameData].favourites];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"IPAD";
    }else{
        device = @"IPHONE";
    }
    
    screen = [[UIScreen mainScreen]bounds];
    
    NSString *reuseID = @"reuseID";
    
    self.view.backgroundColor = [UIColor backgroundColorGrey];
    
    if ([device isEqualToString:@"IPHONE"]) {
        self.view.frame = CGRectMake(0, 52, 320, screen.size.height);
    }else{
        self.view.frame = CGRectMake(0, 97, 768, screen.size.height);
    }
    
    UILabel *starLabel;
    if ([device isEqualToString:@"IPHONE"]) {
        starLabel = [[UILabel alloc] initWithFrame:CGRectMake(140, 18, 40, 34)];
        [starLabel setFont:[UIFont fontWithName:@"tomhais-icons" size:35]];
    }else{
        starLabel = [[UILabel alloc] initWithFrame:CGRectMake(360, 30, 50, 50)];
        [starLabel setFont:[UIFont fontWithName:@"tomhais-icons" size:50]];
    }
    [starLabel setText:@"e"];
    [starLabel setTextColor:[UIColor profileYellowColor]];
    [starLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:starLabel];
    
    UIView *topHorizontalLine;
    if ([device isEqualToString:@"IPHONE"]) {
        topHorizontalLine = [[UIView alloc] initWithFrame:CGRectMake(20, 62.5, 271, 1)];
    }else{
        topHorizontalLine = [[UIView alloc] initWithFrame:CGRectMake(167, 100, 464, 1)];
    }
    [topHorizontalLine setBackgroundColor:[UIColor horizontalGreyColor]];
    [self.view addSubview:topHorizontalLine];
    
    UIView *bottomHorizontalLine;
    if ([device isEqualToString:@"IPHONE"]) {
        bottomHorizontalLine = [[UIView alloc] initWithFrame:CGRectMake(20, screen.size.height / 1.25, 271, 1)];
    }else{
        bottomHorizontalLine = [[UIView alloc] initWithFrame:CGRectMake(167, 780, 464, 1)];
    }
    [bottomHorizontalLine setBackgroundColor:[UIColor horizontalGreyColor]];
    [self.view addSubview:bottomHorizontalLine];
    
    
    // Do any additional setup after loading the view.
    if ([device isEqualToString:@"IPHONE"]) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(25, 75, 261, self.view.frame.size.height - 200)];
    }else{
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(174, 130, 450, 626)];
    }
    [self.tableView registerClass:[SavedRiddleCell class] forCellReuseIdentifier:reuseID];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView setBackgroundColor:[UIColor backgroundColorGrey]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    //scrollbars
    [self.tableView setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
    [self.view addSubview:self.tableView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self.tableView flashScrollIndicators];

}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_favouritesArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SavedRiddleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseID"];
    if (cell == nil) {
        cell = [[SavedRiddleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuseID"];
    }
    NSLog(@"Cell Width: %f", cell.frame.size.width);
    NSNumber *favouriteID = [_favouritesArray objectAtIndex:indexPath.row];
    cell.star.text = @"d";
    NSArray *riddles = [_riddleDictionary objectForKey:@"Tomhaiseanna"];
    for (int i = 0; i < [riddles count]; i++) {
        NSMutableDictionary *question = [riddles objectAtIndex:i];
        NSNumber *currentRiddle = [question objectForKey:@"id"];
        if ([currentRiddle  isEqual: favouriteID]) {
            cell.savedTomhais.text = [question objectForKey:@"question"];
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  OptionsScene.m
//  Facts
//
//  Created by Linda Keating on 09/12/2014.
//  Copyright (c) 2014 LindaKeating. All rights reserved.
//

#import "OptionsScene.h"
#import "GameScene.h"


@implementation OptionsScene{
    NSUserDefaults* defaults;
}

-(id) initWithSize:(CGSize)size{
    defaults = [NSUserDefaults standardUserDefaults];
    if (self = [super initWithSize:size]) {
        NSLog(@"OptionsScene");
    }
    return self;
}

-(void)didMoveToView:(SKView *)view{
    _backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _backButton.frame = CGRectMake(CGRectGetMidX(self.frame)-230, CGRectGetMidY(self.frame)+360, 200, 70.0);
    _backButton.backgroundColor = [UIColor clearColor];
    [_backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *buttonExitImageNormal = [UIImage imageNamed:@"ExitBtn.png"];
    UIImage *strechableButtonExitImageNormal = [buttonExitImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [_backButton setBackgroundImage:strechableButtonExitImageNormal forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(moveToHome) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_backButton];
    
    _soundSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.frame)-50, CGRectGetMaxY(self.frame)- 300, 100, 100)];
    [_soundSwitch addTarget:self action:@selector(flipMusicAndSound:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_soundSwitch];
    
    _musicSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(CGRectGetMidX(self.frame)-50, CGRectGetMidY(self.frame)+180, 100, 100)];
    [_musicSwitch addTarget:self action:@selector(flipMusicAndSound:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_musicSwitch];
    
    _soundTitle = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    [_soundTitle setText:@"Sound"];
    [_soundTitle setFontSize:40];
    [_soundTitle setPosition:CGPointMake(CGRectGetMidX(self.frame)-20, CGRectGetMidY(self.frame))];
    [self addChild:_soundTitle];
    
    _musicTitle = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    [_musicTitle setText:@"Music"];
    [_musicTitle setFontSize:40];
    [_musicTitle setPosition:CGPointMake(CGRectGetMidX(self.frame)-20, CGRectGetMidY(self.frame)-60)];
    [self addChild:_musicTitle];
    
    long soundDefaults = [defaults integerForKey:@"sound"];
    long musicDefaults = [defaults integerForKey:@"music"];
    
    if (soundDefaults == 1) {
        [_soundSwitch setOn:YES animated:YES];
    }
    else{
        [_soundSwitch setOn:FALSE animated:YES];
    }
    
    if (musicDefaults == 1) {
        [_musicSwitch setOn:YES animated:YES];
    }
    else{
        [_musicSwitch setOn:FALSE animated:YES];
    }
}

-(void)moveToHome {
    GameScene* gScene = [[GameScene alloc] initWithSize:CGSizeMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame))];
    [_backButton removeFromSuperview];
    [_soundSwitch removeFromSuperview];
    [_musicSwitch removeFromSuperview];
    [self.scene.view presentScene:gScene];
}

-(IBAction)flipMusicAndSound:(id)sender{
    if (_musicSwitch.on) {
        [defaults setInteger:1 forKey:@"music"];
    }
    else {
        [defaults setInteger:0 forKey:@"music"];
    }
    
    if (_soundSwitch.on) {
        [defaults setInteger:1 forKey:@"sound"];
    }
    else {
        [defaults setInteger:0 forKey:@"sound"];
    }
}

@end
